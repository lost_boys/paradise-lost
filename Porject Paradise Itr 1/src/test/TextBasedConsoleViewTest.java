/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */
package test;

import model.AgentCollection;
import model.GameState;
import model.Map;
import model.Resources;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EnumMap;


@SuppressWarnings("serial")
public class TextBasedConsoleViewTest extends JFrame
{
	public  GameState aGame;
	private  AgentCollection agents;
	private  EnumMap<Resources, Integer> resourcepool;
	public  Map map;
	
	private  JTextPane textPane;
	private  JScrollPane scrollPane;
	private  JButton zoomIn;
	private  JButton zoomOut;
	private  JButton randomIt;
	private  String str = "";
	private  TextBasedConsoleViewTest console;
    private  String gamestate = "";
    private  JButton refresh;
    private JFrame gameStatusWindow;
    private static int DELAY_IN_MILLIS = 250;

    private  JTextPane gameStatusPane;
	
	private  Map aMap;

    private  GameState model;
    
/*	
	public static void main(String args [])
	{
		
//		//aMap = new Map();
//		
//		for(int i = 0; i< 200; i++)
//		{
//			for(int j = 0; j< 400; j++)
//			{
//				str = str + (new Random().nextInt(10));
//			}
//			str = str + "\n";
//		}
		
		
		//BuildingSimulation run = new BuildingSimulation();
		
        aGame = new GameState();
        aMap = aGame.getMap();
        agents = aGame.getAgents();
        resourcepool = aGame.getResourcepool();
        
        //runSimulation();
        Agent a1 = new Agent(aMap, 10, 10);
		Agent a2 = new Agent(aMap, 11, 10);
		Agent a3 = new Agent(aMap, 12, 10);
		a1.setName("Bill");
		a2.setName("Sanket");
		a3.setName("Hercy");
		a1.setHunger((short) 2000);
		a2.setHunger((short) 2000);
		a3.setHunger((short) 2000);
		a1.setThirst((short) 2000);
		a2.setThirst((short) 2000);
		a3.setThirst((short) 2000);
		a1.setFatigue((short) 2000);
		a2.setFatigue((short) 2000);
		a3.setFatigue((short) 2000);
		
		
		agents.addAgent(a1);
		agents.addAgent(a2);
		agents.addAgent(a3);

		resourcepool.put(Resources.Wood, 2000);
		resourcepool.put(Resources.Airplane, 1000);
		resourcepool.put(Resources.Stone, 1000);
		resourcepool.put(Resources.Water, 0);
		resourcepool.put(Resources.Iron, 500);

		a1.setFatigue((short) 10);
		a2.setFatigue((short) 10);
		a3.setFatigue((short) 1000);

		agents.command(new Build("Living Tent", 20, 20));

		a1.setLocation(18, 20);
		a2.setLocation(20, 20);
		a3.setLocation(20, 20);
        
        str = aMap.toString();
        
        gamestate = agents.populationStatus();
		
		console = new TextBasedConsoleView();
		
//		
//		while(true)
//		{
//			scrollPane.setSize(new Dimension(console.getWidth()-10, console.getHeight()-70));
//			textPane.setSize(new Dimension(console.getWidth()-10, console.getHeight()-70));
//			zoomIn.setLocation(5, console.getHeight()-60);
//			zoomOut.setLocation(40, console.getHeight()-60);
//			randomIt.setLocation(75, console.getHeight()-60);
//			
//			str = "";
//			for(int i = 0; i< 100; i++)
//			{
//				for(int j = 0; j< 100; j++)
//				{
//					str = str + (new Random().nextInt(10));
//				}
//				str = str + "\n";
//			}
//			textPane.setText(str);
//		}
//		
		
	}
*/
	
	public TextBasedConsoleViewTest(GameState state)
	{
		this();
		this.model = state;
		this.agents = state.getAgents();
		this.aMap = state.getMap();
		this.resourcepool = state.getResourcepool();
		
	}
	
	
	public TextBasedConsoleViewTest()
	{
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setTitle("Console");
		this.setLayout(null);
		this.setSize(900, 700);

		textPane = new JTextPane();
		textPane.setEditable(true);
		textPane.setBackground(Color.BLACK);
		textPane.setForeground(Color.GREEN);
		textPane.setText(str);
		textPane.setFont(new Font("Consolas", Font.PLAIN, 8));
		textPane.setFocusable(true);
		this.add(textPane);
		
		JScrollPane scrollPane = new JScrollPane(textPane);
		scrollPane.setBounds(5, 5, this.getWidth()-10, this.getHeight()-70);
		this.add(scrollPane);


		
		//textPane.setBounds(5, 5, this.getWidth()-10, this.getHeight()-70);
		//this.add(textPane);
		
		zoomIn = new JButton("+");
		zoomIn.addActionListener(new ZoomInListner());
		zoomOut = new JButton("-");
		zoomOut.addActionListener(new ZoomOutListner());
		randomIt = new JButton("@");
		randomIt.addActionListener(new randomIt());
		
		zoomIn.setBounds(5, this.getHeight()-60, 30, 30);
		zoomOut.setBounds(40, this.getHeight()-60, 30, 30);
		randomIt.setBounds(75, this.getHeight()-60, 30, 30);
		
		this.add(zoomIn);
		this.add(zoomOut);
		this.add(randomIt);
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(screenSize.width / 2 - this.getSize().width / 2, screenSize.height / 2 - this.getSize().height / 2);
		this.setVisible(true);

        //Game status window
		
        JFrame gameStatusWindow = new JFrame();
        gameStatusWindow.setVisible(true);
        gameStatusWindow.setLayout(null);
        gameStatusWindow.setTitle("Game Status");
        gameStatusWindow.setSize(800, 600);
        gameStatusWindow.setLocation(screenSize.width / 2 - this.getSize().width / 2 + 400, screenSize.height / 2 - this.getSize().height / 2 +100);
        gameStatusWindow.setResizable(false);

        gameStatusPane = new JTextPane();
        gameStatusPane.setEditable(false);
        gameStatusPane.setBackground(Color.BLACK);
        gameStatusPane.setForeground(Color.GREEN);
        gameStatusPane.setText(gamestate);
        gameStatusPane.setBounds(5, 5, 790, 600-30);


        refresh = new JButton("Refresh");
        refresh.addActionListener(new RefreshListener());
        refresh.setBounds(5, gameStatusWindow.getHeight()-100, 100, 40);


        gameStatusWindow.add(gameStatusPane);
        gameStatusWindow.add(refresh);
        
        
        Timer timer = new Timer(DELAY_IN_MILLIS, new RefreshListener());
		timer.start();

	}
	
	private class ZoomInListner implements ActionListener
	{

		public void actionPerformed(ActionEvent e)
		{
			
			textPane.setFont(new Font(textPane.getFont().getFamily(), Font.PLAIN, textPane.getFont().getSize()+1));

			System.out.println("You zoomed in!");
		}
	}
	
	private class ZoomOutListner implements ActionListener
	{

		public void actionPerformed(ActionEvent e)
		{
			
			textPane.setFont(new Font(textPane.getFont().getFamily(), Font.PLAIN, textPane.getFont().getSize()-1));

			System.out.println("You zoomed out!");
		}
	}
	
	private class randomIt implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			/*
			str = "";
			for(int i = 0; i< 200; i++)
			{
				for(int j = 0; j< 400; j++)
				{
					str = str + (new Random().nextInt(10));
				}
				str = str + "\n";
			}
			*/
			aMap = new Map(65,65);
			str = aMap.toString();
			
			textPane.setText(str);
			System.out.println("You rebuilt the map!");
			
		}
	}


    private class RefreshListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e)
        {
        	/*
            gamestate = model.getAgents().populationStatus();
            gameStatusPane.setText(gamestate);
            System.out.println("You refreshed the game state!");
            */
         	
        	//System.out.println("You refreshed the game state!");
        	gamestate = agents.populationStatus();
        	gameStatusPane.setText(gamestate);
        	String laststate = str;
        	str = aMap.toString();
        	if (!str.equals(laststate))
        	textPane.setText(str);
        }
    }
    /*
    private static void runSimulation()
	{
		Agent a1 = new Agent(aMap, 10, 10);
		Agent a2 = new Agent(aMap, 11, 10);
		Agent a3 = new Agent(aMap, 12, 10);

		agents.addAgent(a1);
		agents.addAgent(a2);
		agents.addAgent(a3);

		resourcepool.put(Resources.Wood, 2000);
		resourcepool.put(Resources.Airplane, 1000);
		resourcepool.put(Resources.Stone, 1000);
		resourcepool.put(Resources.Water, 0);
		resourcepool.put(Resources.Iron, 500);

		a1.setFatigue((short) 10);
		a2.setFatigue((short) 10);
		a3.setFatigue((short) 100);

		agents.command(new Build("Living Tent", 20, 20));

		a1.setLocation(40, 20);
		a2.setLocation(20, 21);
		a3.setLocation(20, 20);
	}
	*/

}
