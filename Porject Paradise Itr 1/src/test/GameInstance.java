/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package test;

import model.AgentCollection;
import model.GameState;
import model.Map;

public class GameInstance
{
	private GameState gameState;
	private AgentCollection agents;

	public GameInstance()
	{
		this.gameState = new GameState(65, 65);
		this.agents = gameState.getAgents();

	}

	public AgentCollection getAgents()
	{
		return this.agents;
	}

	public Map getMap()
	{
		return gameState.getMap();
	}

}
