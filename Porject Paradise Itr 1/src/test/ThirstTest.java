/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package test;

import model.*;

import java.util.Scanner;

public class ThirstTest
{

	/*
	 * Press Enter at the console to update and redraw state, to watch an Agent
	 * get a drink Alter the starting thirst to let them starve to death
	 */

	@SuppressWarnings("resource")
	public static void main(String[] args)
	{
		GameState state = new GameState(65, 65);
		Map map = state.getMap();
		map.generateMap(new SimpleMap());
		Agent thirstyAgent = new Agent(state.getMap(), state.getMap().getPlaneLocation().y, state.getMap().getPlaneLocation().x);
		thirstyAgent.setLocation(25, 25, map);
		thirstyAgent.setThirst((short) 99);
		Agent parchedAgent = new Agent(map, 15, 10);
		parchedAgent.setThirst((short) 5);
		Agent carrierAgent = new Agent(state.getMap(), state.getMap().getPlaneLocation().y, state.getMap().getPlaneLocation().x);
		carrierAgent.setLocation(50, 10, map);

		carrierAgent.order(new HarvestCommand(Resources.Water));
		carrierAgent.order(new AssembleCommand(PersonalItem
				.getInstance("hammer")));

		state.getAgents().addAgent(thirstyAgent);
		state.getAgents().addAgent(parchedAgent);
		state.getAgents().addAgent(carrierAgent);

		map.getTile(55, 10).setItem(Item.Container);

		System.out.println(map);
		Scanner pacer = new Scanner(System.in);
		while(true)
		{
			pacer.nextLine();
			for(int i = 0; i < 80; i++)
			{
				System.out.println("\n\n\n");
			}
			state.update();
			System.out.println(map);
			System.out.println(thirstyAgent);
			System.out.println(parchedAgent);
			System.out.println(carrierAgent);
		}

	}
}
