/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package test;

import model.*;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.ref.WeakReference;

public class IntegrationTest
{

	/**
	 * @param args
	 */
	public static void main(String[] args)
	{
		GameState state = new GameState(65, 65);
		AgentCollection as = state.getAgents();
		Map map = state.getMap();
		as.addAgent(new Agent(map, 10, 10));
		as.addAgent(new Agent(map, 12, 10));
		as.addAgent(new Agent(map, 10, 12));
		as.addAgent(new Agent(map, 12, 12));
		as.addAgent(new Agent(map, 14, 10));
		new TextBasedConsoleViewTest(state);
		new Timer(550, new TimerListener(state)).start();

	}

	private static class TimerListener implements ActionListener
	{
		private WeakReference<GameState> state;

		public TimerListener(GameState state)
		{
			this.state = new WeakReference<GameState>(state);
		}

		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			GameState current = state.get();
			if(current != null)
				current.update();

		}

	}

}
