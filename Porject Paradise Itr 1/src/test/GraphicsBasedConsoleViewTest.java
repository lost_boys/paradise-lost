/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */
package test;

import model.GameState;
import model.Map;
import view.MapPanel;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

@SuppressWarnings("serial")
public class GraphicsBasedConsoleViewTest extends JFrame
{
	private static JPanel MapPane;
	private static JScrollPane scrollPane;
	private static JButton reDecorate;
	private static JButton saveImage;
	private static JButton randomIt;
	private static JButton saveGame;
	private static JButton loadGame;
	private static String str = "";

	private Map aMap;
	private GameState state;

	public static void main(String args[])
	{
		new GraphicsBasedConsoleViewTest();
	}

	public GraphicsBasedConsoleViewTest()
	{
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.setSize(1024, 768);
		this.setTitle("Console");
		this.setLayout(null);

		state = new GameState(65, 65);
		aMap = state.getMap();
		MapPane = new MapPanel(state);
		System.out.println(aMap);
		System.out.println();

		// MapPane.setFocusable(false);

		// MapPane.setBounds(5, 5, this.getWidth()-10, this.getHeight()-70);
		// this.add(MapPane);
		// scrollPane = new JScrollPane(MapPane);
		// JScrollPane scroller = new JScrollPane(MapPane);
		// scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		// add(scroller);

		scrollPane = new JScrollPane(MapPane,
				JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		// scrollPane.getViewport().setScrollMode(JViewport.SIMPLE_SCROLL_MODE);
		scrollPane.setBounds(5, 5, this.getWidth() - 10, this.getHeight() - 70);

		this.add(scrollPane);

		reDecorate = new JButton("*");
		reDecorate.addActionListener(new ReDecorateListener());
		saveImage = new JButton("$");
		saveImage.addActionListener(new SaveImageListener());
		randomIt = new JButton("@");
		randomIt.addActionListener(new ReGenerateAMapListener());
		saveGame = new JButton("S");
		saveGame.addActionListener(new SaveGameListener());
		loadGame = new JButton("L");
		loadGame.addActionListener(new LoadGameListener());

		reDecorate.setBounds(5, this.getHeight() - 60, 30, 30);
		saveImage.setBounds(40, this.getHeight() - 60, 30, 30);
		randomIt.setBounds(75, this.getHeight() - 60, 30, 30);
		loadGame.setBounds(110, this.getHeight() - 60, 50, 30);
		saveGame.setBounds(165, this.getHeight() - 60, 50, 30);

		this.add(reDecorate);
		this.add(saveImage);
		this.add(randomIt);
		this.add(loadGame);
		this.add(saveGame);

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(screenSize.width / 2 - this.getSize().width / 2,
				screenSize.height / 2 - this.getSize().height / 2);
		this.setVisible(true);

	}

	private class ReDecorateListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e)
		{

			// MapPane.setFont(new Font(MapPane.getFont().getFamily(),
			// Font.PLAIN, MapPane.getFont().getSize()+1));
			((MapPanel) MapPane).reDecorations();
			System.out
					.println("\n\n\n\n____________________________\nYou redecorated!\n____________________________");
		}
	}

	private class SaveImageListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e)
		{

			// MapPane.setFont(new Font(MapPane.getFont().getFamily(),
			// Font.PLAIN, MapPane.getFont().getSize()-1));

			try
			{
				((MapPanel) MapPane).saveMapIntoPNG("PNG");
			}
			catch(IOException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			System.out
					.println("\n\n\n\n____________________________\nYou saved a image!\n____________________________");
		}
	}

	private class ReGenerateAMapListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			/*
			 * str = ""; for(int i = 0; i< 200; i++) { for(int j = 0; j< 400;
			 * j++) { str = str + (new Random().nextInt(10)); } str = str +
			 * "\n"; }
			 */
			aMap = new Map(65, 65);
			str = aMap.toString();
			((MapPanel) MapPane).reGenerateAMap(aMap);

			System.out
					.println("\n\n\n\n____________________________\nYou rebuilt the map!\n____________________________");

			System.out.print(str);
		}
	}

	private class SaveGameListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			File gameFile = new File("game.sav");
			FileOutputStream fileOut;
			try
			{
				fileOut = new FileOutputStream(gameFile);
				ObjectOutputStream objectout = new ObjectOutputStream(fileOut);
				objectout.writeObject(state);
				objectout.close();
			}
			catch(FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch(IOException e)
			{
				e.printStackTrace();
			}

		}

	}

	private class LoadGameListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			File myFile = new File("game.sav");
			FileInputStream filein;

			try
			{
				filein = new FileInputStream(myFile);
				ObjectInputStream objectin = new ObjectInputStream(filein);
				state = (GameState) objectin.readObject();
				objectin.close();
				filein.close();
				aMap = state.getMap();
				aMap.makeConnections();
				((MapPanel) MapPane).reGenerateAMap(aMap);
				System.out.println(aMap.toString());
				System.out.println();
			}
			catch(FileNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch(IOException f)
			{
				// TODO Auto-generated catch block
				f.printStackTrace();
			}
			catch(ClassNotFoundException g)
			{
				// TODO Auto-generated catch block
				g.printStackTrace();
			}

		}

	}

}
