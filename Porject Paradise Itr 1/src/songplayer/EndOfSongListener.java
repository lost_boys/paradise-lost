/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */


package songplayer;

/**
 * Created with IntelliJ IDEA. User: Sank Date: 4/27/14 Time: 2:07 PM To change
 * this template use File | Settings | File Templates.
 */
public interface EndOfSongListener
{
	public void songFinishedPlaying(
			EndOfSongEvent eventWithFileNameAndDateFinished);

}
