/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package songplayer;

import java.util.GregorianCalendar;

/**
 * Created with IntelliJ IDEA. User: Sank Date: 4/27/14 Time: 2:06 PM To change
 * this template use File | Settings | File Templates.
 */
public class EndOfSongEvent
{
	private String fileName;
	private GregorianCalendar currentTime;

	/**
	 * Construct a new EndOfSongEvent with the file name just finished playing
	 * and the time at which the song finished playing
	 * 
	 * @param fileName
	 *            The song that just finished playing
	 * @param currentTime
	 *            The moment at which the song finished playing
	 */
	public EndOfSongEvent(String fileName, GregorianCalendar currentTime)
	{
		this.fileName = fileName;
		this.currentTime = currentTime;
	}

	/**
	 * Provide access to the name of the audio file that just finished.
	 * 
	 * @return the name of the file sent to the SongPlayer
	 */
	public String fileName()
	{
		return fileName;
	}

	/**
	 * Provide access to time of this EndOfSongEvent
	 * 
	 * @return the name of the file sent to the SongPlayer
	 */
	public GregorianCalendar finishedTime()
	{
		return currentTime;
	}
}
