/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package songplayer;

import java.util.*;

/**
 * Created with IntelliJ IDEA. User: Sank Date: 4/27/14 Time: 2:34 PM To change
 * this template use File | Settings | File Templates.
 */
public class MusicLibrary
{
	Queue<String> musicLibrary;
	String menu;
	boolean stopped;

	
	public MusicLibrary()
	{
		@SuppressWarnings("unused")
		String menu = "music/Menu_Background_Music.wav";
		
		ArrayList<String> tracks = new ArrayList<String>();
		musicLibrary = new LinkedList<String>();
		tracks.add("music/Clockwise_Operetta.mp3");
		tracks.add("music/The_Furnace.mp3");
		tracks.add("music/The_Glasshouse_With_Butterfly.mp3");
		tracks.add("music/The_Sea.mp3");

		Collections.shuffle(tracks, new Random());
		Collections.shuffle(tracks, new Random());

		musicLibrary.add(tracks.get(0));
		musicLibrary.add(tracks.get(1));
		musicLibrary.add(tracks.get(2));
		musicLibrary.add(tracks.get(3));
	}

	public void playMenu()
	{
		// TODO: change this for the state of being in the menu
		while(true)
		{
			EndOfSongListener waiter = new SongListener();
			SongPlayer.playFile(waiter, menu);
		}
	}

	// Playing songs
	public void playSongs()
	{
		System.out.println("Start playing");
		if(stopped)
		{
			stopped = false;
			new MusicLibrary();
		}

		EndOfSongListener waiter = new SongListener();
		if(!musicLibrary.isEmpty())
		{
			SongPlayer.playFile(waiter, musicLibrary.peek());
			musicLibrary.remove();
		}

		System.out.println("Continue");
	}

	public void stopPlaying()
	{
		try
		{
			SongPlayer.aPlayer.stopPlay();
			musicLibrary.clear();
			stopped = true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	private void playNextSong()
	{
		EndOfSongListener waiter = new SongListener();
		if(!musicLibrary.isEmpty())
			SongPlayer.playFile(waiter, musicLibrary.poll());
	}

	private class SongListener implements EndOfSongListener
	{

		@SuppressWarnings({ "static-access", "unused" })
		@Override
		public void songFinishedPlaying(EndOfSongEvent eos)
		{
			System.out.println("Start next song");
			int start = eos.finishedTime().SECOND;
			try
			{
				Thread.sleep(2000);

			}
			catch(InterruptedException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// System.out.println(eos.finishedTime());
			if(!musicLibrary.isEmpty())
				playNextSong();
			if(musicLibrary.isEmpty() && !stopped)
			{
				new MusicLibrary();
				playSongs();
			}
		}

	}

}
