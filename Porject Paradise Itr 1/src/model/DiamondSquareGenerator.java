/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package model;

import java.util.HashMap;

public class DiamondSquareGenerator implements TerrainGenerator
{

	@Override
	public void generateTerrain(Map map)
	{
		/*
		 * Iterations are the number of "recursions" it takes to reach the
		 * smallest square
		 */
		int iterations = 0;
		int size = map.getHeight();
		while(size > 1)
		{
			iterations++;
			size >>= 1;
		}

		int[][] heightMap;

		/*
		 * TODO: Make terrain placement range more dynamic so that if we want a
		 * high variability or a different seed value, we don't end up with a
		 * bunch of water tiles.
		 */

		HashMap<Terrains, Integer> terrain = new HashMap<Terrains, Integer>();
		double waterFraction;
		double rockyFraction;

		/*
		 * Generate the map. If the map has too many water tiles or rocky tiles,
		 * scrap it and generate a new one.
		 */
		do
		{
			terrain.put(Terrains.Water, 0);
			terrain.put(Terrains.Beach, 0);
			terrain.put(Terrains.Jungle, 0);
			terrain.put(Terrains.Rocky, 0);
			heightMap = makeHeightMap(iterations, 32, 250);
			for(int i = 2; i < map.getHeight() - 1; i++)
			{
				for(int j = 2; j < map.getWidth() - 1; j++)
				{
					int value = heightMap[i][j];
					if(value > 255)
						value = 255;
					else if(value < 0)
						value = 0;

					if(value < 64)
					{
						map.getTile(i, j).setTerrain(Terrains.Water);
						terrain.put(Terrains.Water,
								terrain.get(Terrains.Water) + 1);
					}
					else if(value < 90)
					{
						map.getTile(i, j).setTerrain(Terrains.Beach);
						terrain.put(Terrains.Beach,
								terrain.get(Terrains.Beach) + 1);
					}
					else if(value < 192)
					{
						map.getTile(i, j).setTerrain(Terrains.Jungle);
						terrain.put(Terrains.Jungle,
								terrain.get(Terrains.Jungle) + 1);
					}
					else if(value >= 192)
					{
						map.getTile(i, j).setTerrain(Terrains.Rocky);
						terrain.put(Terrains.Rocky,
								terrain.get(Terrains.Rocky) + 1);
					}
					else
						System.out.println("Heightmap value out of bounds: "
								+ heightMap[i][j]);
				}
			}
			waterFraction = (double) terrain.get(Terrains.Water)
					/ (map.getHeight() * map.getHeight());
			rockyFraction = (double) terrain.get(Terrains.Rocky)
					/ (map.getHeight() * map.getHeight());
			System.out.println("Water: " + waterFraction);
			System.out.println("Rocky: " + rockyFraction);
		}
		while(waterFraction > 0.4 || rockyFraction > 0.5);

		/*
		 * Set the terrain of the edge of the map to be water
		 */
		for(int i = 0; i < map.getWidth(); i++)
		{
			map.getTile(i, 0).setTerrain(Terrains.Water);
			map.getTile(0, i).setTerrain(Terrains.Water);
			map.getTile(i, 1).setTerrain(Terrains.Water);
			map.getTile(1, i).setTerrain(Terrains.Water);
			map.getTile(i, map.getWidth() - 1).setTerrain(Terrains.Water);
			map.getTile(map.getHeight() - 1, i).setTerrain(Terrains.Water);
			map.getTile(i, map.getWidth() - 2).setTerrain(Terrains.Water);
			map.getTile(map.getHeight() - 2, i).setTerrain(Terrains.Water);
		}
	}

	/**
	 * This method uses the seed value to initialize the four corners of the
	 * map. The variation creates randomness in the map. The size of the array
	 * is determined by the amount of iterations (i.e. 1 iteration -> 3x3 array,
	 * 2 iterations -> 5x5 array, etc.).
	 * 
	 * @param iterations
	 *            the amount of iterations to do (minimum of 1)
	 * @param seed
	 *            the starting value
	 * @param variation
	 *            the amount of randomness in the height map (minimum of 0)
	 * @return a height map in the form of a 2-dimensional array containing
	 *         integer values or null if the arguments are out of range
	 */
	public static int[][] makeHeightMap(int iterations, int seed, int variation)
	{
		if(iterations < 1 || variation < 0)
		{
			return null;
		}

		int size = (1 << iterations) + 1;
		int[][] map = new int[size][size];
		final int maxIndex = map.length - 1;

		// seed the corners
		map[0][0] = seed;
		map[0][maxIndex] = seed;
		map[maxIndex][0] = seed;
		map[maxIndex][maxIndex] = seed;

		for(int i = 1; i <= iterations; i++)
		{
			int minCoordinate = maxIndex >> i;// Minimum coordinate of the
												// current map spaces
			size = minCoordinate << 1;// Area surrounding the current place in
										// the map

			diamondStep(minCoordinate, size, map, variation);
			squareStepEven(minCoordinate, map, size, maxIndex, variation);
			squareStepOdd(map, size, minCoordinate, maxIndex, variation);

			variation = variation >> 1;// Divide variation by 2
		}

		return map;
	}

	/**
	 * Calculates average values of four corner values taken from the smallest
	 * possible square.
	 * 
	 * @param minCoordinate
	 *            the x and y coordinate of the first square center
	 * @param size
	 *            width and height of the squares
	 * @param map
	 *            the height map to fill
	 * @param variation
	 *            the randomness in the height map
	 */
	private static void diamondStep(int minCoordinate, int size, int[][] map,
			int variation)
	{
		for(int x = minCoordinate; x < (map.length - minCoordinate); x += size)
		{
			for(int y = minCoordinate; y < (map.length - minCoordinate); y += size)
			{
				int left = x - minCoordinate;
				int right = x + minCoordinate;
				int up = y - minCoordinate;
				int down = y + minCoordinate;

				// the four corner values
				int val1 = map[left][up]; // upper left
				int val2 = map[left][down]; // lower left
				int val3 = map[right][up]; // upper right
				int val4 = map[right][down];// lower right

				calculateAndInsertAverage(val1, val2, val3, val4, variation,
						map, x, y);
			}
		}
	}

	/**
	 * Calculates average values of four corner values taken from the smallest
	 * possible diamond. This method calculates the values for the even rows,
	 * starting with row 0.
	 * 
	 * @param minCoordinate
	 *            the x-coordinate of the first diamond center
	 * @param map
	 *            the height map to fill
	 * @param size
	 *            the length of the diagonals of the diamonds
	 * @param maxIndex
	 *            the maximum index in the array
	 * @param variation
	 *            the randomness in the height map
	 */
	private static void squareStepEven(int minCoordinate, int[][] map,
			int size, int maxIndex, int variation)
	{
		for(int x = minCoordinate; x < map.length; x += size)
		{
			for(int y = 0; y < map.length; y += size)
			{
				if(y == maxIndex)
				{
					map[x][y] = map[x][0];
					continue;
				}

				int left = x - minCoordinate;
				int right = x + minCoordinate;
				int down = y + minCoordinate;
				int up = 0;

				if(y == 0)
				{
					up = maxIndex - minCoordinate;
				}
				else
				{
					up = y - minCoordinate;
				}

				// the four corner values
				int val1 = map[left][y]; // left
				int val2 = map[x][up]; // up
				int val3 = map[right][y];// right
				// System.out.println(x + ", " + down);
				int val4 = map[x][down]; // down

				calculateAndInsertAverage(val1, val2, val3, val4, variation,
						map, x, y);
			}
		}
	}

	/**
	 * Calculates average values of four corner values taken from the smallest
	 * possible diamond. This method calculates the values for the odd rows,
	 * starting with row 1.
	 * 
	 * @param minCoordinate
	 *            the x-coordinate of the first diamond center
	 * @param map
	 *            the height map to fill
	 * @param size
	 *            the length of the diagonals of the diamonds
	 * @param maxIndex
	 *            the maximum index in the array
	 * @param variation
	 *            the randomness in the height map
	 */
	private static void squareStepOdd(int[][] map, int size, int minCoordinate,
			int maxIndex, int variation)
	{
		for(int x = 0; x < map.length; x += size)
		{
			for(int y = minCoordinate; y < map.length; y += size)
			{
				if(x == maxIndex)
				{
					map[x][y] = map[0][y];
					continue;
				}

				int left = 0;
				int right = x + minCoordinate;
				int down = y + minCoordinate;
				int up = y - minCoordinate;

				if(x == 0)
				{
					left = maxIndex - minCoordinate;
				}
				else
				{
					left = x - minCoordinate;
				}

				// the four corner values
				int val1 = map[left][y]; // left
				int val2 = map[x][up]; // up
				int val3 = map[right][y];// right
				int val4 = map[x][down]; // down

				calculateAndInsertAverage(val1, val2, val3, val4, variation,
						map, x, y);
			}
		}
	}

	/**
	 * Calculates an average value, adds a variable amount to that value and
	 * inserts it into the height map.
	 * 
	 * @param val1
	 *            first of the values used to calculate the average
	 * @param val2
	 *            second of the values used to calculate the average
	 * @param val3
	 *            third of the values used to calculate the average
	 * @param val4
	 *            fourth of the values used to calculate the average
	 * @param variation
	 *            adds variation to the average value
	 * @param map
	 *            the height map to fill
	 * @param x
	 *            the x-coordinate of the place to fill
	 * @param y
	 *            the y-coordinate of the place to fill
	 */
	private static void calculateAndInsertAverage(int val1, int val2, int val3,
			int val4, int variation, int[][] map, int x, int y)
	{
		int avg = (val1 + val2 + val3 + val4) >> 2;// average
		// double rand;
		// do
		// {
		// rand = Math.random();
		// } while (rand > 0.1 && rand < 0.9);
		int var = (int) ((Math.random() * ((variation << 1) + 1)) - variation);
		map[x][y] = avg + var;
	}
}
