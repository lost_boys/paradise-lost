/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package model;


import model.PersonalItem.Axe;
import model.PersonalItem.Bucket;

import java.awt.*;
import java.io.Serializable;
import java.util.*;
import java.util.List;
import java.util.Queue;

/* The active unit of gameplay, these set about executing tasks with their own AI
 *  While inner classes are declared in this class, they are of protected scope, and so subclasses could be made outside this class as well;
 *  These are here for convenience and to reduce clutter 
 */

public class Agent implements Serializable
{
	private static final long serialVersionUID = -4024710760243565781L;
	private final static int NEED_CEILING = Short.MAX_VALUE / 7;
	private final static int NEED_THRESHOLD = (int) (.90 * NEED_CEILING);
	private final static int NEED_REPLENISHMENT_CUTOFF = (int)(.95 * NEED_CEILING);
	private final static int RESOURCE_CARRY_CAP = 20;
	// The three needs will diminish on each tick, but various actions or other
	// contexts might also cause drops in them
	private short hunger, thirst, fatigue;
    private List<PersonalItem> carriedItems;
	private PriorityQueue<Task> taskQueue;
	private String name;
	private Behavior behavior;
	private int locationX, locationY;
	private Map enclosingMap;
	private AgentCollection enclosingCollection;
	boolean dead;

	private EnumMap<Resources, Integer> resourcesCarried;
	
	

	// is a reference to the global pool; constant style name because treat it
	// like a constant
	private EnumMap<Resources, Integer> RESOURCE_POOL;

	public Agent()
	{
		this.hunger = NEED_CEILING;
		this.thirst = NEED_CEILING;
		this.fatigue = NEED_CEILING;
		//this.inventory = new ArrayList<Item>();
		this.taskQueue = new PriorityQueue<Task>();
		this.behavior = new IdleBehavior();
		// This short array of names is taken from the Wiki page for the
		// characters of LOST; it would ultimately be preferable to have a text
		// file with a much larger set of names, to reduce the chances of name
		// collision
		String[] names = new String[]
		{ "Daniel", "Boone", "Miles", "Michael", "Ana Lucia", "Charlotte",
				"Frank", "Shannon", "Desmond", "Eko", "Kate", "Jack", "Sawyer",
				"Locke", "Ben", "Sayid", "Libby", "Sun", "Jin", "Claire",
				"Hurley", "Juliet", "Charlie", "Richard", "Bernard", "Rose",
				"Vincent" };
		this.name = names[new java.util.Random().nextInt(names.length)];
		this.resourcesCarried = new EnumMap<Resources, Integer>(Resources.class);
		for(Resources resource : Resources.values())
		{
			resourcesCarried.put(resource, 0);
		}
		
		carriedItems = new ArrayList<PersonalItem>();
		
		dead = false;
		
	}


	public Agent(Map map, int x, int y)
	{
		this();
		this.locationX = x;
		this.locationY = y;
		this.enclosingMap = map;
		this.enclosingMap.getTile(x,y).setAgent(this);
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	public void setMembership(AgentCollection a)
	{
		this.enclosingCollection = a;
	}

	// These should probably only be called by things that can let the map know
	// what's up
	// Unless maps update their knowledge of what agent is where by iterating
	// over the collection
	public void setLocation(int x, int y, Map map)
	{
		this.enclosingMap = map;
		this.locationX = x;
		this.locationY = y;
		this.enclosingMap.getTile(x, y).setAgent(this);
	}

	public void setLocation(int x, int y)
	{
		this.locationX = x;
		this.locationY = y;
		this.enclosingMap.getTile(x, y).setAgent(this);
	}

	public int getLocationX()
	{
		return this.locationX;
	}

	public int getLocationY()
	{
		return this.locationY;
	}

	public Point getLocation()
	{
		return new Point(locationX, locationY);
	}

	public void setThirst(short t)
	{
		this.thirst = t;
	}
	
	

	public short getHunger() 
	{
		return hunger;
	}

	public short getThirst() 
	{
		return thirst;
	}

	public short getFatigue() 
	{
		return fatigue;
	}
	
	public String getName()
	{
		return this.name;
	}

	public boolean move(int x, int y)
	{
		
		if(!this.enclosingMap.passableAt(x, y))
		{
			return false;
		}
		this.enclosingMap.getTile(locationX, locationY).setAgent(null);
		this.locationX = x;
		this.locationY = y;
		this.enclosingMap.getTile(x, y).setAgent(this);
		locationX = x;
		locationY = y;
		return true;
	}

	public void setGlobalResources(EnumMap<Resources, Integer> pool)
	{
		this.RESOURCE_POOL = pool;
	}

	public void die()
	{
		
		this.enclosingCollection.killAgent(this);
		//System.out.println(locationX + ", " + locationY);
		//AgentCollection will push notification of Agent's death
		this.enclosingMap.getTile(locationX, locationY).setAgent(null);
		dead = true;
		//System.out.print(name + " DIED!!!!!!!!!!!!!!!!!!");
		
	}
	
	protected void taskComplete()
	{
		this.behavior = new IdleBehavior();
	}

	/*
	 * This method should be called exactly once every game tick, and is where a
	 * unit's interactions with the game world occur
	 */
	public void update()
	{
		// Update needs; at 30 ticks per second, it should take about 18 minutes
		// to go from full to zero
		this.hunger--;
		this.thirst--;
		this.fatigue--;

		// Check if needs are below threshold to require satisfaction
		
		//Starvation turned off until food is implemented
		if(this.fatigue <= NEED_THRESHOLD
				)
		{
			boolean isPresent = false;
			for(Task task : taskQueue)
			{
				if(task instanceof Rest)
					isPresent = true;
				/*if(isPresent)
					break;*/
			}
			if(!isPresent)
			{
				this.taskQueue.add(new Rest());
			}

			if(this.fatigue <= 0)
			{
				this.die();
				return;
			}
		}
		
		if(this.hunger <= NEED_THRESHOLD)
		{

			boolean isPresent = false;
			if (!taskQueue.isEmpty())
			{
				for(Task task : taskQueue)
				{
					if(task instanceof Eat)
						isPresent = true;
				}
			}
			
			if(!isPresent)
			{
				this.taskQueue.add(new Eat());
			}
			
			if(this.hunger <= 0)
			{
				this.die();
				return;
			}
		}
		
		if(this.thirst <= NEED_THRESHOLD)
		{
			
			boolean isPresent = false;
			if (!taskQueue.isEmpty())
			{
				for(Task task : taskQueue)
				{
					if(task instanceof Drink)
						isPresent = true;
					/*if(isPresent)
						break;*/
				}
			}
			
			if(!isPresent)
			{
				this.taskQueue.add(new Drink());
			}
			
			if(this.thirst <= 0)
			{
				this.die();
				return;
			}
		}

		this.behavior.act();
	}

	public void order(Command c)
	{
		
		if(c instanceof model.Build) {
			
			this.taskQueue.add(new BuildTask(((Build) c).getCoordinates().x, ((Build) c).getCoordinates().y, ((Build) c).getType()));
		}
		else if (c instanceof HarvestCommand)
		{
			this.taskQueue.add(new Harvest(((HarvestCommand)c).getResource(), Agent.RESOURCE_CARRY_CAP, true));
		}
		else if (c instanceof AssembleCommand)
		{
			this.taskQueue.add(new Assemble(((AssembleCommand)c).getItem()));
		}
		else if (c instanceof PlaceItemCommand)
		{
			this.taskQueue.add(new PlaceItem(((PlaceItemCommand) c).getX(), ((PlaceItemCommand) c).getY(), ((PlaceItemCommand) c).getItem()));
		}
	}

	/*
	 * For some interface tool for examining the condition of an agent
	 */
	public String getStatus()
	{
		if (dead) return name + " is dead";
		return this.behavior.statusMessage();
		
	}
	
	public String getUIStatus()
	{
		if (dead) return "Dead Already!";
		return this.behavior.statusMessage();
		
	}

	public String toDataString()
	{
		return "Agent::" + this.name + "(x:" + this.locationX + ",y:"
				+ this.locationY + ")\n\tHunger: " + this.hunger + " Thirst: "
				+ this.thirst + " Fatigue: " + this.fatigue + "\n\tTasks:"
				+ this.taskQueue + "Resources Carried: " + this.resourcesCarried + "\n\tItems: " + this.carriedItems + "\n\tStatus Message: " + this.getStatus()
				+ "\nGlobal Resource Pool: " + this.RESOURCE_POOL;
	}
	public String toString()
	{
		return this.name;
	}
	
	public ArrayList<String> getStatusList()
	{
		ArrayList<String> statusString = new ArrayList<String>();
		// 0 get status.
		statusString.add(this.getUIStatus());
		
		// 1-3 get live status.
		statusString.add("Hunger: " + this.hunger);
		statusString.add("Thirst: " + this.thirst);
		statusString.add("Fatigue: " + this.fatigue);
		
		// 4-8 get resources carried.
		statusString.add("Water \u00D7 " +  this.resourcesCarried.get(Resources.Water));
		statusString.add("Wood \u00D7 " +  this.resourcesCarried.get(Resources.Wood));
		statusString.add("Stone \u00D7 " +  this.resourcesCarried.get(Resources.Stone));
		statusString.add("Food \u00D7 " +  this.resourcesCarried.get(Resources.Food));
		statusString.add("Iron \u00D7 " +  this.resourcesCarried.get(Resources.Iron));
        
		// 9~ get items carried.
		for(PersonalItem aItem : this.carriedItems)
		{
			statusString.add(aItem.toString());
		}
		return statusString;
	}

	/*
	 * If it ever becomes important to ensure that an Agent's tasks are still
	 * properly prioritized, just call this method
	 */
	private void reprioritize()
	{
		this.taskQueue = new PriorityQueue<Task>(
				(Collection<? extends Task>) taskQueue);

	}

	/*
	 * This inner class represents a job an Agent intends to do, or is currently
	 * doing, depending on its place in the queue. Much of the implementation is
	 * dependent on concrete subclasses. This class is somewhat similar to the
	 * Command design pattern.
	 */
	protected abstract class Task implements Comparable<Task>, Serializable
	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1957286286324641960L;

		/*
		 * Because a task's priority is likely to be dependent on circumstantial
		 * factors, it should be calculated by method call Its calculation
		 * should be deterministic, so that it will produce the same result
		 * during the duration of a sort on the queue
		 */
		public abstract int getPriority();

		public int compareTo(Task other)
		{
			return this.getPriority() - other.getPriority();
		}

		abstract protected void setBehavior();
	}

	protected class Assemble extends Task
	{
		
		/**
		 * 
		 */
		private static final long serialVersionUID = -1873325472675188830L;
		private Craftable item;
		
		public Assemble(Craftable item2)
		{
			this.item = item2;
		}
		
		@Override
		public int getPriority() {
			// TODO Auto-generated method stub
			return 9;
		}

		@Override
		protected void setBehavior() {
			// TODO Auto-generated method stub
			Agent.this.behavior = new AssembleBehavior(item);
		}
		
		
		
		
	}
	
	//
	protected class PlaceItem extends Task
	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1164675364339828240L;
		int x, y;
		Item item;
		
		public PlaceItem( int x, int y, Item item)
		{
			this.x = x;
			this.y = y;
			this.item = item;
		}
		
		@Override
		public int getPriority() {
			
			return 4;
		}

		@Override
		protected void setBehavior() 
		{
			Agent.this.behavior = new PlaceItemBehavior(x, y, item);
			
		}
		
	}
	
	protected class PlaceItemBehavior extends Behavior
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -3696920149653007468L;
		int x, y;
		Item item;

		public PlaceItemBehavior(int x, int y, Item item) 
		{
			this.x = x;
			this.y = y;
			this.item = item;
		}

		@Override
		public String statusMessage() 
		{
			if("aeiou".contains(item.toString().substring(0,1)))
			{
				return Agent.this.name + " is putting down an " + item.toString() + ".";
			}
			else
			{
				return Agent.this.name + " is putting down a " + item.toString() + ".";
			}
		}

		@Override
		public void act() 
		{
			
			PersonalItem qualifyingObject = null;
			for(PersonalItem held : Agent.this.carriedItems)
			{
				if(held instanceof PlaceableInventoryItem)
				{
					if(((PlaceableInventoryItem)held).getItem() == item)
						qualifyingObject = held;
				}
			}
			
			if(qualifyingObject == null)
			{
				Agent.this.taskQueue.add(new Assemble(item));
				Agent.this.taskComplete();
				return;
			}
			
			if (this.path == null)
			{
				if(Agent.this.locationX == x && Agent.this.locationY == y)
				{
					Agent.this.enclosingMap.getTile(x, y).setItem(item);
					Agent.this.carriedItems.remove(qualifyingObject);
					if(item == Item.Container)
					{
						Agent.this.enclosingMap.increaseContainerCount();
					}
					Agent.this.taskComplete();
				}
				else
				{
					this.pathfind(x, y);
					if (path == null)
					{
						Agent.this.taskComplete();
						return;
					}
				}
			}
			else
			{
				if (this.path.isEmpty())
				{
					this.path = null;
				}
				else
				{
					Point point = path.poll();
					if (!Agent.this.move(point.x, point.y)) this.path = null;
				}
			}
			
		}
		
		
		
	}
	
	
	/*
	 * This task is added to an Agent's task queue when their Hunger need falls
	 * too low Its priority should be proportionate to how low the Agent's
	 * hunger has fallen
	 */
	protected class Eat extends Task
	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 2313010310137794344L;

		/*
		 * Priority currently scales only with current Hunger value; At very
		 * roughly half hunger (18000), it should be about priority 6 At the
		 * hunger level which is about 5 minutes until the stat decays into
		 * starvation (9000), priority is 3 Priority continues to improve as
		 * hunger intensifies
		 */
		@Override
		public int getPriority()
		{
			int x = Agent.this.hunger;
			int priority = (int) Math.floor((double) x * x
					/ (3000 * (.5 * x + 4500)));

			return priority;

		}

		@Override
		protected void setBehavior()
		{
			Agent.this.behavior = new HungryBehavior();

		}

	}
	
	protected class Drink extends Task
	{

		/**
		 * 
		 */
		private static final long serialVersionUID = -2443168579596787893L;

		@Override
		public int getPriority() {
			// TODO Auto-generated method stub
			return 3;
		}

		@Override
		protected void setBehavior() {
			
			Agent.this.behavior = new ThirstyBehavior();
		}
		
	}

    protected class Rest extends Task
    {

		private static final long serialVersionUID = 6635826682076059742L;

		@Override
        public int getPriority() {
            // TODO Auto-generated method stub
            return 3;
        }

        @Override
        protected void setBehavior() {

            Agent.this.behavior = new RestBehavior();
        }

    }
	

	protected class Harvest extends Task
	{
		
		private static final long serialVersionUID = 1333994102453107367L;
		Resources resource;
		boolean autoDeposit;
		Integer amount;
		
		public Harvest(Resources resource, Integer amount, boolean autoDeposit)
		{
			this.resource = resource;
			this.autoDeposit = autoDeposit;
			this.amount = amount;
		}
		
		
		@Override
		public int getPriority()
		{
			if(resource == Resources.Wood)
			{
				boolean hasAxe = false;
				for (PersonalItem item : Agent.this.carriedItems)
				{
					if(item instanceof Axe)
					{
						hasAxe = true;
					}
				}
				if (!hasAxe) return 12;
			}
			else if (resource == Resources.Water)
			{
				boolean hasBucket = false;
				for (PersonalItem item : Agent.this.carriedItems)
				{
					if(item instanceof Bucket)
					{
						hasBucket = true;
					}
				}
				if (!hasBucket) return 12;
			}
			return 6;
		}

		@Override
		protected void setBehavior()
		{
			Agent.this.behavior = new HarvestBehavior(resource, amount, autoDeposit);

		}

	}
	
	protected class Deposit extends Task
	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 4411783079249470733L;

		@Override
		public int getPriority() {

			return 10;
		}

		@Override
		protected void setBehavior() {
			
			Agent.this.behavior = new DepositBehavior();
		}
		
		
		
	}

	protected class BuildTask extends Task
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -5244242999725747890L;
		/**
		 * 
		 */
		//private static final long serialVersionUID ;
		private int x,y;
		private Building buildType;
		public BuildTask(int x, int y, Building building){
			this.x = x;
			this.y = y;
			this.buildType = building;
		}

		@Override
		public int getPriority()
		{
			boolean hasHammer = Agent.this.carriedItems.contains(PersonalItem.getInstance("hammer"));
			
			if(hasHammer)
				return 5;
			else
				return 12;
		}
		
		public Building getBuildType() {
			return buildType;
		}
		
		public Point getCoordinates() {
			return new Point(x,y);
		}

		@Override
		protected void setBehavior()
		{
			
			Agent.this.behavior = new BuildBehavior(x, y, buildType);
		}
		
	}
	/*
	 * An application of the Strategy design pattern, an agent has one Behavior
	 * at a time, produced from the head of their Task queue. Whenever the Agent
	 * is given opportunity to act, it consults its behavior, which then directs
	 * its action. Behaviors exist to accomplish a goal, and the actions they
	 * produce should lead to that behavior. It's an abstract class instead of
	 * an interface mostly just in case implementation detail starts
	 * overlapping; it could go either way
	 */

	protected static abstract class Pathfinder
	{
		private Pathfinder()
		{
		}

		public static Queue<Point> AStar(final Agent self, final int xdest,
				final int ydest)
		{
			class Node implements Comparable<Node>
			{
				int x, y;
				double g_score /* cost of current path to here */,
						f_score /* g score plus heuristic */;
				Map.MapTile tile;

				public Node(int x, int y, double g)
				{
					this.x = x;
					this.y = y;
					this.tile = self.enclosingMap.getTile(x, y);
					this.g_score = g;
					this.f_score = this.g_score
							+ (Math.abs(this.x - xdest) + Math.abs(this.y
									- ydest));
				}

				public void reG(double g)
				{
					this.g_score = g;
					this.f_score = this.g_score
							+ (Math.abs(this.x - xdest) + Math.abs(this.y
									- ydest));
				}

				@Override
				public int compareTo(Node o)
				{

					return (int) (this.f_score - o.f_score);
				}

				@Override
				public int hashCode()
				{
					return (this.x << 8) | (this.y);
				}

				@Override
				public boolean equals(Object o)
				{
					if(!(o instanceof Node))
						return false;
					else
					{
						if(this.tile == ((Node) o).tile)
							return true;
						else
							return false;
					}
				}

				public String toString()
				{
					return "Node: (" + this.x + ", " + this.y + ")["
							+ this.f_score + "]";
				}
			}

			HashSet<Node> closedset = new HashSet<Node>();
			PriorityQueue<Node> openset = new PriorityQueue<Node>();
			HashMap<Node, Node> came_from = new HashMap<Node, Node>();

			Node start = new Node(self.locationX, self.locationY, 0.0);
			openset.add(start);

			while(!openset.isEmpty())
			{
				// DEBUG:
				// System.out.println(openset.size());

				Node current = openset.remove();

				// DEBUG:
				// System.out.println(current);

				if(current.x == xdest && current.y == ydest)
				{
					// DEBUG:
					// System.out.println("Found it!");

					
					LinkedList<Point> path = new LinkedList<Point>();
					Node n = current;
					while(n != null && n != start)
					{
						// DEBUG:
						// System.out.println(n);

						Point point = new Point(n.x, n.y);
						path.add(0, point);
						n = came_from.get(n);
					}

					return (Queue<Point>) path;
				}
				closedset.add(current);

				// Check neighbors
				if(current.tile.north() != null
						&& current.tile.north().isPassable())
				{
					Node temp = (new Node(current.x, current.y - 1,
							current.g_score
									+ current.tile.north().getTerrain()
											.getPassCost()));
					// if(closedset.contains(temp)) continue;
					if(openset.contains(temp) && !closedset.contains(temp))
					{
						// Whichever one has the lower g score stays
						Iterator<Node> i = openset.iterator();
						boolean exit = false;
						while(!exit && i.hasNext())
						{
							Node n = i.next();
							if(temp.equals(n))
							{
								if(n.g_score <= temp.g_score)
									exit = true;
								else
								{
									openset.remove(n);
									openset.add(temp);
									came_from.put(temp, current);
									exit = true;
								}
							}
						}
					}
					else if(!closedset.contains(temp))
					{
						openset.add(temp);
						came_from.put(temp, current);
					}

				}
				if(current.tile.south() != null
						&& current.tile.south().isPassable())
				{
					Node temp = (new Node(current.x, current.y + 1,
							current.g_score
									+ current.tile.south().getTerrain()
											.getPassCost()));
					// if(closedset.contains(temp)) continue;
					if(openset.contains(temp) && !closedset.contains(temp))
					{
						// Whichever one has the lower g score stays
						Iterator<Node> i = openset.iterator();
						boolean exit = false;
						while(!exit && i.hasNext())
						{
							Node n = i.next();
							if(temp.equals(n))
							{
								if(n.g_score <= temp.g_score)
									exit = true;
								else
								{
									openset.remove(n);
									openset.add(temp);
									came_from.put(temp, current);
									exit = true;
								}
							}
						}
					}
					else if(!closedset.contains(temp))
					{
						openset.add(temp);
						came_from.put(temp, current);
					}
				}
				if(current.tile.east() != null
						&& current.tile.east().isPassable())
				{
					Node temp = (new Node(current.x + 1, current.y,
							current.g_score
									+ current.tile.east().getTerrain()
											.getPassCost()));
					// if(closedset.contains(temp)) continue;
					if(openset.contains(temp) && !closedset.contains(temp))
					{
						// Whichever one has the lower g score stays
						Iterator<Node> i = openset.iterator();
						boolean exit = false;
						while(!exit && i.hasNext())
						{
							Node n = i.next();
							if(temp.equals(n))
							{
								if(n.g_score <= temp.g_score)
									exit = true;
								else
								{
									openset.remove(n);
									openset.add(temp);
									came_from.put(temp, current);
									exit = true;
								}
							}
						}
					}
					else if(!closedset.contains(temp))
					{
						openset.add(temp);
						came_from.put(temp, current);
					}
				}
				if(current.tile.west() != null
						&& current.tile.west().isPassable())
				{
					Node temp = (new Node(current.x - 1, current.y,
							current.g_score
									+ current.tile.west().getTerrain()
											.getPassCost()));
					// if(closedset.contains(temp)) continue;
					if(openset.contains(temp) && !closedset.contains(temp))
					{
						// Whichever one has the lower g score stays
						Iterator<Node> i = openset.iterator();
						boolean exit = false;
						while(!exit && i.hasNext())
						{
							Node n = i.next();
							if(temp.equals(n))
							{
								if(n.g_score <= temp.g_score)
									exit = true;
								else
								{
									openset.remove(n);
									openset.add(temp);
									came_from.put(temp, current);
									exit = true;
								}
							}
						}
					}
					else if(!closedset.contains(temp))
					{
						openset.add(temp);
						came_from.put(temp, current);
					}
				}
				// DEBUG:
				// System.out.println("Loop end: " + openset);
			}// openset found empty

			return null;
		}

	}

	protected abstract class Behavior implements Serializable
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1594226214731258338L;
		// Most behaviors will involve getting places. When path isn't empty or
		// null, follow the path, otherwise do something else
		protected Queue<Point> path;

		/*
		 * This method returns a String describing the Agent's current behavior,
		 * in the pattern of
		 * "[Agent's name] is [description of ongoing behavior]" For instance,
		 * "John is eating" or "John is walking," or perhaps even
		 * "John is walking to get food" *
		 */
		abstract public String statusMessage();

		/*
		 * This method is called by the Agent's update method. It should perform
		 * a single, discrete action which helps bring about the behavior's
		 * goal.
		 */
		abstract public void act();
		
		/* Search for the nearest maptile containing a Container and path to it
		 * Path remains null if there are no containers
		 * 
		 */
		
		protected boolean containerAdjacent()
		{
			Map.MapTile current = Agent.this.enclosingMap.getTile(Agent.this.locationX, Agent.this.locationY);
			
			for(Map.MapTile m : new Map.MapTile[]{current, current.east(), current.west(), current.north(), current.south()})
			{
				if(m!= null && m.getItem(m) == Item.Container) return true;
			}
			return false;
			
		}
		
		protected boolean terrainAdjacent(Terrains terrain)
		{
			Map.MapTile current = Agent.this.enclosingMap.getTile(Agent.this.locationX, Agent.this.locationY);
						
			for(Map.MapTile m : new Map.MapTile[]{current.east(), current.west(), current.north(), current.south()})
			{
				if (m != null && m.getTerrain() == terrain) return true;
			}
			return false;
				
		}
		
		protected boolean resourceAdjacent(Resources resource)
		{
			Map.MapTile current = Agent.this.enclosingMap.getTile(Agent.this.locationX, Agent.this.locationY);
			
			for(Map.MapTile m : new Map.MapTile[]{current.east(), current.west(), current.north(), current.south()})
			{
				if (m != null && m.getResource() == resource) return true;
			}
			return false;
		}
		
		protected void pathToBuilding(String name)
		{
			Queue<Map.MapTile> tiles = new LinkedList<Map.MapTile>();
			Map.MapTile start = Agent.this.enclosingMap.getTile(Agent.this.locationX, Agent.this.locationY);
			tiles.add(start);
			HashSet<Map.MapTile> searched = new HashSet<Map.MapTile>();
			while(! tiles.isEmpty())
			{
				Map.MapTile current = tiles.poll();
				if(current.getBuilding() != null && current.getBuilding().getName().equalsIgnoreCase(name))
				{
					this.pathfind(current.getX(), current.getY());
					return;
				}
				else
				{
					searched.add(current);
					for(Map.MapTile m : new Map.MapTile[]{current.east(), current.west(), current.north(), current.south()})
					{
						if(m!= null && !searched.contains(m)) tiles.add(m);
						if(!searched.contains(m))searched.add(m);
					}
				}
			}
			
		}
		
		
		
		/* Paths to a tile adjacent to the specified terrain
		 * 
		 */
		protected void pathAdjacentToTerrain(Terrains terrain)
		{
			Queue<Map.MapTile> tiles = new LinkedList<Map.MapTile>();
			Map.MapTile start = Agent.this.enclosingMap.getTile(Agent.this.locationX, Agent.this.locationY);
			tiles.add(start);
			ArrayList<Map.MapTile> searched = new ArrayList<Map.MapTile>();
			while(! tiles.isEmpty())
			{
				Map.MapTile current = tiles.poll();
				for(Map.MapTile m : new Map.MapTile[]{current.east(), current.west(), current.north(), current.south()})
				{	
					if(m != null && m.getTerrain() == terrain)
					{
						this.pathfind(current.getX(), current.getY());
						return;
					}
				}
				
					for(Map.MapTile m : new Map.MapTile[]{current.east(), current.west(), current.north(), current.south()})
					{
						if(m != null && !searched.contains(m)) tiles.add(m);
						if(m != null && !searched.contains(m))searched.add(m);
					}
				
			}
			
			
		}
		
		protected void pathToResource(Resources resource)
		{
			Queue<Map.MapTile> tiles = new LinkedList<Map.MapTile>();
			Map.MapTile start = Agent.this.enclosingMap.getTile(Agent.this.locationX, Agent.this.locationY);
			tiles.add(start);
			HashSet<Map.MapTile> searched = new HashSet<Map.MapTile>();
			while(! tiles.isEmpty())
			{
				Map.MapTile current = tiles.poll();
				for(Map.MapTile m : new Map.MapTile[]{current.east(), current.west(), current.north(), current.south()})
				{
					if(m != null && m.getResource() == resource)
					{
						this.pathfind(current.getX(), current.getY());
						return;
					}
				}
				
				
					searched.add(current);
					for(Map.MapTile m : new Map.MapTile[]{current.east(), current.west(), current.north(), current.south()})
					{
						if(m != null && !searched.contains(m)) tiles.add(m);
						if(m!= null && !searched.contains(m))searched.add(m);
					}
				
			}
			
		}
		
		
		protected void pathToContainer()
		{
			Queue<Map.MapTile> tiles = new LinkedList<Map.MapTile>();
			Map.MapTile start = Agent.this.enclosingMap.getTile(Agent.this.locationX, Agent.this.locationY);
			tiles.add(start);
			HashSet<Map.MapTile> searched = new HashSet<Map.MapTile>();
			while(! tiles.isEmpty())
			{
				Map.MapTile current = tiles.poll();
				if(current.getItem(current) == Item.Container)
				{
					this.pathfind(current.getX(), current.getY());
					return;
				}
				else
				{
					searched.add(current);
					for(Map.MapTile m : new Map.MapTile[]{current.east(), current.west(), current.north(), current.south()})
					{
						if(m!= null && !searched.contains(m)) tiles.add(m);
						if(!searched.contains(m))searched.add(m);
					}
				}
			}
			
			
		}
		

		/*
		 * Sets this Behavior's Path field to a path to a given destination, or
		 * null if one cannot be found
		 */
		protected void pathfind(final int xdest, final int ydest)
		{
			this.path = Pathfinder.AStar(Agent.this, xdest, ydest);

		}

	}

	/*
	 * This is the behavior an Agent has when they have no other behavior. It
	 * should do its best to find something to do.
	 */

	protected class AssembleBehavior extends Behavior
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -4692232104078611417L;
		private Craftable item;
		
		public AssembleBehavior(Craftable item)
		{
			this.item = item;
		}
		
		@Override
		public String statusMessage() 
		{
			return Agent.this.name + " is making a " + item.toString() + ".";
			
		}

		@Override
		public void act() 
		{
			java.util.Map<Resources,Integer> cost = item.getCost();
			boolean haveEnough = true;
			
			for(Resources key : cost.keySet())
			{
				int carried = Agent.this.resourcesCarried.get(key);
				int needed = cost.get(key);
				if(needed > carried)
				{
					Agent.this.taskQueue.add(new ObtainResource(key, needed - carried));
					haveEnough = false;
					
				}
			}
			if(haveEnough)
			{
				for(Resources key : cost.keySet())
				{
					int carried = Agent.this.resourcesCarried.get(key);
					int needed = cost.get(key);
					Agent.this.resourcesCarried.put(key, carried - needed);
					
				}
				
				if(item instanceof PersonalItem)
					Agent.this.carriedItems.add((PersonalItem)item);
				else if (item instanceof Item)
					Agent.this.carriedItems.add(new PlaceableInventoryItem((Item)item));
			}
			else
			{
				boolean planning = false;
				for (Task task : Agent.this.taskQueue)
				{
					if(task instanceof Assemble)
					{
						if (((Assemble)task).item == item)
						{
							planning = true;
						}
					}
				}
				if(!planning)
				Agent.this.taskQueue.add(new Assemble(item));
			}
			
			Agent.this.taskComplete();
			return;
			
		}
		
		
		
	}
	
	protected class IdleBehavior extends Behavior
	{
		private Random random = new Random();
		/**
		 * 
		 */
		private static final long serialVersionUID = -3674486605500145316L;

		@Override
		public String statusMessage()
		{
			return Agent.this.name + " is idle.";
			//return "I have nothing to do.";
		}

		/*
		 * When idle, Agents should look for work to do Needs should be checked
		 * in the Agent.update() call, so it'd probably be redundant to do that
		 * here
		 */
		@Override
		public void act()
		{
			if(!Agent.this.taskQueue.isEmpty())
			{
				Agent.this.taskQueue.poll().setBehavior();
			}
			else
			{
				//Given nothing better to do, agents will meander
				if (random.nextDouble() < .3)
				{
					int x = Agent.this.locationX;
					int y = Agent.this.locationY;
					switch(random.nextInt(4))
					{
					case 0:
						Agent.this.move(x, y - 1);
						break;
					case 1:
						Agent.this.move(x + 1, y);
						break;
					case 2:
						Agent.this.move(x, y + 1);
					case 3:
						Agent.this.move(x - 1, y);
					}
				}
			}
			

		}

	}

	/*
	 * This behavior directs an Agent to improve their Hunger need If they have
	 * food, they will eat it If they do not have food,
	 */
	protected class HungryBehavior extends Behavior
	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 6704579734194161862L;

		/*
		 * The HungryBehavior status message should be dynamic in its deliered
		 * message; If the agent's available actions include eating food
		 * immediately, it should be of the form "[Agent] is eating" If the
		 * agent must walk to get food, the status message should be of the for
		 * "[Agent] is walking to get food" Etc
		 */
		
		private final static int FOOD_VALUE = 250;
		
		@Override
		public String statusMessage()
		{
			return Agent.this.name + " is trying to eat.";
			//return "I am finding some food.";
		}

	
		@Override
		public void act()
		{
			
			
			if(this.path == null)
			{
				
				// IF: Resources contains food
				if(Agent.this.resourcesCarried.get(Resources.Food) > 0)
				{
					boolean onKitchen = false;
					//IF:Not in a kitchen, try to get to one, but if you can't, keep going
					if(!(Agent.this.enclosingMap.getTile(Agent.this.locationX, Agent.this.locationY).getBuilding()!= null && (Agent.this.enclosingMap.getTile(Agent.this.locationX, Agent.this.locationY).getBuilding().getName().equalsIgnoreCase("kitchen"))))
					{
						this.pathToBuilding("kitchen");
					}
					else
					{
						onKitchen = true;
					}
					//If no pathing occurred, either because the agent is at a kitchen or can't get to one
					if(path == null)
					{ 
						// Decrease food carried; Increase Hunger
						Agent.this.increaseHunger(FOOD_VALUE);
						//If at a kitchen, get twice as much out of it
						if(onKitchen)
							Agent.this.increaseHunger(FOOD_VALUE);
						int cache = Agent.this.resourcesCarried.get(Resources.Food);
						Agent.this.resourcesCarried.put(Resources.Food, cache - 1);
						if(Agent.this.hunger >= Agent.NEED_REPLENISHMENT_CUTOFF)
						Agent.this.taskComplete();
						return;
					}
				}
				// Else: IF not in possession of food
				else 
				{
					
					new ObtainResource(Resources.Food, Math.abs(NEED_REPLENISHMENT_CUTOFF - Agent.this.hunger) / FOOD_VALUE).setBehavior();
				}
			}
			else
			{
				if (this.path.isEmpty())
				{
					this.path = null;
				}
				else
				{
					Point point = path.poll();
					if (!Agent.this.move(point.x, point.y)) this.path = null;
				}
				
			}
		}

	}
	
	protected class ThirstyBehavior extends Behavior
	{

		/**
		 * 
		 */
		private static final long serialVersionUID = -3045644339635345694L;
		private static final int WATER_VALUE = 250;
		@Override
		public String statusMessage() 
		{
			
			return Agent.this.name + " is going to get a drink.";
			//return "I am finding water to drink.";
		}

		@Override
		public void act() 
		{
			if (this.path == null)
			{
				if (Agent.this.resourcesCarried.get(Resources.Water) > 0)
				{
					Agent.this.increaseThirst(WATER_VALUE);
					int cache = Agent.this.resourcesCarried.get(Resources.Water);
					Agent.this.resourcesCarried.put(Resources.Water, cache - 1);
					if(Agent.this.thirst >= Agent.NEED_REPLENISHMENT_CUTOFF)
					Agent.this.taskComplete();
					return;
				}
				else if(!terrainAdjacent(Terrains.Water))
				{
					this.pathAdjacentToTerrain(Terrains.Water);
					//If can't get to water, switch behavior to getting water (presumably from a container)
					if (path == null)
						new ObtainResource(Resources.Water, (Agent.NEED_REPLENISHMENT_CUTOFF - Agent.this.thirst) / WATER_VALUE).setBehavior();
				}
				else
				{
					if (Agent.this.thirst < Agent.NEED_REPLENISHMENT_CUTOFF)
					{
						Agent.this.thirst += (WATER_VALUE / 2);
					}
					else
					{
						Agent.this.taskComplete();
						return;
					}
				}
			}
			else
			{
				
				if (this.path.isEmpty())
				{
					this.path = null;
				}
				else
				{
					Point point = path.poll();
					if (!Agent.this.move(point.x, point.y)) this.path = null;
					
				}
			}
			
		}
		
	}
	
	protected class HarvestBehavior extends Behavior
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -7468243362909818655L;
		Resources resource;
		boolean autoDeposit;
		Integer amount;
		
		//The behavior cares about what kind of resource it's looking for
		//The first argument is the type of resource to gather
		//The second argument is how much must be gathered at least; if null, it is collected just once
		//The third argument determines whether the agent will add a task to deposit their resources after finishing gathering
		public HarvestBehavior(Resources resourcetype, Integer amount, boolean autoDeposit)
		{
			this.resource = resourcetype;
			this.autoDeposit = autoDeposit;
			this.amount = amount;
			
		}
		
		

		@Override
		public String statusMessage() {
			return Agent.this.name + " is gathering " + resource + ".";
			//return "I am collecting " + resource + ".";
		}

		@Override
		public void act() 
		{
			int existing = Agent.this.resourcesCarried.get(resource);
			if(existing >= RESOURCE_CARRY_CAP)
			{
				if(autoDeposit)
				Agent.this.taskQueue.add(new Deposit());
				Agent.this.taskComplete();
				return;
			}
			
			if(resource == Resources.Water)
			{
				boolean hasBucket = false;
				for (PersonalItem item : Agent.this.carriedItems)
				{
					if(item instanceof Bucket)
					{
						hasBucket = true;
					}
				}
				if(!hasBucket)
				{
					Agent.this.behavior = new AssembleBehavior(PersonalItem.getInstance("Bucket"));
					Agent.this.taskQueue.add(new Harvest(resource, amount, autoDeposit));
					return;
				}
			}
			
			if(resource == Resources.Wood)
			{
				boolean hasAxe = false;
				for (PersonalItem item : Agent.this.carriedItems)
				{
					if(item instanceof Axe)
					{
						hasAxe = true;
					}
				}
				if(!hasAxe)
				{
					Agent.this.behavior = new AssembleBehavior(PersonalItem.getInstance("Axe"));
					Agent.this.taskQueue.add(new Harvest(resource, amount, autoDeposit));
					return;
				}
			}
			
			
			
			if(this.path == null)
			{
				Map.MapTile current = Agent.this.enclosingMap.getTile(Agent.this.locationX, Agent.this.locationY);
				if (this.resourceAdjacent(resource))
				//IF: currently on a tile with the right resources
				{
					
					Agent.this.resourcesCarried.put(resource, existing + 5);
					if(this.amount == null || Agent.this.resourcesCarried.get(resource) >= amount)
					{
						if(autoDeposit)
						Agent.this.taskQueue.add(new Deposit());
						Agent.this.taskComplete();
						return;
					}
				}
				else
				{
					this.pathToResource(resource);
					
					//If pathing failed; can't reach any resources; abandon task
					if (this.path == null) Agent.this.taskComplete();
					
				}
			}
			else
			{
				
				if (this.path.isEmpty())
				{
					this.path = null;
				}
				else
				{
					Point point = path.poll();
					if (!Agent.this.move(point.x, point.y)) this.path = null;
				}
			}
			
		}
		
	}
	
	
	
	
	
	protected class DepositBehavior extends Behavior
	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 7969422252420240003L;

		@Override
		public String statusMessage() {
			return Agent.this.name + " is depositing resources.";
			//return "I am depositing resources.";
		}

		@Override
		public void act() 
		{
			if(path == null)
			{
				if(!containerAdjacent())
				{
					this.pathToContainer();
					//If pathing failed; can't reach any containers; abandon task
					if (this.path == null) Agent.this.taskComplete();
					return;
				}
				else
				{
					for (Resources resource : Agent.this.resourcesCarried.keySet())
					{
						int amount = Agent.this.resourcesCarried.get(resource);
						int used = Agent.this.enclosingCollection.getGameState().putResource(resource, amount);
						Agent.this.resourcesCarried.put(resource, amount - used);
					}
					
					Agent.this.taskComplete();
					return;
				}
			}
			else
			{
				if (this.path.isEmpty())
				{
					this.path = null;
				}
				else
				{
					Point point = path.poll();
					if (!Agent.this.move(point.x, point.y)) this.path = null;
				}
			}
			
		}
		
	}
	

	protected class BuildBehavior extends Behavior
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1256660764479253692L;
		/**
 * 
 */
//		private static final long	serialVersionUID	= 328237935638642054L;
		private int						x, y;
		private Building				building;
		private int						buildTime;
		private String 				buildType;
		private String					statusMessage;

		public BuildBehavior(int x, int y, Building b)
		{
			this.building = b;
			this.x = x;
			this.y = y;
			statusMessage = Agent.this.name + " is trying to build.";//"I am trying to build.";
			this.buildType = building.getName();

			buildTime = building.getBuildTime();

		}

		@Override
		public String statusMessage()
		{
			return statusMessage;
		}

		@Override
		public void act()
		{
			
			/* Pathfind to the center tile of where the building is supposed to be built,
			 * The building process should be updated with each call of this agent's update method
			 * Perhaps eschew building time for Itr 1?
			 * When finished building, tell the map where the building should be put
			 */

			boolean hasHammer = false;

			for(Object item : Agent.this.carriedItems)
			{
				if (item instanceof PersonalItem.Hammer)
				{
					hasHammer = true;
				}
			}

			/*
			 * If an agent doesn't have a hammer, try to make one
			 * If we implement the ability to transfer items between agents, have
			 * them look for a hammer before trying to make one
			 */
			if (!hasHammer)
			{
				Agent.this.taskQueue.add(new Assemble(PersonalItem.getInstance("hammer")));
				Agent.this.taskQueue.add(new BuildTask(x, y, building));
				Agent.this.taskComplete();
				return;
			}

			/*
			 * Agent should only path to the building site when they have the necessary materials,
			 * i.e., hammer and other resources like wood
			 */
			if (hasHammer)
			{

				if (path == null)
				{
					if (Agent.this.locationX == x && Agent.this.locationY == y)
					{

						if (buildTime > 0)
						{
							if("aeiou".contains(this.buildType.substring(0,1)))
							{
								statusMessage = Agent.this.name + " is trying to build an " + this.buildType +  ".";
							}
							else
							{
								statusMessage = Agent.this.name + " is trying to build a " + this.buildType + "."; //  "I am trying to build a " + this.buildType + ".";
							}
							buildTime--;
						}

						else
						{
							// When the building is finished, the map gets told where the center tile of the
							// building is, and calculates the area of the building from there
							Collection<Map.MapTile> buildingTiles = Agent.this.enclosingMap
									.generateCollection(enclosingMap.getTile(x, y), building.getBlockWidth(), building.getBlockHeight());
							building.setTopography(buildingTiles);
							building.setTiles();
							Agent.this.taskComplete();
							return;
						}
					}
					else
					{
						statusMessage = Agent.this.name + " is moving to a building site."; // "I am moving to a building site.";
						this.pathfind(x, y);
						if (path == null)
						{
							Agent.this.taskComplete();
							return;
						}
					}
				}
				else
				{
					if (this.path.isEmpty())
					{
						this.path = null;
					}

					else
					{
						Point point = path.poll();
						if (!Agent.this.move(point.x, point.y))
							this.path = null;
					}
				}

			}
		}

	}

    //Untested and needs work for sure --Sanket
    protected class RestBehavior extends Behavior{

        /**
		 * 
		 */
		private static final long serialVersionUID = 596977995348214483L;

		@Override
        public String statusMessage() {
            return Agent.this.name + " is trying to rest."; // "I am trying to rest.";
        }

        @Override
        public void act() 
        {
           if (path == null)
           {
	        	Map.MapTile currentPos = enclosingMap.getTile(Agent.this.getLocationX(), Agent.this.getLocationY());
	            //int count = 0;
	            if(currentPos.getBuilding() != null && currentPos.getBuilding().getName().equals("Living Tent"))
	            {
	                 Agent.this.increaseFatigue(100);
	            }
	            else
	            {
	            	this.pathToBuilding("Living Tent");
	            	if (path == null) Agent.this.taskComplete();
	            }
	            if(Agent.this.fatigue > Agent.NEED_REPLENISHMENT_CUTOFF)
	            	Agent.this.taskComplete();
           }
           else
           {
        	   if (this.path.isEmpty())
				{
					this.path = null;
				}
				else
				{
					Point point = path.poll();
					if (!Agent.this.move(point.x, point.y)) this.path = null;
					
				}
           }
        }
    }
    
    protected class ObtainResource extends Task
    {
    	/**
		 * 
		 */
		private static final long serialVersionUID = 5527359076908357968L;
		private Resources resource;
    	private int amount;
    	public ObtainResource(Resources resource, int amount)
    	{
    		this.resource = resource;
    		this.amount = amount;
    	}

		@Override
		public int getPriority() 
		{
			if(resource == Resources.Wood)
			{
				boolean hasAxe = false;
				for (PersonalItem item : Agent.this.carriedItems)
				{
					if(item instanceof Axe)
					{
						hasAxe = true;
					}
				}
				if (!hasAxe)
					{
					
						return 9;
					}
			}
			else if (resource == Resources.Water)
			{
				boolean hasBucket = false;
				for (PersonalItem item : Agent.this.carriedItems)
				{
					if(item instanceof Bucket)
					{
						hasBucket = true;
					}
				}
				if (!hasBucket)
				{
					
					return 10;
				}
			}
			return 6;
		}

		@Override
		protected void setBehavior() 
		{
			if(Agent.this.RESOURCE_POOL.get(resource) >= amount && enclosingMap.getContainerCount() > 0)
			{
				Agent.this.behavior = new RetrieveResourceBehavior(resource, amount);
			}
			else
			{
				Agent.this.behavior = new HarvestBehavior(resource, amount, false);
			}
			
		}
    }
    
    protected class RetrieveResourceBehavior extends Behavior
    {
		private static final long serialVersionUID = 1577750673056807505L;
		private Resources resource;
    	private int amount;
    	
    	public RetrieveResourceBehavior(Resources resource, int amount)
    	{
    		this.resource = resource;
    		this.amount = amount;
    	}
    	
		@Override
		public String statusMessage() 
		{
			return Agent.this.name + " is getting " + resource + " from the stockpile."; // "I am getting " + resource + " from the stockpile.";
		}

		@Override
		public void act() 
		{
			if(path == null)
			{
				if(this.containerAdjacent())
				{
					int existing = Agent.this.RESOURCE_POOL.get(resource);
					if(existing >= amount)
					{
						Agent.this.RESOURCE_POOL.put(resource, existing - amount);
						Agent.this.resourcesCarried.put(resource, amount + Agent.this.resourcesCarried.get(resource));
					}
					Agent.this.taskComplete();
					
				}
				else
				{
					this.pathToContainer();
					if(path == null) Agent.this.taskComplete();
				}
			}
			else
			{
				if (this.path.isEmpty())
				{
					this.path = null;
				}
				else
				{
					Point point = path.poll();
					if (!Agent.this.move(point.x, point.y)) this.path = null;
				}
			}
		}
    	
    }


	public void increaseHunger(int i) 
	{
		int newHunger = this.hunger + i;
		if (newHunger > NEED_CEILING) this.hunger = NEED_CEILING;
		else this.hunger = (short)newHunger;
		

	}
	
	public void increaseThirst(int i)
	{
		int newThirst = this.thirst + i;
		if (newThirst > NEED_CEILING) this.thirst = NEED_CEILING;
		else this.thirst = (short)newThirst;
	}
	
	public void increaseFatigue(int i)
	{
		int newFatigue = this.fatigue + i;
		if (newFatigue > NEED_CEILING) this.fatigue = NEED_CEILING;
		else this.fatigue = (short)newFatigue;
	}
	
	public void setHunger(short s)
	{
		this.hunger = s;
	}
	
	public void setFatigue(short s)
	{
		this.fatigue = s;
	}
	
	

}
