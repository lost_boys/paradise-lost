/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package model;

import java.util.Random;

public class BlankTestGenerator implements TerrainGenerator
{

	@Override
	public void generateTerrain(Map map)
	{
		Random rand = new Random();
		for(int i = 0; i < map.getWidth(); i++)
		{
			for(int j = 0; j < map.getHeight(); j++)
			{
				int terrain = rand.nextInt(3);
				switch(terrain)
				{
					case 0:
						map.getTile(i, j).setTerrain(Terrains.Beach);
						break;
					case 1:
						map.getTile(i, j).setTerrain(Terrains.Rocky);
						break;
					case 2:
						map.getTile(i, j).setTerrain(Terrains.Jungle);
						break;
					case 3:
						map.getTile(i, j).setTerrain(Terrains.Water);
						break;
				}
				// map.getTile(i, j).setTerrain(Terrains.Beach);
			}
		}

	}

}
