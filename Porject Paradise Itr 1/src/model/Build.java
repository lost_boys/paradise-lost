/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package model;

import java.awt.*;

@SuppressWarnings("serial")
public class Build extends Command
{
	private Building	type;
	private int		x, y; // location of building's center tile

	public Build(Building buildingType, int x, int y)
	{
		this.completed = false;
		this.type = buildingType;
		this.x = x;
		this.y = y;

	}

	public Building getType()
	{
		return type;
	}

	public Point getCoordinates()
	{
		return new Point(x, y);
	}

}
