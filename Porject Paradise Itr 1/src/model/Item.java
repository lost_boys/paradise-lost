/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package model;

import java.io.Serializable;
import java.util.EnumMap;
import java.util.Map;

//The class for items that can be placed in the world
public enum Item implements Craftable, Serializable
{

	Container(crateCost());

	Map<Resources, Integer> cost;

	private Item(java.util.Map<Resources, Integer> cost)
	{
		this.cost = cost;
	}

	public String consoleToString(Item e)
	{
		if(e == Container)
		{
			return "U";
		}

		return "I'm not working";
	}

	private static Map<Resources, Integer> crateCost()
	{
		Map<Resources, Integer> c = new EnumMap<Resources, Integer>(
				Resources.class);
		c.put(Resources.Wood, 5);

		return c;
	}

	@Override
	public Map<Resources, Integer> getCost()
	{
		return cost;
	}

}
