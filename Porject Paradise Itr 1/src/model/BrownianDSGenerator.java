/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package model;

import model.Map.MapTile;

import java.util.HashMap;
import java.util.Random;

public class BrownianDSGenerator implements TerrainGenerator
{
	private static Random					random	= new Random(System.currentTimeMillis());
	private HashMap<Terrains, Integer>	terrain	= new HashMap<Terrains, Integer>();

	@Override
	public void generateTerrain(Map map)
	{
		/*
		 * Iterations are the number of "recursions" it takes to
		 * reach the smallest square
		 */
		int iterations = 0;
		int size = map.getHeight();
		while (size > 1)
		{
			iterations++;
			size >>= 1;
		}

		int[][] heightMap;

		/*
		 * TODO: Make terrain placement range more dynamic so that
		 * if we want a high variability or a different seed value,
		 * we don't end up with a bunch of water tiles.
		 */

		double waterFraction;
		double rockyFraction;
		double beachFraction;
		double jungleFraction;
		int numLandTiles;
		int mapsSacrificed = 0;
		long startTime = System.currentTimeMillis();
		long endTime;

		/*
		 * Generate the map.  If the map has too many water tiles or rocky tiles,
		 * scrap it and generate a new one.
		 */
		do
		{
			terrain.put(Terrains.Water, 0);
			terrain.put(Terrains.Beach, 0);
			terrain.put(Terrains.Jungle, 0);
			terrain.put(Terrains.Rocky, 0);
			/*
			 * First, get the terrain heightmap from the Diamond-Square algorithm
			 * and assign terrain values according to the values on that heightmap
			 */
			heightMap = makeHeightMap(iterations, 50, 100);
			for(int i = 0; i < map.getHeight(); i++)
			{
				for(int j = 0; j < map.getWidth(); j++)
				{
					int value = heightMap[i][j];
					if (value > 99)
						value = 99;
					else if (value < 0)
						value = 0;

					if (value < 0)
					{
						map.getTile(i, j).setTerrain(Terrains.Water);
						terrain.put(Terrains.Water, terrain.get(Terrains.Water) + 1);
					}
					else if (value < 22)
					{
						map.getTile(i, j).setTerrain(Terrains.Beach);
						terrain.put(Terrains.Beach, terrain.get(Terrains.Beach) + 1);
					}
					else if (value < 66)
					{
						map.getTile(i, j).setTerrain(Terrains.Jungle);
						terrain.put(Terrains.Jungle, terrain.get(Terrains.Jungle) + 1);
					}
					else if (value >= 66)
					{
						map.getTile(i, j).setTerrain(Terrains.Rocky);
						terrain.put(Terrains.Rocky, terrain.get(Terrains.Rocky) + 1);
					}
					else
						System.out.println("Heightmap value out of bounds: " + heightMap[i][j]);
				}
			}
					
			/*
			 * Set the terrain of the edge of the map to be water 
			 */
			for(int i = 0; i < map.getWidth(); i++)
			{
				map.getTile(i, 0).setTerrain(Terrains.Water);
				map.getTile(0, i).setTerrain(Terrains.Water);
				map.getTile(i, 1).setTerrain(Terrains.Water);
				map.getTile(1, i).setTerrain(Terrains.Water);
				map.getTile(i, map.getWidth() - 1).setTerrain(Terrains.Water);
				map.getTile(map.getHeight() - 1, i).setTerrain(Terrains.Water);
				map.getTile(i, map.getWidth() - 2).setTerrain(Terrains.Water);
				map.getTile(map.getHeight() - 2, i).setTerrain(Terrains.Water);
			}

			/*
			 * Next, generate the coastline using Brownian motion.  This doesn't actually affect
			 * the map yet, we're just getting the blueprint for the coastline.
			 */
			boolean[][] coast = new boolean[map.getWidth()][map.getHeight()];
			coast = this.brownianCoastline(0, 0, 0, 0, 0.5, 15, coast);

			/*
			 * Unfortunately, the terrain heightmap and the coastline won't match up, so there are probably
			 * swathes of terrain lying outside the coastline blueprint.  We need to trim those away.
			 * Unfortunately (again), the function used above tends to cross over itself, creating
			 * "internal coastlines".  Because of this, we can't just follow the coastline and set
			 * all the tiles on one side to water, because that could really mess with the interior of the island.
			 * Right now, we start at the edge of the map and move inward in straight lines along the x and y axes,
			 * converting terrain to water as we go, until we hit the coastline.  This naive approach runs
			 * into problems with fishbowl-shaped concavities because the scan lines miss the curves at the
			 * edge of the bowl, so they leave ugly straight-edged artifacts behind where there should be bay-like
			 * areas.
			 * TODO: Fix this.
			 */
			for(int x = 0; x < map.getHeight(); x++)
			{
				/*
				 * Start at the north, moving south 
				 */
				int y = 0;
				while (y < map.getHeight() && !coast[x][y])
				{
					if(!map.getTile(x, y).getTerrain().equals(Terrains.Water)){
						terrain.put(Terrains.Water, terrain.get(Terrains.Water) + 1);
						map.getTile(x, y).setTerrain(Terrains.Water);
					}
					if (y + 1 < map.getHeight() && coast[x][y + 1])
						this.randomWalk(map.getArray(), x, y, coast);
					y++;
				}
				/*
				 * Start at the south, moving north
				 */
				y = map.getWidth() - 1;
				while (y > 0 && !coast[x][y])
				{
					if(!map.getTile(x, y).getTerrain().equals(Terrains.Water)){
						terrain.put(Terrains.Water, terrain.get(Terrains.Water) + 1);
						map.getTile(x, y).setTerrain(Terrains.Water);
					}
					if (y - 1 > 0 && coast[x][y - 1])
						this.randomWalk(map.getArray(), x, y, coast);
					y--;
				}

			}

			for(int y = 0; y < map.getHeight(); y++)
			{
				/*
				 * Start at the west, moving east
				 */
				int x = 0;
				while (x < map.getWidth() && !coast[x][y])
				{
					if(!map.getTile(x, y).getTerrain().equals(Terrains.Water)){
						terrain.put(Terrains.Water, terrain.get(Terrains.Water) + 1);
						map.getTile(x, y).setTerrain(Terrains.Water);
					}
					if (x + 1 < map.getWidth() && coast[x + 1][y])
						this.randomWalk(map.getArray(), x, y, coast);
					x++;
				}
				/*
				 * Start at the east, moving west
				 */
				x = map.getHeight() - 1;
				while (x > 0 && !coast[x][y])
				{
					if(!map.getTile(x, y).getTerrain().equals(Terrains.Water)){
						terrain.put(Terrains.Water, terrain.get(Terrains.Water) + 1);
						map.getTile(x, y).setTerrain(Terrains.Water);
					}
					if (x - 1 > 0 && coast[x - 1][y])
						this.randomWalk(map.getArray(), x, y, coast);
					x--;
				}
			}
			numLandTiles = terrain.get(Terrains.Rocky) + terrain.get(Terrains.Jungle) + terrain.get(Terrains.Jungle);
			waterFraction = (double) terrain.get(Terrains.Water)
					/ numLandTiles;
			rockyFraction = (double) terrain.get(Terrains.Rocky)
					/ numLandTiles;
			beachFraction = (double) terrain.get(Terrains.Beach)
					/ numLandTiles;
			jungleFraction = (double) terrain.get(Terrains.Jungle)
					/ numLandTiles;
			mapsSacrificed++;
//			System.out.println(waterFraction + ", " + terrain.get(Terrains.Water));
			//TODO: Find an appropriate range of values for ideal maps
		} while (waterFraction > 0.4 || rockyFraction < 0.15 || beachFraction < 0.05 || beachFraction > 0.25 || jungleFraction < 0.05);
		endTime = System.currentTimeMillis();
		double timeTaken = (endTime - startTime) / 1000;
		System.out.println("You have sacrificed " + mapsSacrificed
				+ " maps to the Random Number God!");
		System.out.println("Generating this map took " + timeTaken + " seconds");

	}

	/**
	 * This method uses the seed value to initialize the four corners of the map.
	 * The variation creates randomness in the map. The size of the array is
	 * determined by the amount of iterations (i.e. 1 iteration -> 3x3 array, 2
	 * iterations -> 5x5 array, etc.).
	 * 
	 * @param iterations
	 *           the amount of iterations to do (minimum of 1)
	 * @param seed
	 *           the starting value
	 * @param variation
	 *           the amount of randomness in the height map (minimum of 0)
	 * @return a height map in the form of a 2-dimensional array containing
	 *         integer values or null if the arguments are out of range
	 */
	public static int[][] makeHeightMap(int iterations, int seed, int variation)
	{
		if (iterations < 1 || variation < 0)
		{
			return null;
		}

		int size = (1 << iterations) + 1;
		int[][] map = new int[size][size];
		final int maxIndex = map.length - 1;

		// seed the corners
		map[0][0] = seed;
		map[0][maxIndex] = seed;
		map[maxIndex][0] = seed;
		map[maxIndex][maxIndex] = seed;

		for(int i = 1; i <= iterations; i++)
		{
			int minCoordinate = maxIndex >> i;// Minimum coordinate of the
															// current map spaces
			size = minCoordinate << 1;// Area surrounding the current place in
												// the map

			diamondStep(minCoordinate, size, map, variation);
			squareStepEven(minCoordinate, map, size, maxIndex, variation);
			squareStepOdd(map, size, minCoordinate, maxIndex, variation);

			variation = variation >> 1;// Divide variation by 2
		}

		return map;
	}

	/**
	 * Calculates average values of four corner values taken from the smallest
	 * possible square.
	 * 
	 * @param minCoordinate
	 *           the x and y coordinate of the first square center
	 * @param size
	 *           width and height of the squares
	 * @param map
	 *           the height map to fill
	 * @param variation
	 *           the randomness in the height map
	 */
	private static void diamondStep(int minCoordinate, int size, int[][] map, int variation)
	{
		for(int x = minCoordinate; x < (map.length - minCoordinate); x += size)
		{
			for(int y = minCoordinate; y < (map.length - minCoordinate); y += size)
			{
				int left = x - minCoordinate;
				int right = x + minCoordinate;
				int up = y - minCoordinate;
				int down = y + minCoordinate;

				// the four corner values
				int val1 = map[left][up]; // upper left
				int val2 = map[left][down]; // lower left
				int val3 = map[right][up]; // upper right
				int val4 = map[right][down];// lower right

				calculateAndInsertAverage(val1, val2, val3, val4, variation, map, x, y);
			}
		}
	}

	/**
	 * Calculates average values of four corner values taken from the smallest
	 * possible diamond. This method calculates the values for the even rows,
	 * starting with row 0.
	 * 
	 * @param minCoordinate
	 *           the x-coordinate of the first diamond center
	 * @param map
	 *           the height map to fill
	 * @param size
	 *           the length of the diagonals of the diamonds
	 * @param maxIndex
	 *           the maximum index in the array
	 * @param variation
	 *           the randomness in the height map
	 */
	private static void squareStepEven(int minCoordinate, int[][] map, int size,
			int maxIndex, int variation)
	{
		for(int x = minCoordinate; x < map.length; x += size)
		{
			for(int y = 0; y < map.length; y += size)
			{
				if (y == maxIndex)
				{
					map[x][y] = map[x][0];
					continue;
				}

				int left = x - minCoordinate;
				int right = x + minCoordinate;
				int down = y + minCoordinate;
				int up = 0;

				if (y == 0)
				{
					up = maxIndex - minCoordinate;
				}
				else
				{
					up = y - minCoordinate;
				}

				// the four corner values
				int val1 = map[left][y]; // left
				int val2 = map[x][up]; // up
				int val3 = map[right][y];// right
				//				System.out.println(x + ", " + down);
				int val4 = map[x][down]; // down

				calculateAndInsertAverage(val1, val2, val3, val4, variation, map, x, y);
			}
		}
	}

	/**
	 * Calculates average values of four corner values taken from the smallest
	 * possible diamond. This method calculates the values for the odd rows,
	 * starting with row 1.
	 * 
	 * @param minCoordinate
	 *           the x-coordinate of the first diamond center
	 * @param map
	 *           the height map to fill
	 * @param size
	 *           the length of the diagonals of the diamonds
	 * @param maxIndex
	 *           the maximum index in the array
	 * @param variation
	 *           the randomness in the height map
	 */
	private static void squareStepOdd(int[][] map, int size, int minCoordinate,
			int maxIndex, int variation)
	{
		for(int x = 0; x < map.length; x += size)
		{
			for(int y = minCoordinate; y < map.length; y += size)
			{
				if (x == maxIndex)
				{
					map[x][y] = map[0][y];
					continue;
				}

				int left = 0;
				int right = x + minCoordinate;
				int down = y + minCoordinate;
				int up = y - minCoordinate;

				if (x == 0)
				{
					left = maxIndex - minCoordinate;
				}
				else
				{
					left = x - minCoordinate;
				}

				// the four corner values
				int val1 = map[left][y]; // left
				int val2 = map[x][up]; // up
				int val3 = map[right][y];// right
				int val4 = map[x][down]; // down

				calculateAndInsertAverage(val1, val2, val3, val4, variation, map, x, y);
			}
		}
	}

	/**
	 * Calculates an average value, adds a variable amount to that value and
	 * inserts it into the height map.
	 * 
	 * @param val1
	 *           first of the values used to calculate the average
	 * @param val2
	 *           second of the values used to calculate the average
	 * @param val3
	 *           third of the values used to calculate the average
	 * @param val4
	 *           fourth of the values used to calculate the average
	 * @param variation
	 *           adds variation to the average value
	 * @param map
	 *           the height map to fill
	 * @param x
	 *           the x-coordinate of the place to fill
	 * @param y
	 *           the y-coordinate of the place to fill
	 */
	private static void calculateAndInsertAverage(int val1, int val2, int val3, int val4,
			int variation, int[][] map, int x, int y)
	{
		int avg = (val1 + val2 + val3 + val4) >> 2;// average
		int var = (int) ((Math.random() * ((variation << 1) + 1)) - variation);
		map[x][y] = avg + var;
	}

	/**
	 * This method is used to generate the coastline of the island. How it works:
	 * Calculates the midpoint of the line between (x0,y0) and (x1,y1), and moves
	 * it by a random amount, then the midpoint of the line between (x0,y0) and
	 * (xmid, ymid), etc., N times (a Gaussian distribution works best for this,
	 * purely random/pseudo-random numbers tend to generate stretched islands
	 * with very jagged coastlines).
	 */
	public boolean[][] brownianCoastline(double x0, double y0, double x1, double y1,
			double var, int N, boolean[][] map)
	{
		if (N == 0)
		{
			int x = (int) (Math.abs(x0) * (map.length - 1));
			int y = (int) (Math.abs(y0) * (map.length - 1));
			if (x >= map.length)
				x = map.length - 1;
			if (y >= map.length)
				y = map.length - 1;
			//			System.out.println(x0 + ", " + y0);
			//			System.out.println(x + ", " + y);
			map[x][y] = true;
			return map;
		}
		double xGaussian = Math.sqrt(var) * random.nextGaussian();
		double yGaussian = Math.sqrt(var) * random.nextGaussian();
		double xmid = 0.5 * (x0 + x1) + xGaussian;
		double ymid = 0.5 * (y0 + y1) + yGaussian;

		brownianCoastline(x0, y0, xmid, ymid, var / 3.5, N - 1, map); // 3 seems to be a good value
		brownianCoastline(xmid, ymid, x1, y1, var / 3.5, N - 1, map);
		return map;
	}

	/*
	 * This algorithm moves in a random direction, converting tiles to water
	 * as it goes, while trying to avoid the coastline. This is primarily
	 * used to break up previously mentioned artifacts on the coastline.
	 */
	public void randomWalk(MapTile[][] map, int x, int y, boolean[][] coast)
	{
		int steps = 0;
		while (Math.abs(x) < map.length - 1 && Math.abs(y) < map.length - 1 && x > 0
				&& y > 0 && steps < 200)
		{

			if (coast[x - 1][y] && !map[x - 1][y].getTerrain().equals(Terrains.Water))
			{
				map[x - 1][y].setTerrain(Terrains.Beach);
			}
			if (coast[x + 1][y] && !map[x + 1][y].getTerrain().equals(Terrains.Water))
			{
				map[x + 1][y].setTerrain(Terrains.Beach);
			}
			if (coast[x][y - 1] && !map[x][y - 1].getTerrain().equals(Terrains.Water))
			{
				map[x][y - 1].setTerrain(Terrains.Beach);
			}
			if (coast[x][y + 1] && !map[x][y + 1].getTerrain().equals(Terrains.Water))
			{
				map[x][y + 1].setTerrain(Terrains.Beach);
			}

			double r = random.nextDouble();
			while (r < 1)
			{
				r = random.nextDouble();
				if (r < 0.25 && !coast[x - 1][y])
				{
					x--;
					break;
				}
				else if (r < 0.50 && !coast[x + 1][y])
				{
					x++;
					break;
				}
				else if (r < 0.75 && !coast[x][y - 1])
				{
					y--;
					break;
				}
				else if (r < 1.00 && !coast[x][y + 1])
				{
					y++;
					break;
				}
//				System.out.println(x + ", " + y);
			}
			//			System.out.println(x + ", " + y);
			if (!map[x][y].getTerrain().equals(Terrains.Water))
			{
				if (map[x][y].getTerrain().equals(Terrains.Beach))
					terrain.put(Terrains.Beach, terrain.get(Terrains.Beach) - 1);
				else if (map[x][y].getTerrain().equals(Terrains.Jungle))
					terrain.put(Terrains.Jungle, terrain.get(Terrains.Jungle) - 1);
				else if (map[x][y].getTerrain().equals(Terrains.Rocky))
					terrain.put(Terrains.Rocky, terrain.get(Terrains.Rocky) - 1);

				map[x][y].setTerrain(Terrains.Water);
				terrain.put(Terrains.Water, terrain.get(Terrains.Water) + 1);
			}
			steps++;
		}
		//		System.out.println(steps);
	}
}
