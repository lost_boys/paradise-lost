/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */


package model;

import java.io.Serializable;

public enum Terrains implements Serializable
{
	Beach(0, true), Rocky(2, true), Jungle(1, true), Water(Integer.MAX_VALUE,
			false);

	// movement cost for each type of terrain
	private int passCost;
	private boolean isPassable;

	Terrains(int passCost, boolean passable)
	{
		this.passCost = passCost;
		this.isPassable = passable;
	}

	public int getPassCost()
	{
		return passCost;
	}

	public boolean isPassable()
	{
		return isPassable;
	}

	public String consoleToString(Terrains t)
	{
		if(t == Beach)
		{
			return ".";
		}
		else if(t == Water)
		{
			return "~";
		}
		else if(t == Jungle)
		{
			return "#";
		}
		return "*";
	}

}
