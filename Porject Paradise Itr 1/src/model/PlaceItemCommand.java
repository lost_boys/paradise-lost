/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */


package model;

@SuppressWarnings("serial")
public class PlaceItemCommand extends Command 
{
	int x, y;
	Item item;
	public PlaceItemCommand(int x, int y, Item item)
	{
		this.x = x;
		this.y = y;
		this.item = item;
	}
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	public Item getItem() {
		return item;
	}
	
	
	
	
}
