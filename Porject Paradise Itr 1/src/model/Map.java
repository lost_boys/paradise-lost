/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package model;

import java.awt.*;
import java.io.Serializable;
import java.util.*;
import java.util.List;

@SuppressWarnings("deprecation")
public class Map implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -4168221100897474537L;

	private final MapTile[][] map;
	/*
	 * Note: Maps must have width and height of 1+2^n for the Diamond-Square
	 * algorithm to work
	 */
	private final int MAP_WIDTH;
	private final int MAP_HEIGHT;
	private final int RESOURCE_PROBABILITY = 100;
	private Point planeLocation;
	private int containerCount;

	private List<Building> buildings;

	public List<Item>	itemList;

	public List<Building> getBuildingList()
	{
		HashSet<Building> buildings = new HashSet<Building>();
		for(int x = 0; x < map.length; x++)
		{
			for(int y = 0; y < map[x].length; y++)
			{
				Building building = map[x][y].getBuilding();
				if(building != null)
					buildings.add(building);
			}
		}

		List<Building> buildingList = new LinkedList<Building>();
		for(Building building : buildings)
			buildingList.add(building);

		return buildingList;
	}

	public Map(int width, int height)
	{
		MAP_WIDTH = width;
		MAP_HEIGHT = height;
		this.containerCount = 0;
		this.buildings = new ArrayList<Building>();
		this.itemList = new ArrayList<Item>();
		map = new MapTile[MAP_WIDTH][MAP_HEIGHT];
		// Each tile starts off with no terrain type, as this is presumably
		// determined in the randomization stage
		for(int x = 0; x < map.length; x++)
		{
			for(int y = 0; y < map[x].length; y++)
			{

				map[x][y] = new MapTile(x, y);
			}
		}
		// planeLocation = randomLocation();
		makeConnections();
		generateMap();
	}
	
	public List<Item> getItemList() {
		return this.itemList;
	}

	// Initiates the map generation process

	// **4/20- changed BlankTestGenerator to SimpleMap in order to get Itr 1
	// requirements tested
	private void generateMap()
	{
		TerrainGenerator gen = new BrownianDSGenerator();
		gen.generateTerrain(this);
		grantResources(this, RESOURCE_PROBABILITY);
	}

	public void generateMap(TerrainGenerator gen)
	{
		gen.generateTerrain(this);
		grantResources(this, RESOURCE_PROBABILITY);
	}

	public void makeConnections()
	{
		for(int x = 0; x < map.length; x++)
		{
			for(int y = 0; y < map[x].length; y++)
			{
				map[x][y].makeConnections();
			}
		}

	}

	public boolean hasDock()
	{
		for(Building building : buildings)
			if(building.getName().equals("Dock"))
				return true;

		return false;
	}

	private void grantResources(Map map, int kindness)
	{
		// Using one Random object improves performance without decreasing
		// randomness
		Random r = new Random();
		for(int i = 0; i < map.getWidth(); i++)
		{
			for(int j = 0; j < map.getWidth(); j++)
			{
				int randomSource = r.nextInt(1000);

				if(map.getTile(i, j).getTerrain().equals(Terrains.Water))
				{
					map.getTile(i, j).setResource(Resources.Water);
				}
				else if(map.getTile(i, j).getTerrain().equals(Terrains.Jungle))
				{
					if(randomSource < kindness)
					{
						if(r.nextInt(100) < 60)
						{
							map.getTile(i, j).setResource(Resources.Wood);
						}
						else
						{
							map.getTile(i, j).setResource(Resources.Food);
						}
					}
				}
				else if(map.getTile(i, j).getTerrain().equals(Terrains.Rocky))
				{
					if(randomSource < kindness)
					{
						map.getTile(i, j).setResource(Resources.Stone);
					}
				}
			}
			// TODO: Plane Iron
		}
		int aX = new Random().nextInt(MAP_WIDTH - 5) + 6, aY = new Random()
				.nextInt(MAP_HEIGHT - 5) + 6;
		do
		{
			aX = new Random().nextInt(MAP_WIDTH - 10) + 6;
			aY = new Random().nextInt(MAP_HEIGHT - 10) + 6;

			planeLocation = new Point(aY, aX);

			for(int j = 0; j < 3; j++)
			{
				for(int i = 0; i < 3; i++)
				{
					map.getTile(aX + i, aY + j).setResource(Resources.Iron);
				}
			}
		}
		while((map.getTile(aX, aY).getTerrain().equals(Terrains.Water)
				|| map.getTile(aX + 1, aY).getTerrain().equals(Terrains.Water)
				|| map.getTile(aX + 1, aY + 1).getTerrain()
						.equals(Terrains.Water)
				|| map.getTile(aX + 1, aY + 2).getTerrain()
						.equals(Terrains.Water)
				|| map.getTile(aX + 2, aY).getTerrain().equals(Terrains.Water)
				|| map.getTile(aX + 2, aY + 1).getTerrain()
						.equals(Terrains.Water)
				|| map.getTile(aX + 2, aY + 2).getTerrain()
						.equals(Terrains.Water)
				|| map.getTile(aX, aY + 1).getTerrain().equals(Terrains.Water) || map
				.getTile(aX, aY + 2).getTerrain().equals(Terrains.Water))

		/*
		 * || (map.getTile(aX - 1, aY - 1).getResource().equals(Terrains.Water)
		 * || map.getTile(aX - 1, aY).getResource().equals(Terrains.Water) ||
		 * map.getTile(aX - 1, aY + 1).getResource().equals(Terrains.Water) ||
		 * map.getTile(aX - 1, aY + 2).getResource().equals(Terrains.Water) ||
		 * map.getTile(aX - 1, aY + 3).getResource().equals(Terrains.Water)
		 * 
		 * || map.getTile(aX + 3, aY - 1).getResource().equals(Terrains.Water)
		 * || map.getTile(aX + 3, aY).getResource().equals(Terrains.Water) ||
		 * map.getTile(aX + 3, aY + 1).getResource().equals(Terrains.Water) ||
		 * map.getTile(aX + 3, aY + 2).getResource().equals(Terrains.Water) ||
		 * map.getTile(aX + 3, aY + 3).getResource().equals(Terrains.Water)
		 * 
		 * || map.getTile(aX, aY - 1).getResource().equals(Terrains.Water) ||
		 * map.getTile(aX + 1, aY - 1).getResource().equals(Terrains.Water) ||
		 * map.getTile(aX + 2, aY - 1).getResource().equals(Terrains.Water)
		 * 
		 * || map.getTile(aX, aY + 3).getResource().equals(Terrains.Water) ||
		 * map.getTile(aX + 1, aY + 3).getResource().equals(Terrains.Water) ||
		 * map.getTile(aX + 2, aY + 3).getResource().equals(Terrains.Water) )
		 */);
	}

	public Point randomLocation()
	{
		int aX = new Random().nextInt(MAP_WIDTH - 5) + 1, aY = new Random()
				.nextInt(MAP_HEIGHT - 5) + 1;
		return new Point(aX, aY);
	}

	public Point getPlaneLocation()
	{
		return planeLocation;
	}

	// Call this when adding a container so that the resource pool cap increases
	public void increaseContainerCount()
	{
		this.containerCount++;
	}

	// I don't know if they can be removed, but if they can, call this when a
	// container is removed
	public void decreaseContainerCount()
	{
		if(this.containerCount > 0)
			this.containerCount--;
	}

	public int getContainerCount()
	{
		return containerCount;
	}

	public int getWidth()
	{
		return map.length;
	}

	public int getHeight()
	{
		return map[0].length;
	}

	public MapTile getTile(int x, int y)
	{
		return map[x][y];
	}

	public boolean passableAt(int x, int y)
	{
		if(x < 0 || x >= map.length || y < 0 || y >= map[x].length)
			return false;
		return map[x][y].isPassable();
	}

	public class MapTile implements Serializable
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1454243274737310406L;

		private final int x;
		private final int y;
		private Item itemInPlace;
		private Agent agentInPlace;
		private Building building;
		private Terrains terrain;
		private Resources resource;
		// private boolean isPassable;
		/*
		 * These are transient because when saving the game, the output stream
		 * seems to get stuck in an infinite recursion when it tries to save
		 * these fields
		 */
		private transient MapTile north, south, east, west;
		/*
		 * This variable is used by BrownianDSGenerator to generate the
		 * coastline of the island. It's transient because when we save the map,
		 * it should have its coastline already there. Probably unnecessary, due
		 * to the size of a boolean compared to everything else we're saving.
		 */
		private transient boolean isCoastline = false;

		public boolean isPassable()
		{
			// Add logical conjunction with other things that could affect
			// passability, like buildings
			return this.agentInPlace == null && this.terrain.isPassable()
					&& this.resource != Resources.Wood
					&& this.resource != Resources.Stone
					&& this.resource != Resources.Food;
		}

		/*
		 * If there is a building with a tile on this map tile, return that
		 * building. Else, return null
		 */
		public Building getBuilding()
		{
			return building;
		}

		public void setBuilding(Building building)
		{
			Map.this.buildings.add(building);
			this.building = building;
		}

		public void setResource(Resources resource)
		{
			this.resource = resource;
		}

		public Resources getResource()
		{
			return this.resource;
		}

		/*
		 * 
		 */
		// public Resources harvest() {
		// resource = null;
		// }

		public MapTile(int row, int col)
		{
			this.x = row;
			this.y = col;
			building = null;
		}

		public void makeConnections()
		{
			// connects each MapTile to its neighbors to form a graph-like
			// structure

			/*
			 * set east and west tiles
			 */
			if(x == 0)
			{
				west = null;
				east = Map.this.map[x + 1][y];
			}
			else if(x == MAP_WIDTH - 1)
			{
				west = Map.this.map[x - 1][y];
				east = null;
			}
			else
			{
				west = Map.this.map[x - 1][y];
				east = Map.this.map[x + 1][y];
			}

			/*
			 * Set north and south tiles
			 */
			if(y == 0)
			{
				north = null;
				south = Map.this.map[x][y + 1];
			}
			else if(y == MAP_HEIGHT - 1)
			{
				north = Map.this.map[x][y - 1];
				south = null;
			}
			else
			{
				north = Map.this.map[x][y - 1];
				south = Map.this.map[x][y + 1];
			}

			/*
			 * Switch case statements don't work with variables
			 */
			/*
			 * switch(x) { case 0: west = null; east = Map.this.map[x + 1][y];
			 * break; case MAP_SIZE - 1: west = Map.this.map[x - 1][y]; east =
			 * null; break; default: west = Map.this.map[x - 1][y]; east =
			 * Map.this.map[x + 1][y]; break; }
			 * 
			 * switch(y) { case 0: north = null; south = Map.this.map[x][y + 1];
			 * break; case MAP_SIZE - 1: north = Map.this.map[x][y - 1]; south =
			 * null; break; default: north = Map.this.map[x][y - 1]; south =
			 * Map.this.map[x][y + 1]; break; }
			 */

		}

		public void setCoastline(boolean isCoast)
		{
			this.isCoastline = isCoast;
		}

		public boolean getCoastline()
		{
			return this.isCoastline;
		}

		public void setTerrain(Terrains type)
		{
			this.terrain = type;
		}

		public Terrains getTerrain()
		{
			return this.terrain;
		}

		public MapTile north()
		{
			return this.north;
		}

		public MapTile east()
		{
			return this.east;
		}

		public MapTile south()
		{
			return this.south;
		}

		public MapTile west()
		{
			return this.west;
		}

		public void setItem(Item toPlace)
		{
			Map.this.itemList.add(toPlace);
			this.itemInPlace = toPlace;
		}

		public Item getItem(MapTile tile)
		{
			return tile.itemInPlace;
		}

		public void setAgent(Agent toPlace)
		{
			this.agentInPlace = toPlace;
		}

		public Agent getAgent()
		{
			return agentInPlace;
		}

		public int getX()
		{
			return this.x;
		}

		public int getY()
		{
			return this.y;
		}

	}

	public MapTile[][] getArray()
	{
		return this.map;
	}

	// Current implementation is for iteration one purposes but should be
	// updated for the final version of the game.. generates a rectangular
	// collection of tiles given a corner tile, should be upper left corner
	public Collection<MapTile> generateCollection(MapTile c, int width,
			int height)
	{
		int i = c.x;
		int j = c.y;
		Collection<MapTile> output = new LinkedList<MapTile>();

		if(i < 0 || j < 0 || i + width >= map.length
				|| j + height >= map[0].length)
		{
			return null;
		}

		for(int x = c.x; x < c.x + width; x++)
		{
			for(int y = c.y; y < c.y + height; y++)
			{
				output.add(map[x][y]);
			}

		}
		return output;
	}

	// Simply used for testing purposes for now. Can add support for showing
	// different terrain, agents, and items on tiles later
	public String toString()
	{
		String str = "";
		for(int i = 0; i < map.length; i++)
		{
			for(int j = 0; j < map[0].length; j++)
			{
				if(map[i][j].agentInPlace != null)
				{
					str += "A";
				}
				else if(map[i][j].itemInPlace != null)
				{
					str += map[i][j].itemInPlace
							.consoleToString(map[i][j].itemInPlace);
				}
				else if(map[i][j].building != null)
				{
					str += map[i][j].building
							.consoleToString(map[i][j].building);
				}
				else if(map[i][j].resource != null)
				{
					if(map[i][j].resource.equals(Resources.Airplane))
					{
						str += "p";
					}
					else
					{
						str += map[i][j].resource.getKey();
					}
				}
				else
				{
					str += map[i][j].terrain.consoleToString(map[i][j].terrain);
				}
			}
			str += "\n";

		}
		return str;
	}

	
	public String getMapTeilInfo(int x, int y)
	{
		MapTile temp = map[y][x];
		String str = "| Terrain type: "
				+ temp.terrain.consoleToString(temp.terrain) + "\n";

		if(temp.getAgent() != null)
		{
			str += "| Agent Here!\n";
		}
		if(temp.itemInPlace != null)
		{
			str += "| Item: "
					+ temp.itemInPlace.consoleToString(temp.itemInPlace)
					+ " Here!\n";
		}
		if(temp.building != null)
		{
			str += "| Some kind of building Here!\n";
		}
		if(temp.resource != null)
		{
			if(temp.resource.equals(Resources.Airplane))
			{
				str += "| Airplane Here!\n";
			}

			str += "| Resource: " + temp.resource.getKey() + " Here!\n";
		}

		return str;
	}

}