/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package model;

@SuppressWarnings("serial")
public class HarvestCommand extends Command
{
	Resources resource;

	public HarvestCommand(Resources resource)
	{
		this.resource = resource;
	}

	public Resources getResource()
	{
		return resource;
	}

}
