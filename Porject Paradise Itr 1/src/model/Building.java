/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package model;

import java.awt.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.TreeMap;

public abstract class Building implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1230297227852161847L;
	/*
	 * Each building knows which tiles it occupies; The exact best structure for
	 * holding on to that, I (Collin) don't know right now, but the Collection
	 * interface is probably a fine generalization
	 */
	private Collection<Map.MapTile> topography;
	protected String buildingName;
	private TreeMap<Resources, Integer> cost;
	protected int buildTime; // the number of ticks it takes to finish a
								// building
	protected int blockHeight;
	protected int blockWidth;
	protected Point buildingLocation;

	public Collection<Map.MapTile> getTiles()
	{
		return topography;
	}

	public abstract TreeMap<Resources, Integer> getCost();

	public void setTiles()
	{
		for(Map.MapTile tile : topography)
		{
			tile.setBuilding(this);
		}
	}

	public String getName()
	{
		return buildingName;
	}

	public Point getBuildingLocation()
	{
		return buildingLocation;
	}

	public void setBuildingLocation(int x, int y)
	{
		buildingLocation = new Point(x, y);
	}

	public int getBuildTime()
	{
		return buildTime;
	}

	public int getBlockHeight()
	{
		return blockHeight;
	}

	public int getBlockWidth()
	{
		return blockWidth;
	}

	public String consoleToString(Building b)
	{
		if(b.getName().equals("Living Tent"))
		{
			return "^";
		}

		else if(b.getName().equals("Kitchen"))
		{
			return "&";
		}
		else if(b.getName().equals("Workshop"))
		{
			return "W";
		}

		return "Something went wrong...";
	}

	// public static Building getInstance(String name)
	// {
	// if(name.equalsIgnoreCase("Living Tent"))
	// {
	// return new Tent();
	// }
	// else if(name.equalsIgnoreCase("Kitchen"))
	// {
	// return new Kitchen();
	// }
	// else if (name.equalsIgnoreCase("Workshop"))
	// return new Workshop();
	//
	// return null;
	// }

	public void setTopography(Collection<Map.MapTile> zone)
	{
		this.topography = zone;
	}

	public static class Tent extends Building
	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1728534200714861568L;
		private static final TreeMap<Resources, Integer> buildingCost = init();

		private static TreeMap<Resources, Integer> init()
		{
			TreeMap<Resources, Integer> t = new TreeMap<Resources, Integer>();
			t.put(Resources.Wood, 4);
			return t;
		}

		public Tent()
		{
			super();
			this.buildingName = "Living Tent";
			this.buildTime = 10;

			this.blockHeight = 3;
			this.blockWidth = 2;
		}

		@Override
		public TreeMap<Resources, Integer> getCost()
		{
			return buildingCost;
		}

	}

	public static class Kitchen extends Building
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = -7617299026546390310L;
		private static final TreeMap<Resources, Integer> buildingCost = init();

		private static TreeMap<Resources, Integer> init()
		{
			TreeMap<Resources, Integer> t = new TreeMap<Resources, Integer>();
			t.put(Resources.Wood, 4);
			t.put(Resources.Airplane, 1);
			return t;
		}

		public Kitchen()
		{
			this.buildingName = "Kitchen";
			this.buildTime = 20;

			this.blockHeight = 3;
			this.blockWidth = 3;
		}

		@Override
		public TreeMap<Resources, Integer> getCost()
		{
			return buildingCost;
		}

	}

	public static class Workshop extends Building
	{
		/**
		 * 
		 */
		private static final long serialVersionUID = 8126528942556367909L;
		private static final TreeMap<Resources, Integer> buildingCost = init();

		private static TreeMap<Resources, Integer> init()
		{
			TreeMap<Resources, Integer> t = new TreeMap<Resources, Integer>();
			t.put(Resources.Wood, 4);
			t.put(Resources.Airplane, 2);
			return t;
		}

		public Workshop()
		{
			this.buildingName = "Workshop";
			this.buildTime = 30;
			this.blockHeight = 3;
			this.blockWidth = 4;
		}

		@Override
		public TreeMap<Resources, Integer> getCost()
		{
			return buildingCost;
		}

	}

	public static class Dock extends Building
	{

		/**
		 * 
		 */
		private static final long serialVersionUID = 3186695750516899035L;
		private static final TreeMap<Resources, Integer> buildingCost = init();

		private static TreeMap<Resources, Integer> init()
		{
			TreeMap<Resources, Integer> t = new TreeMap<Resources, Integer>();
			t.put(Resources.Wood, 4);
			t.put(Resources.Airplane, 2);
			return t;
		}

		public Dock()
		{
			this.buildingName = "Dock";
			this.buildTime = 100;
			this.blockHeight = 4;
			this.blockWidth = 5;
		}

		@Override
		public TreeMap<Resources, Integer> getCost()
		{
			return buildingCost;
		}

	}

	/*
	 * Need a build compeleted to detect if the building is finished building.
	 * !!! Also, it's really good if we can have a building time remained.
	 */
	/**
	 * buildCompeleted: Check if the building is finished building.
	 * 
	 * @return true if it's finished, false if it's still constructing.
	 */
	public boolean buildCompeleted(Map aMap)
	{
		return aMap.getTile(buildingLocation.x, buildingLocation.y)
				.getBuilding() != null;
	}

}
