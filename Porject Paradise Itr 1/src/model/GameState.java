/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package model;

import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumMap;
import java.util.LinkedList;
import java.util.List;

import main.MainViewController;
import model.Map.MapTile;

public class GameState implements Serializable
{
	private static final long serialVersionUID = -4681241972398461186L;
	private final AgentCollection agents;
	private final EnumMap<Resources, Integer> resourcepool;
	private final Map map;
	private final int totalAgents;
	private final LinkedList<String> notificationFeed;
	private final ArrayList<Point> containerList;
	private final ArrayList<Building> incompleteBuildings;
	private final boolean[][] buildingsOccupied;
	

	private final static int RESOURCE_CAPACITY_FOR_CONTAINER = 45;

	private boolean wonTheGame, lostTheGame;

	private RandomEvent eventEngine;
	private MainViewController aFrame;

	public GameState(int mapWidth, int mapHeight, MainViewController aFrame)
	{
		this.aFrame = aFrame;
		this.agents = new AgentCollection(this);

		this.totalAgents = agents.totalAgents();

		this.resourcepool = new EnumMap<Resources, Integer>(Resources.class);
		for(Resources resource : Resources.values())
		{
			resourcepool.put(resource, 0);
		}
		this.map = new Map(mapWidth, mapHeight);
		this.notificationFeed = new LinkedList<String>();
		eventEngine = new RandomEvent(this);
		containerList = new ArrayList<Point>();
		incompleteBuildings = new ArrayList<Building>();
		buildingsOccupied = new boolean[map.getWidth()][map.getHeight()];
		for(int i = 0; i < buildingsOccupied.length; i++)
		{
			for(int j = 0; j < buildingsOccupied[i].length; j++)
			{
				buildingsOccupied[i][j] = false;
			}
		}
		// Do not put instance-specific data in a constructor, ever
	}
	
	public GameState(int mapWidth, int mapHeight)
	{
		this.aFrame = null;
		this.agents = new AgentCollection(this);

		this.totalAgents = agents.totalAgents();

		this.resourcepool = new EnumMap<Resources, Integer>(Resources.class);
		for(Resources resource : Resources.values())
		{
			resourcepool.put(resource, 0);
		}
		this.map = new Map(mapWidth, mapHeight);
		this.notificationFeed = new LinkedList<String>();
		eventEngine = new RandomEvent(this);
		this.containerList = new ArrayList<Point>();
		this.incompleteBuildings = new ArrayList<Building>();
		buildingsOccupied = new boolean[map.getWidth()][map.getHeight()];
		for(int i = 0; i < buildingsOccupied.length; i++)
		{
			for(int j = 0; j < buildingsOccupied[i].length; j++)
			{
				buildingsOccupied[i][j] = false;
			}
		}
		// Do not put instance-specific data in a constructor, ever
	}
	
	/*
	 * Keeps track of which tiles have a building, whether complete
	 * or incomplete, so that the UI knows where players can build.
	 */
	public void updateBuildingOccupied(Building building) {
		List<Point> tileCoords = new ArrayList<Point>();
		Collection<MapTile> tiles = building.getTiles();
		for(MapTile tile : tiles){
			tileCoords.add(new Point(tile.getX(), tile.getY()));
		}
		for(Point coord : tileCoords){
			int x = coord.x;
			int y = coord.y;
			buildingsOccupied[x][y] = true;
		}
	}
	
	public boolean[][] getBuildingsOccupied()
	{
		return this.buildingsOccupied;
	}

	public void addBuilding(Building aBuilding)
	{
		this.incompleteBuildings.add(aBuilding);
	}
	
	public ArrayList<Building> getBuildingsUnderConstruction()
	{
		return this.incompleteBuildings;
	}
	
	public List<Point> getContainerList()
	{
		return this.containerList;
	}
	
	public void pushNotification(String update)
	{
		notificationFeed.add(update);
	}

	public String getMostRecentNotification()
	{
		return notificationFeed.peekLast();
	}

	public List<String> getAllNotifications()
	{
		return notificationFeed;
	}

	public AgentCollection getAgents()
	{
		return agents;
	}

	public EnumMap<Resources, Integer> getResourcepool()
	{
		return resourcepool;
	}

	public Map getMap()
	{
		return map;
	}
	
	public List<Item> getItemList() {
		return this.map.getItemList();
	}

	public void update()
	{
		if(!lostTheGame && !wonTheGame)
		{
			this.agents.update();
			eventEngine.tick();
		}
		if(map.hasDock())
			this.win();
	}

	public int getResourceCap()
	{
		return map.getContainerCount()
				* GameState.RESOURCE_CAPACITY_FOR_CONTAINER;
	}

	// Call this method to try and put things in the resource pool, up to the
	// cap
	// The amount actually put in is returned
	public int putResource(Resources resource, int amount)
	{
		int existing = resourcepool.get(resource);
		if(amount > existing)
			return 0;
		int capacity = this.getResourceCap() - existing;
		if(capacity < amount)
		{
			resourcepool.put(resource, existing + capacity);
			return capacity;
		}
		else
		{
			resourcepool.put(resource, existing + amount);
			return amount;
		}
	}

	public void win()
	{
		this.wonTheGame = true;

		this.pushNotification("You've won the game!");

	}
	
	public boolean hasWon()
	{
		return wonTheGame;
	}
	
	public boolean hasLose()
	{
		return lostTheGame;
	}

	public void lose()
	{
		this.lostTheGame = true;

		this.pushNotification("You've lost the game!");

	}
	
	public void pushMessage(String str)
	{
		this.pushNotification(str);
	}
}
