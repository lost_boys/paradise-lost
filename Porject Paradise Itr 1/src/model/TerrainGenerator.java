/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */


package model;


interface TerrainGenerator
{
	public void generateTerrain(Map map);
}
