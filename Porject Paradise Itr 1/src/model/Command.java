/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */
package model;

import java.io.Serializable;

/* The Command family of classes exist to communicate player objectives to Agents
 * How it might should work: Player declares some job to do, like "Build wall"
 * Each Agent in the AgentCollection has a Task put in its queue to carry out this Command
 * (The Task has a Command field to provide a reference to it)
 * If a task cannot be worked on cooperatively, it should be able to be "locked" by the first agent to start on it
 * Otherwise, once an Agent is done achieving the goal of the Command, it is marked completed
 * Then, after the Command Tasks have exited the queues of all agents, the Command should expire and be garbage collected
 */
@SuppressWarnings("serial")
public abstract class Command implements Serializable
{
	/*
	 * All commands should track whether they have been completed; Once a
	 * command has been completed, Agents should ignore Tasks that wrap them, as
	 * the job is done
	 */
	protected boolean completed;
	protected boolean isAvailable;

	public Command()
	{
		this.isAvailable = true;
		this.completed = false;
	}

	public void finished()
	{
		this.completed = true;
	}

	public void setAvailability(boolean set)
	{
		this.isAvailable = set;
	}
}
