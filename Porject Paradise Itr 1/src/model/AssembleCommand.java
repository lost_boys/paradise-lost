/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package model;

@SuppressWarnings("serial")
public class AssembleCommand extends Command
{
	private PersonalItem item;

	public AssembleCommand(PersonalItem item)
	{
		this.item = item;
	}

	public PersonalItem getItem()
	{
		// TODO Auto-generated method stub
		return item;
	}
}
