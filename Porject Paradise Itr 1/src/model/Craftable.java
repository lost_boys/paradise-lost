/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package model;
import java.util.Map;

public interface Craftable 
{
	public Map<Resources, Integer> getCost();
}
