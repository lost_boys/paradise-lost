/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package model;

import java.util.EnumMap;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA. User: Sank Date: 4/22/14 Time: 10:04 PM To change
 * this template use File | Settings | File Templates.
 */

public class BuildingSimulation
{
	public static GameState state;
	private static AgentCollection agents;
	private static EnumMap<Resources, Integer> resourcepool;
	public static Map map;

	public static void main(String args[])
	{
		state = new GameState(65, 65);
		agents = state.getAgents();
		resourcepool = state.getResourcepool();
		map = state.getMap();
		TerrainGenerator gen = new SimpleMap();
		gen.generateTerrain(map);

		runSimulation();
	}

	@SuppressWarnings(
	{ "deprecation", "resource" })
	private static void runSimulation()
	{
		Agent a1 = new Agent(state.getMap(), state.getMap().getPlaneLocation().y, state.getMap().getPlaneLocation().x);
		Agent a2 = new Agent(state.getMap(), state.getMap().getPlaneLocation().y, state.getMap().getPlaneLocation().x);
		Agent a3 = new Agent(state.getMap(), state.getMap().getPlaneLocation().y, state.getMap().getPlaneLocation().x);

		agents.addAgent(a1);
		agents.addAgent(a2);
		agents.addAgent(a3);

		resourcepool.put(Resources.Wood, 2000);
		resourcepool.put(Resources.Airplane, 1000);
		resourcepool.put(Resources.Stone, 1000);
		resourcepool.put(Resources.Water, 0);
		resourcepool.put(Resources.Iron, 500);

		a1.setFatigue((short) 10);
		a2.setFatigue((short) 10);
		a3.setFatigue((short) 100);

		// agents.command(new Build("Living Tent", 30, 20));

		a1.setLocation(10, 10, map);
		a2.setLocation(10, 10, map);
		a3.setLocation(5, 5, map);

		System.out.println(map);
		Scanner pacer = new Scanner(System.in);
		while(true)
		{
			pacer.nextLine();
			for(int i = 0; i < 10; i++)
			{
				System.out.println("\n\n\n");
			}
			state.update();

			System.out.println(map);
			System.out.println(a1);
			System.out.println(a2);
			System.out.println(a3);
		}

	}

}
