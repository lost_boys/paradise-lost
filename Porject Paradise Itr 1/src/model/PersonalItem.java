/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package model;

import java.io.Serializable;
import java.util.EnumMap;
import java.util.Map;
import java.util.TreeMap;
/* An analogous implementation of Hercy's Item class that might be more clean and efficient
 * 
 */
@SuppressWarnings({"serial", "unchecked", "rawtypes", "deprecation" })
public abstract class PersonalItem implements Craftable, Serializable
{
	protected String name;
	
	public abstract java.util.Map<Resources, Integer> getCost();
	
	public String toString()
	{
		return this.name;
	}
	
	
	public static PersonalItem getInstance(String name)
	{
		if(name.equalsIgnoreCase("hammer"))
		{
			return new Hammer();
		}
		else if(name.equalsIgnoreCase("knife"))
		{
			return new Knife();
		}
		else if(name.equalsIgnoreCase("axe"))
		{
			return new Axe();
		}
		else if(name.equalsIgnoreCase("rope"))
		{
			return new Rope();
		}
		else if(name.equalsIgnoreCase("bucket"))
		{
			return new Bucket();
		}
		

		return null;
	}

	protected static class Hammer extends PersonalItem
	{
		private static final TreeMap<Resources, Integer> constructionCost = init();

		private static TreeMap<Resources, Integer> init()
		{
			TreeMap<Resources, Integer> t = new TreeMap();
			t.put(Resources.Stone, 4);
			return t;
		}

		public Hammer()
		{
			this.name = "Hammer";
		}

		@Override
		public Map<Resources, Integer> getCost() 
		{

			return constructionCost;
		}

	}
	
	protected static class Bucket extends PersonalItem
	{
		private static final EnumMap<Resources, Integer> constructionCost = init();
		
		private static EnumMap<Resources, Integer> init()
		{
			EnumMap<Resources, Integer> m = new EnumMap(Resources.class);
			m.put(Resources.Wood, 6);
			return m;
		}
		
		public Bucket()
		{
			this.name = "Bucket";
		}

		@Override
		public Map<Resources, Integer> getCost() {
			// TODO Auto-generated method stub
			return constructionCost;
		}
		
		
		
		
		
	}

	protected static class Knife extends PersonalItem
	{
		private static final TreeMap<Resources, Integer> constructionCost = init();

		private static TreeMap<Resources, Integer> init()
		{
			TreeMap<Resources, Integer> t = new TreeMap();
			t.put(Resources.Airplane, 2);
			return t;
		}

		public Knife()
		{
			this.name = "Knife";
		}
		
		@Override
		public Map<Resources, Integer> getCost() 
		{

			return constructionCost;
		}

	}

	protected static class Axe extends PersonalItem
	{
		private static final TreeMap<Resources, Integer> constructionCost = init();

		private static TreeMap<Resources, Integer> init()
		{
			TreeMap<Resources, Integer> t = new TreeMap();
			t.put(Resources.Stone, 1);
			t.put(Resources.Iron, 2);
			return t;
		}

		public Axe()
		{
			this.name = "Axe";
		}
		
		@Override
		public Map<Resources, Integer> getCost() 
		{

			return constructionCost;
		}

	}

	protected static class Rope extends PersonalItem
	{
		private static final TreeMap<Resources, Integer> constructionCost = init();

		private static TreeMap<Resources, Integer> init()
		{
			TreeMap<Resources, Integer> t = new TreeMap();
			t.put(Resources.Airplane, 3);
			return t;
		}

		public Rope()
		{
			this.name = "Rope";
		}
		
		@Override
		public Map<Resources, Integer> getCost() 
		{

			return constructionCost;
		}

	}

}
