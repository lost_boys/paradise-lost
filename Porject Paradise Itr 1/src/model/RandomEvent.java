/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package model;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Random;

/* This class provides a means for unpredictable game events to occur, including both oppositional events and those that benefit the player
 * RandomEvent wraps the GameState
 * Every time the game state is updated, RandomEvent should have its tick method called, then the game state can be rendered
 */
public class RandomEvent implements Serializable
{

	private static final long serialVersionUID = -5235432725912784407L;
	private GameState state;
	private int ticker;
	// This value describes how many ticks *must* occur before another random
	// event *may* happen
	private final static int EVENTLESS_INTERVAL = 300;
	private Random random;

	public RandomEvent(GameState state)
	{
		this.state = state;
		this.ticker = EVENTLESS_INTERVAL;
		this.random = new Random();
	}

	public void tick()
	{
		if(this.ticker <= 0)
		{
			// Give a say, 5% chance of an event happening at all
			if(random.nextInt(100) < 5)
			{
				// Use another random number to pick which event happens
				int choice = random.nextInt(8);
				switch(choice)
				{
					case 0:
						int existingFood = state.getResourcepool().get(
								Resources.Food);
						if(existingFood > 0)
						{
							state.pushNotification("Disaster strikes! Food spoilage halves food stores.");
							state.getResourcepool().put(Resources.Food,
									existingFood / 2);
						}
						break;
					case 1:
						Agent stranger = new Agent(state.getMap(), state.getMap().getPlaneLocation().y, state.getMap().getPlaneLocation().x);
						Map.MapTile location = null;
						while(location == null)
						{
							int x = random.nextInt(state.getMap().getWidth());
							int y = random.nextInt(state.getMap().getHeight());
							if(state.getMap().getTile(x, y).getAgent() == null
									&& state.getMap().getTile(x, y)
											.isPassable())
							{
								location = state.getMap().getTile(x, y);
							}
						}
						state.pushNotification("How mysterious! A strange new lost soul has joined you.");
						location.setAgent(stranger);
						state.getAgents().addAgent(stranger);
						break;
					case 2:
						int existingWood = state.getResourcepool().get(
								Resources.Wood);
						if(existingWood > 0)
						{
							state.pushNotification("Disaster strikes! Termite infestation halves wood stores.");
							state.getResourcepool().put(Resources.Wood,
									existingWood / 2);
						}
						break;
					case 3:
						Iterator<Agent> agentsToHurt = state.getAgents()
								.iterator();
						while(agentsToHurt.hasNext())
						{
							Agent current = agentsToHurt.next();
							if(current.getFatigue() > 2500)
							{
								current.setFatigue((short) 2500);
							}

						}
						state.pushNotification("Disaster strikes! Mysterious fatigue overtakes everyone.");
						break;
					case 4:
						state.pushNotification("How mysterious! A sense of vague unease overtakes everyone.");
						break;
					case 5:
					case 6:
					case 7:
						if(state.getResourcepool().get(Resources.Food) > (.6 * state
								.getResourceCap()))
						{
							Agent guy = new Agent(state.getMap(), state.getMap().getPlaneLocation().y, state.getMap().getPlaneLocation().x);
							Map.MapTile loc = null;
							while(loc == null)
							{
								int x = random.nextInt(state.getMap()
										.getWidth());
								int y = random.nextInt(state.getMap()
										.getHeight());
								if(state.getMap().getTile(x, y).getAgent() == null
										&& state.getMap().getTile(x, y)
												.isPassable())
								{
									location = state.getMap().getTile(x, y);
								}
							}
							loc.setAgent(guy);
							state.getAgents().addAgent(guy);
							guy = new Agent(state.getMap(), state.getMap().getPlaneLocation().y, state.getMap().getPlaneLocation().x);
							loc = null;
							while(loc == null)
							{
								int x = random.nextInt(state.getMap()
										.getWidth());
								int y = random.nextInt(state.getMap()
										.getHeight());
								if(state.getMap().getTile(x, y).getAgent() == null
										&& state.getMap().getTile(x, y)
												.isPassable())
								{
									location = state.getMap().getTile(x, y);
								}
							}
							loc.setAgent(guy);
							state.getAgents().addAgent(guy);

							state.pushNotification("How fortuitous! Your abundant supplies have attracted two other survivors.");
						}
						break;

				}
				this.ticker = EVENTLESS_INTERVAL;
			}

		}
		else
		{
			ticker--;
		}

	}

}
