/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package model;

import java.util.Random;

/**
 * Created with IntelliJ IDEA. User: Sank Date: 4/20/14 Time: 2:23 PM To change
 * this template use File | Settings | File Templates.
 */
public class SimpleMap implements TerrainGenerator
{
	private boolean randomBoolean = new Random().nextBoolean();

	@Override
	public void generateTerrain(Map map)
	{
		for(int i = 0; i < map.getWidth(); i++)
		{
			for(int j = 0; j < map.getHeight(); j++)
			{
				randomBoolean = new Random().nextBoolean();

				if(randomBoolean)
				{
					map.getTile(i, j).setTerrain(Terrains.Jungle);
				}
				else
				{
					map.getTile(i, j).setTerrain(Terrains.Rocky);
				}
			}
		}

		for(int i = map.getWidth() / 2 - 10; i <= map.getWidth() / 2 + 10; i++)
		{
			for(int j = map.getWidth() / 2 - 10; j <= map.getWidth() / 2 + 10; j++)
			{
				map.getTile(i, j).setTerrain(Terrains.Water);

			}
		}

		map.getTile(map.getWidth() / 2 - 10 - 1, map.getWidth() / 2 - 10 + 5)
				.setTerrain(Terrains.Water);
		map.getTile(map.getWidth() / 2 - 10 - 1, map.getWidth() / 2 - 10 + 6)
				.setTerrain(Terrains.Water);
		map.getTile(map.getWidth() / 2 - 10 - 1, map.getWidth() / 2 - 10 + 7)
				.setTerrain(Terrains.Water);

		map.getTile(map.getWidth() / 2 - 10 + 10, map.getWidth() / 2 - 10 - 1)
				.setTerrain(Terrains.Water);
		map.getTile(map.getWidth() / 2 - 10 + 11, map.getWidth() / 2 - 10 - 1)
				.setTerrain(Terrains.Water);
		map.getTile(map.getWidth() / 2 - 10 + 12, map.getWidth() / 2 - 10 - 1)
				.setTerrain(Terrains.Water);

		map.getTile(map.getWidth() / 2 - 10 + 21, map.getWidth() / 2 - 10 + 1)
				.setTerrain(Terrains.Water);
		map.getTile(map.getWidth() / 2 - 10 + 21, map.getWidth() / 2 - 10 + 2)
				.setTerrain(Terrains.Water);
		map.getTile(map.getWidth() / 2 - 10 + 21, map.getWidth() / 2 - 10 + 3)
				.setTerrain(Terrains.Water);

		map.getTile(map.getWidth() / 2 - 10 + 4, map.getWidth() / 2 - 10 + 21)
				.setTerrain(Terrains.Water);
		map.getTile(map.getWidth() / 2 - 10 + 5, map.getWidth() / 2 - 10 + 21)
				.setTerrain(Terrains.Water);
		map.getTile(map.getWidth() / 2 - 10 + 6, map.getWidth() / 2 - 10 + 21)
				.setTerrain(Terrains.Water);

		map.getTile(map.getWidth() / 2 - 10 + 3, map.getWidth() / 2 - 10 + 3)
				.setTerrain(Terrains.Jungle);

		map.getTile(map.getWidth() / 2 - 10 + 10, map.getWidth() / 2 - 10 + 14)
				.setTerrain(Terrains.Jungle);
		map.getTile(map.getWidth() / 2 - 10 + 10 + 1,
				map.getWidth() / 2 - 10 + 14 + 1).setTerrain(Terrains.Jungle);

		map.getTile(map.getWidth() / 2 - 10 + 3 + 1,
				map.getWidth() / 2 - 10 + 14).setTerrain(Terrains.Jungle);
		map.getTile(map.getWidth() / 2 - 10 + 3,
				map.getWidth() / 2 - 10 + 14 + 1).setTerrain(Terrains.Jungle);

		map.getTile(map.getWidth() / 2 - 10 + 10, map.getWidth() / 2 - 10 + 10)
				.setTerrain(Terrains.Jungle);
		map.getTile(map.getWidth() / 2 - 10 + 10,
				map.getWidth() / 2 - 10 + 10 + 1).setTerrain(Terrains.Jungle);

		map.getTile(map.getWidth() / 2 - 10 + 7 + 1,
				map.getWidth() / 2 - 10 + 14).setTerrain(Terrains.Jungle);
		map.getTile(map.getWidth() / 2 - 10 + 7 + 2,
				map.getWidth() / 2 - 10 + 14 + 1).setTerrain(Terrains.Jungle);
		map.getTile(map.getWidth() / 2 - 10 + 7 + 2,
				map.getWidth() / 2 - 10 + 14).setTerrain(Terrains.Jungle);
		map.getTile(map.getWidth() / 2 - 10 + 7 + 3,
				map.getWidth() / 2 - 10 + 14 + 1).setTerrain(Terrains.Jungle);
	}
}
