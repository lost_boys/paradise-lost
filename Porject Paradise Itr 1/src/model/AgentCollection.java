/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AgentCollection implements Iterable<Agent>, Serializable
{
	private static final long serialVersionUID = 2217972650599364416L;
	private List<Agent> agents;
	private GameState enclosingState;
	//To prevent concurrent modification: Agents who are removed are put on a blacklist so that the collection can remove them at its own schedule
	private List<Agent> blacklist;

	public AgentCollection(GameState gs)
	{
		this.agents = new ArrayList<Agent>();
		this.enclosingState = gs;
		this.blacklist = new ArrayList<Agent>();
	}

	public void addAgent(Agent a)
	{
		this.agents.add(a);
		a.setMembership(this);
		a.setGlobalResources(enclosingState.getResourcepool());
	}

	public void removeAgent(Agent a)
	{
		this.blacklist.add(a);
	}
	
	public ArrayList<Agent> getAliveAgents()
	{
		ArrayList<Agent> aliveAgents = new ArrayList<Agent>();
		
		for(Agent a : agents)
		{
			if(!blacklist.contains(a))
			{
				aliveAgents.add(a);
			}
		}
		return aliveAgents;
	}
	
	public void killAgent(Agent a)
	{
		if(this.agents.contains(a))
		{
			this.removeAgent(a);
			this.enclosingState.pushNotification(a.getName() + " has died.");
		}
	}

	public void command(Command c)
	{
		for(Agent a : agents)
		{
			a.order(c);
			System.out.println(c.toString());
		}
	}
	
	public int totalAgents()
	{
		
		return agents.size() - blacklist.size();
	}
	public void update()
	{
		if(!agents.isEmpty())
		{
			for(Agent a : agents)
			{
				a.update();
			}
		}
		if(!blacklist.isEmpty())
		{
			for(Agent a : blacklist)
			{
				agents.remove(a);
			}
			blacklist.clear();
		}
		if(agents.isEmpty())
		{
			this.enclosingState.pushNotification("All of your survivors have failed to survive, and you have no living agents left.");
			this.enclosingState.lose();
		}
	}


    public String populationStatus(){
        String result = "";
        if(agents.size() > 0)
        {
        	for(Agent a: this){
            result += a.toString();
            result += "\n\n";
        }
        }
        return result;
    }

	@Override
	public Iterator<Agent> iterator()
	{
		// TODO Auto-generated method stub
		return agents.iterator();
	}
	
	public void sendUpNotification(String message)
	{
		this.enclosingState.pushNotification(message);
	}
	
	//I'm sick of creating intermediary methods
	public GameState getGameState()
	{
		return this.enclosingState;
	}
}
