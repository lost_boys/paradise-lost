/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */


package model;

import java.io.Serializable;

public enum Resources implements Serializable
{
	Water('h'), Food('f'), Wood('w'), Stone('s'), Iron('i'), @Deprecated Airplane('a');

	private char key;

	Resources(char key)
	{
		this.key = key;
	}

	public char getKey()
	{
		return key;
	}
}
