/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package model;

import java.util.TreeMap;

//Wraps an item that can be placed in the world, so it can be held in the inventory and placed
@SuppressWarnings("serial")
public class PlaceableInventoryItem extends PersonalItem
{
	private Item item;

	public PlaceableInventoryItem(Item worldItem)
	{
		this.item = worldItem;
		this.name = this.item.toString();
	}

	public Item getItem()
	{
		return item;
	}

	@Override
	public TreeMap<Resources, Integer> getCost()
	{
		// TODO Auto-generated method stub
		return null;
	}

}
