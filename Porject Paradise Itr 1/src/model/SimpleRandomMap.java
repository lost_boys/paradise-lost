/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package model;

import java.util.Random;

public class SimpleRandomMap implements TerrainGenerator
{

	@Override
	public void generateTerrain(Map map)
	{
		for(int i = 0; i < map.getWidth(); i++)
		{
			for(int j = 0; j < map.getHeight(); j++)
			{
				int randomNum = new Random().nextInt(1000);
				int beachSize = new Random().nextInt(4) + 10;
				int bechPicker = 10;
				int junglePicker = 970;
				if(i <= beachSize || j <= beachSize
						|| i >= map.getWidth() - beachSize - 1
						|| j >= map.getHeight() - beachSize - 1)
				{
					bechPicker = 700;
				}

				if(i >= map.getWidth() / 2 - 5
						|| j >= map.getHeight() / 2 - 5
						|| i <= map.getWidth() / 2 + 5 - 1
						|| j <= map.getHeight() / 2 + 5 - 1)
				{
					junglePicker = 860;
				}

				if(randomNum < bechPicker)
				{
					map.getTile(i, j).setTerrain(Terrains.Beach);
				}
				else if(randomNum < junglePicker)
				{
					map.getTile(i, j).setTerrain(Terrains.Jungle);
				}
				else if(randomNum < 995)
				{
					map.getTile(i, j).setTerrain(Terrains.Rocky);
				}
				else
				{
					map.getTile(i, j).setTerrain(Terrains.Water);
				}

			}
		}
		Terrains[][] originalMap = new Terrains[map.getWidth()][map.getHeight()];
		for(int i = 0; i < map.getWidth(); i++)
		{
			for(int j = 0; j < map.getHeight(); j++)
			{
				originalMap[i][j] = map.getTile(i, j).getTerrain();
			}
		}

		for(int i = 0; i < map.getWidth(); i++)
		{
			for(int j = 0; j < map.getHeight(); j++)
			{
				int seaBorad = new Random().nextInt(1000);
				if(seaBorad < 200)
				{
					seaBorad = 0;
				}
				else if(seaBorad < 500)
				{
					seaBorad = 1;
				}
				else if(seaBorad < 800)
				{
					seaBorad = 2;
				}
				else if(seaBorad < 950)
				{
					seaBorad = 3;
				}
				else if(seaBorad < 980)
				{
					seaBorad = 4;
				}
				else
				{
					seaBorad = 5;
				}

				if(i <= 1 + seaBorad || j <= 1 + seaBorad
						|| i >= map.getWidth() - 2 - seaBorad
						|| j >= map.getHeight() - 2 - seaBorad)
				{
					map.getTile(i, j).setTerrain(Terrains.Water);
				}
			}
		}

		for(int i = 2; i <= map.getWidth() - 2; i++)
		{
			for(int j = 2; j <= map.getHeight() - 2; j++)
			{
				if(originalMap[i][j].equals(Terrains.Water))
				{
					map.getTile(i, j - 1).setTerrain(Terrains.Water);
					map.getTile(i, j + 1).setTerrain(Terrains.Water);
					map.getTile(i + 1, j - 1).setTerrain(Terrains.Water);
					map.getTile(i + 1, j).setTerrain(Terrains.Water);
					map.getTile(i + 1, j + 1).setTerrain(Terrains.Water);
				}
			}
		}
		/*
		for(int i = 1; i <= map.getWidth() - 2; i++)
		{
			for(int j = 1; j <= map.getHeight() - 2; j++)
			{
				if((!map.getTile(i, j).equals(Terrains.Water)))
				{
					int waterCounter = 0;
					for(int x = -1; x < 2; x++)
					{
						for(int y = -1; y < 2; y++)
						{
							if(map.getTile(i + x, j + y).equals(Terrains.Water))
							{
								waterCounter++;
							}
						}
					}
					if(waterCounter > 4)
					{
						map.getTile(i, j).setTerrain(Terrains.Water);
					}
				}
			}

		}
		*/
		
	}

}
