/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package main;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.*;
import java.io.*;
import java.util.EnumMap;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import songplayer.MusicLibrary;
import view.Console;
import view.GamePanel;
import view.HUDPanel;
import view.MenuPanel;
import model.*;

@SuppressWarnings("serial")
public class MainViewController extends JFrame
{
	private Console console;
	private final int GAME_TIMER = 12000;
	private int FRAME_HEIGHT = 768;
	private int FRAME_WIDTH = 1024;

	public static final String VERSION_INFO = "2.7";
	public static final String BUILD_INFO = "14M14e";

	private MenuPanel menuView;

	private int height, width;

	private GamePanel gamePanel;
	private HUDPanel HUD;

	private GameState theGame;
	private Map aMap;
	private AgentCollection agents;
	private boolean theGameIsRunning;

	private boolean playMusic;

	private static MusicLibrary playlist;

	private Timer theGameTimer; 
	private Agent a1;
	private EnumMap<Resources, Integer> resourcepool;
	
	private int winOrLoseCounter;

	public static void main(String args[])
	{

		new MainViewController();
		// TODO: Make it play the music!
		// playerlist = new MusicLibrary();
		// playerlist.playSongs();
	}

	public MainViewController()
	{
		this.winOrLoseCounter = 0;
		this.playMusic = true;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLayout(null);
		this.setResizable(false);
		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		this.addKeyListener(new KeyboardListener());
		theGameIsRunning = false;
		this.setTitle("Paradise Lost - v" + VERSION_INFO + " (" + BUILD_INFO
				+ ")");

		// this.setVisible(true);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(screenSize.width / 2 - this.getSize().width / 2,
				screenSize.height / 2 - this.getSize().height / 2);

		// Adding console
		console = new Console(theGame, this);
		console.setVisible(false);

		this.setVisible(true);

		// Getting the actual size of the content area. There is a possibility
		// that the heigh may be changed under different OS user interface.
		height = (int) this.getContentPane().getSize().getHeight();
		width = (int) this.getContentPane().getSize().getWidth();

		// Adding menu
		menuView = new MenuPanel(width, height, VERSION_INFO, this);
		menuView.setBounds(0, 0, width, height);
		this.add(menuView);

		// Test print out
		System.out.println("Paradise Lost - v" + VERSION_INFO + " ("
				+ BUILD_INFO + ")");
		System.out.println("Window Size: " + FRAME_WIDTH + "x" + FRAME_HEIGHT
				+ "\nContent Size: " + width + "x" + height);

	}

	public int getContentHeight()
	{
		return height;
	}

	public int getContentWidth()
	{
		return width;
	}

	public Console getConsole()
	{
		return console;
	}

	/**
	 * loadGame: Called to load a saved game state.
	 */
	public void loadGame()
	{
		File myFile = new File("game.sav");

		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter filter = new FileNameExtensionFilter(
				"Saved Games", "sav", "SAV");
		chooser.setFileFilter(filter);
		int returnVal = chooser.showOpenDialog(this);
		if(returnVal == JFileChooser.APPROVE_OPTION)
		{
			System.out.println("You chose to open this file: "
					+ chooser.getSelectedFile().getName());
			myFile = chooser.getSelectedFile();

			FileInputStream filein;

			try
			{
				filein = new FileInputStream(myFile);
				ObjectInputStream objectin = new ObjectInputStream(filein);
				theGame = (GameState) objectin.readObject();
				objectin.close();
				filein.close();
			}
			catch(FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch(IOException f)
			{
				f.printStackTrace();
			}
			catch(ClassNotFoundException g)
			{
				g.printStackTrace();
			}

			this.remove(menuView);

			// Setup the gamePanel
			gamePanel = new GamePanel(theGame, this);
			gamePanel.setBounds(0, 0, width, height);
			gamePanel.getScrollPane().getViewport()
					.addChangeListener(new ListenAdditionsScrolled());
			console = new Console(theGame, this);
			if(playMusic)
			{
				playlist = new MusicLibrary();
				playlist.playSongs();
			}

			/* 1st Layer: Setup HUD */
			HUD = new HUDPanel(theGame, this);
			HUD.setBounds(0, 0, width, height);
			HUD.addKeyListener(new KeyboardListener());
			this.add(HUD);

			/* 2nd Layer: Setup the gamePanel */
			gamePanel.addKeyListener(new KeyboardListener());
			this.add(gamePanel);

			System.out.println("Load saved game!");

			this.repaint();
			this.revalidate();
			theGameTimer = new Timer(GAME_TIMER / 60,
					new gameLoopListener());
			theGameTimer.start();
			theGameIsRunning = true;
		}
		else
		{
			this.remove(menuView);
			menuView = new MenuPanel(width, height, VERSION_INFO, this);
			menuView.setBounds(0, 0, width, height);
			this.add(menuView);
		}

	}

	/**
	 * The following three are start new game options.
	 */
	public void startNewGameWithMiniMapSize()
	{
		startTheGame(65, 65);
	}

	public void startNewGameWithRegularMapSize()
	{
		startTheGame(129, 129);
	}

	public void startNewGameWithLargeMapSize()
	{
		startTheGame(257, 257);
	}

	/**
	 * startTheGame: Call to start a new game with specific map size. (This
	 * should be called by start game options.)
	 * 
	 * @param mapWidth
	 *            : Width of the map.
	 * @param mapHeight
	 *            : Height of the map.
	 */
	private void startTheGame(int mapWidth, int mapHeight)
	{

		this.remove(menuView);

		// Setup gamePanel
		theGame = new GameState(mapWidth, mapHeight);
		gamePanel = new GamePanel(theGame, this);
		gamePanel.setBounds(0, 0, width, height);
		gamePanel.getScrollPane().getViewport()
				.addChangeListener(new ListenAdditionsScrolled());

		aMap = theGame.getMap();
		agents = theGame.getAgents();
		resourcepool = theGame.getResourcepool();

		// testing
		a1 = new Agent(aMap, aMap.getPlaneLocation().y,
				aMap.getPlaneLocation().x);
		Agent a2 = new Agent(aMap, aMap.getPlaneLocation().y,
				aMap.getPlaneLocation().x + 1);
		Agent a3 = new Agent(aMap, aMap.getPlaneLocation().y,
				aMap.getPlaneLocation().x + 2);
		Agent a4 = new Agent(aMap, aMap.getPlaneLocation().y,
				aMap.getPlaneLocation().x + 3);
		a1.setName("Bill");
		a2.setName("Sanket");
		a3.setName("Hercy");
		a4.setName("Collin");
		/*
		 * a1.setHunger((short) 2000); a2.setHunger((short) 2000);
		 * a3.setHunger((short) 2000); a1.setThirst((short) 2000);
		 * a2.setThirst((short) 2000); a3.setThirst((short) 2000);
		 * a1.setFatigue((short) 4000); a2.setFatigue((short) 4000);
		 * a3.setFatigue((short) 4000);
		 */
		agents.addAgent(a1);
		agents.addAgent(a2);
		agents.addAgent(a3);
		agents.addAgent(a4);
		/*
		 resourcepool.put(Resources.Wood, 200);
		 resourcepool.put(Resources.Airplane, 340);
		 resourcepool.put(Resources.Stone, 100);
		 resourcepool.put(Resources.Water, 20);
		 resourcepool.put(Resources.Iron, 520);
		 */

		// agents.command(new Build("Living Tent", aMap.getPlaneLocation().y +
		// 4, aMap.getPlaneLocation().x));
		/*
		 * a1.setLocation(18, 20); a2.setLocation(20, 20); a3.setLocation(20,
		 * 20);
		 */
		console = new Console(theGame, this);
		if(playMusic)
		{
			playlist = new MusicLibrary();
			playlist.playSongs();
		}

		/* 1st Layer: Setup HUD */
		HUD = new HUDPanel(theGame, this);
		HUD.setBounds(0, 0, width, height);
		HUD.addKeyListener(new KeyboardListener());
		this.add(HUD);

		/* 2nd Layer: Setup the gamePanel */
		gamePanel.addKeyListener(new KeyboardListener());
		this.add(gamePanel);

		System.out.println("Start a game with " + mapWidth + "x" + mapHeight
				+ " map size!");

		this.repaint();
		this.revalidate();
		theGameTimer = new Timer(GAME_TIMER / 60, new gameLoopListener());
		theGameTimer.start();
		theGameIsRunning = true;
	}

	private class gameLoopListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			theGame.update();
			if(theGame.hasLose() || theGame.hasWon())
			{
				winOrLoseCounter++;
				if(winOrLoseCounter == 1)
				{
					HUD.disableButtons();
					gamePanel.froze();
					if(theGame.hasLose())
					{
						HUD.getLoseAlertPanel().setVisible(true);
					}
					else if(theGame.hasWon())
					{
						HUD.getWinAlertPanel().setVisible(true);
					}
					theGameTimer.stop();
				}
			}
		}

	}

	public void saveTheGame()
	{

		File gameFile = new File("game.sav");
		FileOutputStream fileOut;
		try
		{
			fileOut = new FileOutputStream(gameFile);
			ObjectOutputStream objectout = new ObjectOutputStream(fileOut);
			objectout.writeObject(theGame);
			objectout.close();
		}
		catch(FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}

	public void hideHUD()
	{
		HUD.setVisible(false);
		// this.repaint();
		// this.revalidate();
	}

	public void displayHUD()
	{
		HUD.setVisible(true);
		// this.repaint();
		// this.revalidate();
	}

	public void refresh()
	{
		this.revalidate();
		this.repaint();
	}

	public void showMenu()
	{
		playlist.stopPlaying();
		this.remove(HUD);
		this.remove(gamePanel);
		this.revalidate();
		this.repaint();
		menuView = new MenuPanel(width, height, VERSION_INFO, this);
		menuView.setBounds(0, 0, width, height);
		this.add(menuView);
		theGameIsRunning = false;

	}

	/**
	 * ListenAdditionsScrolled: Refreash the GUI after scrolling.
	 */
	public class ListenAdditionsScrolled implements ChangeListener
	{
		@Override
		public void stateChanged(ChangeEvent e)
		{
			HUD.revalidate();
			HUD.repaint();
		}

	}

	public HUDPanel getHUD()
	{
		return HUD;
	}

	public GamePanel getGamePanel()
	{
		return gamePanel;
	}

	public boolean playMusic()
	{
		// TODO Auto-generated method stub
		return playMusic;
	}

	public void setMusicPlay(boolean onNoff)
	{
		if(!onNoff)
		{
			playMusic = false;
			playlist.stopPlaying();
		}
		else
		{
			playlist = new MusicLibrary();
			playlist.playSongs();
			playMusic = true;
		}
	}

	private class KeyboardListener implements KeyListener
	{

		@Override
		public void keyTyped(KeyEvent e)
		{
			if(e.getKeyChar() == '`' || e.getKeyChar() == '\\'
					|| e.getKeyChar() == '/')
			{
				console.setVisible(true);
			}
		}

		@Override
		public void keyPressed(KeyEvent e)
		{
			if(e.getKeyChar() == '`' || e.getKeyChar() == '\\'
					|| e.getKeyChar() == '/')
			{
				console.setVisible(true);
			}
		}

		@Override
		public void keyReleased(KeyEvent e)
		{
		}

	}

	public boolean getTheGameIsRunning()
	{
		return theGameIsRunning;
	}
	
	public Timer getTheGameTimer()
	{
		return theGameTimer;
	}
	
	public void restartTimer()
	{
		theGameTimer = new Timer(GAME_TIMER / 60, new gameLoopListener());
		theGameTimer.start();
	}
	
}
