/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */
package view;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

import javax.swing.*;

import main.MainViewController;
import model.Agent;
import model.GameState;
import model.Resources;

@SuppressWarnings("serial")
public class Console extends JFrame
{
	private int FRAME_WIDTH = 720;
	private int FRAME_HEIGHT = 480;
	private Color BACKGROUND_COLOR = Color.WHITE;
	private Color TEXT_COLOR = Color.DARK_GRAY;
	// private int height;
	// private int width;

	private MainViewController aFrame;
	@SuppressWarnings("unused")
	private GameState theGame;
	private JTextPane textPanel;
	private JScrollPane scrollPanel;
	private JTextField inputPanel;
	private JLabel commander;
	private String commands;
	private Timer killtimer;

	public Console(GameState theGame, MainViewController aFrame)
	{
		this.aFrame = aFrame;
		this.theGame = theGame;
		this.commands = "";

		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		this.setLayout(null);
		this.setResizable(false);
		this.setSize(FRAME_WIDTH, FRAME_HEIGHT);

		// this.setUndecorated(true);
		// this.setOpacity(0.7f);
		this.getContentPane().setBackground(BACKGROUND_COLOR);

		// height = (int) this.getContentPane().getSize().getHeight();
		// width = (int) this.getContentPane().getSize().getWidth();

		this.setTitle("Paradise Lost - Console");
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(screenSize.width / 2 - this.getSize().width / 2,
				screenSize.height / 2 - this.getSize().height / 2);

		// Setup textPane
		textPanel = new JTextPane();
		// textPanel.setOpaque(false);
		textPanel.setEditable(true);
		textPanel.setForeground(TEXT_COLOR);
		textPanel.setBackground(BACKGROUND_COLOR);
		textPanel.setText(commands);
		textPanel.setFont(new Font("Consolas", Font.PLAIN, textPanel.getFont()
				.getSize()));
		textPanel.setEditable(false);
		textPanel.setFocusable(true);
		this.add(textPanel);

		scrollPanel = new JScrollPane(textPanel);
		scrollPanel.setOpaque(false);
		scrollPanel.setBounds(8, 0, getWidth() - 8, getHeight() - 50);
		scrollPanel.setBorder(BorderFactory.createEmptyBorder());
		scrollPanel.getVerticalScrollBar().setPreferredSize(
				new Dimension(10, 0));
		scrollPanel
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPanel
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPanel.getVerticalScrollBar().setBackground(BACKGROUND_COLOR);
		scrollPanel.getVerticalScrollBar().setForeground(BACKGROUND_COLOR);
		this.add(scrollPanel);

		commander = new JLabel(">");
		commander.setOpaque(false);
		commander.setForeground(TEXT_COLOR);
		commander.setFont(new Font("Consolas", Font.BOLD, commander.getFont()
				.getSize()));
		commander.setHorizontalAlignment(SwingConstants.CENTER);
		commander.setBounds(0, getHeight() - 47, 20, 20);
		this.add(commander);

		inputPanel = new JTextField();
		inputPanel.setOpaque(false);
		inputPanel.setBorder(BorderFactory.createEmptyBorder());
		inputPanel.setBounds(20, getHeight() - 52, getWidth() - 20, 30);
		inputPanel.setForeground(TEXT_COLOR);
		inputPanel.setFont(new Font("Consolas", Font.BOLD, inputPanel.getFont()
				.getSize()));
		inputPanel.addActionListener(new commandListener());
		this.add(inputPanel);

		this.addKeyListener(new EscKeyListener());
		textPanel.addKeyListener(new EscKeyListener());
		textPanel.addKeyListener(new EscKeyListener());
		inputPanel.addKeyListener(new EscKeyListener());
		commander.addKeyListener(new EscKeyListener());

	}

	private class EscKeyListener implements KeyListener
	{

		@Override
		public void keyTyped(KeyEvent e)
		{
			if(e.getKeyCode() == KeyEvent.VK_ESCAPE || e.getKeyChar() == '`')
			{
				setVisible(false);
			}
			else
			{
				inputPanel.requestFocus(true);
			}
		}

		@Override
		public void keyPressed(KeyEvent e)
		{
			if(e.getKeyCode() == KeyEvent.VK_ESCAPE || e.getKeyChar() == '`')
			{
				setVisible(false);
			}
			else
			{
				inputPanel.requestFocus(true);
			}
		}

		@Override
		public void keyReleased(KeyEvent e)
		{
		}

	}

	private class commandListener implements ActionListener
	{
		@SuppressWarnings({ "static-access"})
		@Override
		public void actionPerformed(ActionEvent e)
		{
			String temp = inputPanel.getText();
			if(temp.toLowerCase().equals(""))
			{
				commands += "> \n";
			}
			else if(temp.toLowerCase().contains("theme:"))
			{
				if(temp.toLowerCase().equals("theme: dark"))
				{
					getContentPane().setBackground(Color.BLACK);
					scrollPanel.getVerticalScrollBar().setBackground(
							Color.BLACK);
					scrollPanel.getVerticalScrollBar().setForeground(
							Color.BLACK);
					textPanel.setBackground(Color.BLACK);
					commander.setForeground(Color.GREEN);
					inputPanel.setForeground(Color.GREEN);
					textPanel.setForeground(Color.GREEN);
				}
				else if(temp.toLowerCase().equals("theme: default"))
				{
					getContentPane().setBackground(BACKGROUND_COLOR);
					scrollPanel.getVerticalScrollBar().setBackground(
							BACKGROUND_COLOR);
					scrollPanel.getVerticalScrollBar().setForeground(
							BACKGROUND_COLOR);
					textPanel.setBackground(BACKGROUND_COLOR);
					commander.setForeground(TEXT_COLOR);
					inputPanel.setForeground(TEXT_COLOR);
					textPanel.setForeground(TEXT_COLOR);
				}
				else
				{
					commands += "> theme: <dark|default>\n";
				}
			}
			else if(temp.toLowerCase().contains("display:"))
			{
				if(temp.toLowerCase().contains("display: buildable"))
				{
					if(aFrame.getTheGameIsRunning())
					{
						if(temp.toLowerCase().equals("display: buildable: on"))
						{
							aFrame.getGamePanel().getMapPane()
									.setSHOW_MAP_TEST(true);
							commands += "> Display buildable is now on.\n";
						}
						else if(temp.toLowerCase().equals(
								"display: buildable: off"))
						{
							aFrame.getGamePanel().getMapPane()
									.setSHOW_MAP_TEST(false);
							commands += "> Display buildable is now off.\n";
						}
						else
						{
							commands += "> display: <displayable content>: <on|off>\n";
						}
					}
					else
					{
						commands += "> Error: This command can't be issued in the menu.\n";
					}
				}
				else
				{
					commands += "> display: <displayable content>: <on|off>\n";
				}
			}
			else if(temp.toLowerCase().contains("set:"))
			{
				if(temp.toLowerCase().contains("set: hide hud while scrolling"))
				{
					if(aFrame.getTheGameIsRunning())
					{
						if(temp.toLowerCase().equals(
								"set: hide hud while scrolling: on"))
						{
							aFrame.getGamePanel().setHIDE_HUD_WHEN_SCROLL(true);
							commands += "> Hide HUD while scrolling in the game is now on.\n";
						}
						else if(temp.toLowerCase().equals(
								"set: hide hud while scrolling: off"))
						{
							aFrame.getGamePanel()
									.setHIDE_HUD_WHEN_SCROLL(false);
							commands += "> Hide HUD while scrolling in the game is now off.\n";
						}
						else
						{
							commands += "> set: <setting>: <on|off>\n";
						}
					}
					else
					{
						commands += "> Error: This command can't be issued in the menu.\n";
					}
				}
			}
			else if(temp.toLowerCase().contains("save:"))
			{
				if(temp.toLowerCase().contains("save: map:"))
				{
					if(aFrame.getTheGameIsRunning())
					{
						if(temp.toLowerCase().equals(
								"save: map: png"))
						{
							try
							{
								aFrame.getGamePanel().getMapPane().saveMapIntoPNG("PNG");
							}
							catch(IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							commands += "> Map saved as a PNG image.\n";
						}
						else if(temp.toLowerCase().equals(
								"save: map: jpg"))
						{
							try
							{
								aFrame.getGamePanel().getMapPane().saveMapIntoPNG("JPG");
							}
							catch(IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							commands += "> Map saved as a JPG image.\n";
						}
						else if(temp.toLowerCase().equals(
								"save: map: jpeg"))
						{
							try
							{
								aFrame.getGamePanel().getMapPane().saveMapIntoPNG("JPEG");
							}
							catch(IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							commands += "> Map saved as a JPEG image.\n";
						}
						else if(temp.toLowerCase().equals(
								"save: map: gif"))
						{
							try
							{
								aFrame.getGamePanel().getMapPane().saveMapIntoPNG("GIF");
							}
							catch(IOException e1)
							{
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							commands += "> Map saved as a GIF image.\n";
						
						}
						else
						{
							commands += "> save: map: <PNG|JPG|JPEG|GIF>\n";
						}
					}
					else
					{
						commands += "> Error: This command can't be issued in the menu.\n";
					}
				}
				else
				{
					commands += "> save: <object>: <format>\n";
				}
			}
			else if(temp.toLowerCase().equals("killall"))
			{
				commands += "> Killing the system.";
				int counter = 0;

				killtimer = new Timer(1000, new killListener(counter));
				killtimer.start();

			}
			else if(temp.toLowerCase().equals("godmode: bwcgsshc"))
			{
				commands += "> God mode activated, you now have infinite resources!!!";
				
				theGame.getResourcepool().put(Resources.Wood, 999999999);
				theGame.getResourcepool().put(Resources.Stone, 999999999);
				theGame.getResourcepool().put(Resources.Water, 999999999);
				theGame.getResourcepool().put(Resources.Iron, 999999999);
				theGame.getResourcepool().put(Resources.Food, 999999999);
				for(int i = 0; i < 100; i++)
				{
					theGame.getAgents().addAgent(new Agent(theGame.getMap(), theGame.getMap().getPlaneLocation().y, theGame.getMap().getPlaneLocation().x));
				}

			}
			else if(temp.toLowerCase().equals("info"))
			{
				commands += "> Application info:" + " Version: "
						+ aFrame.VERSION_INFO + " (Build: " + aFrame.BUILD_INFO
						+ ")\n";
			}
			else if(temp.toLowerCase().equals("clean")
					|| temp.toLowerCase().equals("clear"))
			{
				commands = "";
			}
			else if(temp.toLowerCase().equals("help"))
			{
				commands += "> --- Command list ---\n  display: <displayable content>: <on|off>\n"
						+ "\tbuildable\n"
						+ "  info\n"
						+ "  save: <object>: <format>\n"
						+ "\tmap: <PNG|JPG|JPEG|GIF>\n"
						+ "  set: <setting>: <on|off>\n"
						+ "\thide hud while scrolling\n"
						+ "\tsave map to image\n"
						+ "  theme: <dark|default>\n" + "  killall\n";
			}
			else
			{
				commands += "> Unknow Command. Type \"help\" for help.\n";
			}
			textPanel.setText(commands);
			inputPanel.setText("");
		}
	}

	private class killListener implements ActionListener
	{
		private int counter;

		public killListener(int counter)
		{
			this.counter = counter;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			commands += ".";
			textPanel.setText(commands);
			counter += 1;
			if(counter >= 5)
			{
				killtimer.stop();
				System.out.println(counter);
				System.exit(0);
			}
		}

	}

	public void addCommandString(String str)
	{
		commands += "> " + str + "\n";
		textPanel.setText(commands);
	}
}
