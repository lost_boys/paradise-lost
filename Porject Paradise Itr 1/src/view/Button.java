/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package view;

import javax.swing.JButton;

@SuppressWarnings("serial")
public class Button extends JButton
{

	private String text;
	public boolean pressed;

	public Button(String string)
	{
		pressed = false;
		text = string;
		this.setFocusable(false);
		this.setText(text);
	}

}
