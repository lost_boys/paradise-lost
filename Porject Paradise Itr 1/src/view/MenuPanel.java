/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package view;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;

import javax.swing.*;

import java.io.File;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

import main.MainViewController;


@SuppressWarnings("serial")
public class MenuPanel extends JPanel implements ActionListener
{
	private BufferedImage backgroundImage, hoverImage, logoImage, treeImage;
	private Point bgImagePosition, logoImagePosition, hoverImagePosition,
			NSLabelPosition, OMLabelPosition, QALabelPosition, BKLabelPosition,
			miniLabelPosition, regularLabelPosition, largeLabelPosition,
			chooseSizeLabelPosition, warningLabelPosition, treeImagePosition;

	private int width, height;
	private boolean moveRight, moveDown;
	private Font camomileFont, elektoraFont;
	private String versionInfo;
	private Clip clip;
	private Timer subMenuTimer, revMenuTimer;
	private MainViewController aFrame;

	private JPanel newStoryHover, oldMemoriseHover, quitHover, backHover,
			miniHover, regularHover, largeHover;

	private AlertPanel alertPanel;

	public MenuPanel(int width, int height, String ver,
			MainViewController aFrame)
	{
		this.setOpaque(false);

		try
		{
			backgroundImage = ImageIO.read(new File("image/ui/background.png"));
			logoImage = ImageIO.read(new File("image/ui/logo.png"));
			treeImage = ImageIO.read(new File("image/ui/tree.png"));

			hoverImage = ImageIO.read(new File("image/ui/hover.png"));

			camomileFont = Font.createFont(Font.PLAIN, new File(
					"fonts/camomile.ttf"));
			elektoraFont = Font.createFont(Font.PLAIN, new File(
					"fonts/elektora.ttf"));
			camomileFont = camomileFont.deriveFont(Font.PLAIN, 20);
			elektoraFont = elektoraFont.deriveFont(Font.PLAIN, 20);
			GraphicsEnvironment ge = GraphicsEnvironment
					.getLocalGraphicsEnvironment();
			ge.registerFont(camomileFont);
			ge.registerFont(elektoraFont);

			AudioInputStream inputStream = AudioSystem
					.getAudioInputStream(new File(
							"music/Menu_Background_Music.wav"));

			clip = AudioSystem.getClip();
			clip.open(inputStream);
			clip.loop(Clip.LOOP_CONTINUOUSLY);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		int x = new Random().nextInt(backgroundImage.getWidth() - width);
		int y = new Random().nextInt(backgroundImage.getHeight() - height);

		this.aFrame = aFrame;
		this.versionInfo = ver;
		this.width = width;
		this.height = height;

		JLabel bgTempImage = new JLabel(new ImageIcon(backgroundImage));
		this.setBounds(x, y, 1024, 768);

		this.add(bgTempImage);

		bgImagePosition = new Point(-x, -y);
		logoImagePosition = new Point(180, 300);

		hoverImagePosition = new Point(1024, 440); // Hover

		moveRight = true;
		moveDown = true;
		Timer bgMovementTimer = new Timer(6000 / 60, this);
		bgMovementTimer.start();

		// Setup the alert panel
		alertPanel = new AlertPanel("alert",
				"Are you sure you want to power down the system?", "Cancel",
				"Shut Down", new CancelQuitListener(),
				new ConfirmQuitListener(), height);

		// New story
		int marginTop = 270;
		NSLabelPosition = new Point(width - 420, marginTop);

		newStoryHover = new JPanel();
		newStoryHover.setOpaque(false);
		newStoryHover.addMouseListener(new NewStoryListener());
		this.add(newStoryHover);
		newStoryHover.setBounds(600, 245, 210, 40);

		// Old memorise
		OMLabelPosition = new Point(width - 420, marginTop + 100);

		oldMemoriseHover = new JPanel();
		oldMemoriseHover.setOpaque(false);
		oldMemoriseHover.addMouseListener(new OldMemoriseListener());
		this.add(oldMemoriseHover);
		oldMemoriseHover.setBounds(600, 340, 295, 40);

		// Quit
		marginTop = marginTop + 100;
		QALabelPosition = new Point(width - 420, marginTop + 100);

		quitHover = new JPanel();
		quitHover.setOpaque(false);
		quitHover.addMouseListener(new QuitListener());
		this.add(quitHover);
		quitHover.setBounds(600, 440, 315, 40);

		// Back to main menu
		BKLabelPosition = new Point(1024, marginTop + 100);
		backHover = new JPanel();
		backHover.setOpaque(false);
		backHover.addMouseListener(new BackToMenuListener());
		backHover.setBounds(135, 440, 165, 40);

		// Small map option
		int topMargin = 270;
		miniLabelPosition = new Point(1224, topMargin);
		miniHover = new JPanel();
		miniHover.setOpaque(false);
		miniHover.addMouseListener(new MGListener());
		miniHover.setBounds(600, 245, 210, 40);

		// Regular map option
		regularLabelPosition = new Point(1224, topMargin + 100 - 1);
		regularHover = new JPanel();
		regularHover.setOpaque(false);
		regularHover.addMouseListener(new RGListener());
		regularHover.setBounds(600, 340, 295, 40);

		// Large map option
		topMargin = topMargin + 100;
		largeLabelPosition = new Point(1224, topMargin + 100 - 1);
		largeHover = new JPanel();
		largeHover.setOpaque(false);
		largeHover.addMouseListener(new LGListener());
		largeHover.setBounds(600, 440, 315, 40);

		// Tree list image
		treeImagePosition = new Point(1024, 262);

		// Choose map size label
		chooseSizeLabelPosition = new Point(1024, 140);

		// New game warning label
		warningLabelPosition = new Point(1024, 162);
	}

	public void paintComponent(Graphics g)
	{
		Graphics2D g2 = (Graphics2D) g;

		// adding background image
		g2.drawImage(backgroundImage, bgImagePosition.x, bgImagePosition.y,
				null);

		// adding hover image
		g2.drawImage(hoverImage, hoverImagePosition.x, hoverImagePosition.y,
				null);

		// adding menu
		g2.setColor(Color.WHITE);
		g2.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 36));

		// New story
		g2.drawString("New Story", NSLabelPosition.x, NSLabelPosition.y);

		// Old memorise
		g2.drawString("Old Memories", OMLabelPosition.x, OMLabelPosition.y);

		// Back button
		g2.drawString("<< Back", BKLabelPosition.x, BKLabelPosition.y);

		// Quit adventure
		g2.drawString("Quit Adventure", QALabelPosition.x, QALabelPosition.y);

		// Mini map option
		g2.drawString("Mini", miniLabelPosition.x, miniLabelPosition.y);

		// Regular map option
		g2.drawString("Regular", regularLabelPosition.x, regularLabelPosition.y);

		// Large map option
		g2.drawString("Large", largeLabelPosition.x, largeLabelPosition.y);

		// Draw tree list view image
		g2.drawImage(treeImage, treeImagePosition.x, treeImagePosition.y, null);

		// Choose game size label
		g2.drawString("Choose Your Memory Access", chooseSizeLabelPosition.x,
				chooseSizeLabelPosition.y);

		// New game warning label
		g2.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 14));
		g2.setColor(Color.RED);
		g2.drawString(
				"\u2022 Warning: You'll completely forget your previous memories if you start a new story.",
				warningLabelPosition.x, warningLabelPosition.y);

		// Adding logo
		g2.drawImage(logoImage, logoImagePosition.x, logoImagePosition.y, null);

		// Adding info
		g2.setColor(Color.WHITE);
		g2.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 12));
		g2.drawString(
				"Copyright \u00a9 2014 Lost Boys Game Studio. All rights reserved.",
				10, height - 10);
		g2.drawString("Paradise OS " + versionInfo, width - 120, height - 10);
	}

	public void actionPerformed(ActionEvent e)
	{
		if(new Random().nextInt(1000) >= 990)
		{
			moveRight = new Random().nextBoolean();
			moveDown = new Random().nextBoolean();
		}

		if(bgImagePosition.x + backgroundImage.getWidth() <= width)
		{
			moveRight = true;
		}
		else if(bgImagePosition.x >= 0)
		{
			moveRight = false;
		}

		if(bgImagePosition.y + backgroundImage.getHeight() <= height)
		{
			moveDown = true;
		}
		else if(bgImagePosition.y >= 0)
		{
			moveDown = false;
		}

		if(moveRight)
		{
			bgImagePosition.x += 1;
		}
		else
		{
			bgImagePosition.x -= 1;
		}

		if(moveDown)
		{
			bgImagePosition.y += 1;
		}
		else
		{
			bgImagePosition.y -= 1;
		}
		repaint();
	}

	/**
	 * NewStoryListener: Enter the sub-menu of the new story options when mouse
	 * clicked on the object.
	 */
	private class NewStoryListener implements MouseListener
	{

		@Override
		public void mouseClicked(MouseEvent e)
		{
			remove(newStoryHover);
			remove(oldMemoriseHover);
			remove(quitHover);

			hoverImagePosition.x = 1024;
			hoverImagePosition.y = 440;
			repaint();
			subMenuTimer = new Timer(0, new ShowSubMenuListener());
			subMenuTimer.start();

			add(backHover);
			add(miniHover);
			add(regularHover);
			add(largeHover);
		}

		@Override
		public void mousePressed(MouseEvent e)
		{
		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
		}

		@Override
		public void mouseEntered(MouseEvent e)
		{
			hoverImagePosition.x = 600;
			hoverImagePosition.y = 240;
			repaint();
		}

		@Override
		public void mouseExited(MouseEvent e)
		{
			hoverImagePosition.x = 1024;
			hoverImagePosition.y = 440;
			repaint();
		}

	}

	/**
	 * ShowSubMenuListener: Show sub-menu of the new story options.
	 */
	private class ShowSubMenuListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			int NSStop = 100;
			int OMStop = -290;
			int QAStop = -320;

			if(NSLabelPosition.x > NSStop)
			{
				NSLabelPosition.x -= 4;
			}
			if(OMLabelPosition.x > OMStop)
			{
				OMLabelPosition.x -= 5;
			}
			if(QALabelPosition.x > QAStop)
			{
				QALabelPosition.x -= 6;
			}
			if(logoImagePosition.x > -250)
			{
				logoImagePosition.x -= 6;
			}
			if(BKLabelPosition.x > 135)
			{
				BKLabelPosition.x -= 6;
			}

			if(miniLabelPosition.x > width - 420)
			{
				miniLabelPosition.x -= 4;
			}
			if(regularLabelPosition.x > width - 420)
			{
				regularLabelPosition.x -= 4;
			}
			if(largeLabelPosition.x > width - 420)
			{
				largeLabelPosition.x -= 4;
			}
			if(treeImagePosition.x > 380)
			{
				treeImagePosition.x -= 4;
			}
			if(chooseSizeLabelPosition.x > 100)
			{
				chooseSizeLabelPosition.x -= 6;
			}
			if(warningLabelPosition.x > 105)
			{
				warningLabelPosition.x -= 6;
			}

			if(NSLabelPosition.x <= NSStop && OMLabelPosition.x <= OMStop
					&& QALabelPosition.x <= QAStop)
			{
				subMenuTimer.stop();
			}
			repaint();
		}

	}

	/**
	 * BackToMenuListener: Back to the main menu when mouse clicked on the
	 * object.
	 */
	private class BackToMenuListener implements MouseListener
	{

		@Override
		public void mouseClicked(MouseEvent e)
		{
			remove(backHover);
			remove(miniHover);
			remove(regularHover);
			remove(largeHover);

			hoverImagePosition.x = 1024;
			hoverImagePosition.y = 440;
			repaint();
			revMenuTimer = new Timer(0, new RevertMenuListener());
			revMenuTimer.start();

			add(newStoryHover);
			add(oldMemoriseHover);
			add(quitHover);
		}

		@Override
		public void mousePressed(MouseEvent e)
		{
		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
		}

		@Override
		public void mouseEntered(MouseEvent e)
		{
			hoverImagePosition.x = -150;
			hoverImagePosition.y = 440;
			repaint();
		}

		@Override
		public void mouseExited(MouseEvent e)
		{
			hoverImagePosition.x = 1024;
			hoverImagePosition.y = 440;
			repaint();
		}

	}

	/**
	 * RevertMenuListener: Revert the menu.
	 */
	private class RevertMenuListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			int stopper = width - 420;
			if(NSLabelPosition.x < stopper)
			{
				NSLabelPosition.x += 2;
			}
			if(OMLabelPosition.x < stopper)
			{
				OMLabelPosition.x += 4;
			}
			if(QALabelPosition.x < stopper)
			{
				QALabelPosition.x += 4;
			}
			if(logoImagePosition.x < 180)
			{
				logoImagePosition.x += 2;
			}
			if(BKLabelPosition.x < 1024)
			{
				BKLabelPosition.x += 4;
			}

			if(miniLabelPosition.x < 1224)
			{
				miniLabelPosition.x += 3;
			}
			if(regularLabelPosition.x < 1224)
			{
				regularLabelPosition.x += 3;
			}
			if(largeLabelPosition.x < 1224)
			{
				largeLabelPosition.x += 3;
			}
			if(treeImagePosition.x < 1024)
			{
				treeImagePosition.x += 3;
			}
			if(chooseSizeLabelPosition.x < 1024)
			{
				chooseSizeLabelPosition.x += 6;
			}
			if(warningLabelPosition.x < 1024)
			{
				warningLabelPosition.x += 6;
			}

			if(NSLabelPosition.x >= stopper && OMLabelPosition.x >= stopper
					&& QALabelPosition.x >= stopper)
			{
				revMenuTimer.stop();
			}
			repaint();
		}

	}

	/**
	 * OldMemoriseListener: Load saved game when mouse clicked on the object.
	 */
	private class OldMemoriseListener implements MouseListener
	{

		@Override
		public void mouseClicked(MouseEvent e)
		{
			// TODO: Load old memorise.
			clip.stop();
			aFrame.loadGame();
			System.out.println("Load game");
		}

		@Override
		public void mousePressed(MouseEvent e)
		{
		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
		}

		@Override
		public void mouseEntered(MouseEvent e)
		{
			hoverImagePosition.x = 600;
			hoverImagePosition.y = 340;
			repaint();
		}

		@Override
		public void mouseExited(MouseEvent e)
		{
			hoverImagePosition.x = 1024;
			hoverImagePosition.y = 440;
			repaint();
		}

	}

	/**
	 * MGListener: Start a new game with mini size map when mouse clicked on the
	 * object.
	 */
	private class MGListener implements MouseListener
	{

		@Override
		public void mouseClicked(MouseEvent e)
		{
			// Start a new game with small map scale.
			clip.stop();
			aFrame.startNewGameWithMiniMapSize();
		}

		@Override
		public void mousePressed(MouseEvent e)
		{
		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
		}

		@Override
		public void mouseEntered(MouseEvent e)
		{
			hoverImagePosition.x = 600;
			hoverImagePosition.y = 240;
			repaint();
		}

		@Override
		public void mouseExited(MouseEvent e)
		{
			hoverImagePosition.x = 1024;
			hoverImagePosition.y = 440;
			repaint();
		}

	}

	/**
	 * RGListener: Start a new game with regular size map when mouse clicked on
	 * the object.
	 */
	private class RGListener implements MouseListener
	{

		@Override
		public void mouseClicked(MouseEvent e)
		{
			// Start a new game with median map scale.
			clip.stop();
			aFrame.startNewGameWithRegularMapSize();
		}

		@Override
		public void mousePressed(MouseEvent e)
		{
		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
		}

		@Override
		public void mouseEntered(MouseEvent e)
		{
			hoverImagePosition.x = 600;
			hoverImagePosition.y = 340;
			repaint();
		}

		@Override
		public void mouseExited(MouseEvent e)
		{
			hoverImagePosition.x = 1024;
			hoverImagePosition.y = 440;
			repaint();
		}

	}

	/**
	 * LGListener: Start a new game with large size map when mouse clicked on
	 * the object.
	 */
	private class LGListener implements MouseListener
	{

		@Override
		public void mouseClicked(MouseEvent e)
		{
			// Start a new game with large map scale.
			clip.stop();
			aFrame.startNewGameWithLargeMapSize();
		}

		@Override
		public void mousePressed(MouseEvent e)
		{
		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
		}

		@Override
		public void mouseEntered(MouseEvent e)
		{
			hoverImagePosition.x = 600;
			hoverImagePosition.y = 440;
			repaint();
		}

		@Override
		public void mouseExited(MouseEvent e)
		{
			hoverImagePosition.x = 1024;
			hoverImagePosition.y = 440;
			repaint();
		}

	}

	/**
	 * QuitListener: Quite the entire program when mouse clicked on the object.
	 */
	private class QuitListener implements MouseListener
	{

		@Override
		public void mouseClicked(MouseEvent e)
		{
			// Ask to kill application.
			hoverImagePosition.x = 1024;
			remove(newStoryHover);
			remove(oldMemoriseHover);
			remove(quitHover);
			add(alertPanel);
		}

		@Override
		public void mousePressed(MouseEvent e)
		{
		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
		}

		@Override
		public void mouseEntered(MouseEvent e)
		{
			hoverImagePosition.x = 600;
			hoverImagePosition.y = 440;
			repaint();
		}

		@Override
		public void mouseExited(MouseEvent e)
		{
			hoverImagePosition.x = 1024;
			hoverImagePosition.y = 440;
			repaint();
		}

	}

	/**
	 * CancelQuitListener: Close the alert and abort quit.
	 */
	private class CancelQuitListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			// Revert to the main menu and close the alert.
			remove(alertPanel);

			add(newStoryHover);
			add(oldMemoriseHover);
			add(quitHover);

		}
	}

	/**
	 * ConfirmQuitListener: Close the alert and abort quit.
	 */
	private class ConfirmQuitListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			// Kill the entire application.
			System.exit(0);
		}
	}

	// A test of mouse listener
	@SuppressWarnings("unused")
	private class mouseTrackListener implements MouseListener
	{

		@Override
		public void mouseClicked(MouseEvent e)
		{
			Point mousePosition = MouseInfo.getPointerInfo().getLocation();
			SwingUtilities.convertPointFromScreen(mousePosition,
					e.getComponent());
			System.out.println("Mouse Clicked: X: " + mousePosition.x + ", Y: "
					+ mousePosition.y);
		}

		@Override
		public void mousePressed(MouseEvent e)
		{
		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
		}

		@Override
		public void mouseEntered(MouseEvent e)
		{
			Point mousePosition = MouseInfo.getPointerInfo().getLocation();
			SwingUtilities.convertPointFromScreen(mousePosition,
					e.getComponent());
			System.out.println("Mouse Entered: X: " + mousePosition.x + ", Y: "
					+ mousePosition.y);
		}

		@Override
		public void mouseExited(MouseEvent e)
		{
			Point mousePosition = MouseInfo.getPointerInfo().getLocation();
			SwingUtilities.convertPointFromScreen(mousePosition,
					e.getComponent());
			System.out.println("Mouse Exited: X: " + mousePosition.x + ", Y: "
					+ mousePosition.y);
		}

	}
}
