/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package view;

import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import model.*;



@SuppressWarnings("serial")
public class MapPanel extends JPanel
{

	
	private Terrains[][] maps;
	private Map aMap;
	// private List<Image> images;
	private BufferedImage[] jungle = new BufferedImage[4],
			rock = new BufferedImage[3],
			tree = new BufferedImage[3],
			beach = new BufferedImage[4],
			rocky = new BufferedImage[3],
			water2Rocky2Jungle = new BufferedImage[16],
			water2Sand = new BufferedImage[16],
			sand2Rocky = new BufferedImage[16],
			sand2Jungle = new BufferedImage[16],
			jungle2Rocky = new BufferedImage[16];
	
	private boolean SHOW_MAP_TEST = false;
	
	private Timer planeAnimationTimer;
	
	private BufferedImage buildHoverG, buildHoverR, buildHoverD, buildingHover, containerImage, water, paintImage, crashedPlane, crashedPlane2, agentImage, pointer;
	private int randomSeed = 0;
	private GameState theGame;
	
	private boolean displayRBBlocks, drawPointer;
	private int blockWidth;
	private int blockHeight;
	private Point mousePosition;
	
	//private boolean [][] buildingOccupied;
	//private ArrayList<Building> buildings;
	private ArrayList<Agent> agentsStillAlive;
	private Font camomileFont;
	private Font elektoraFont;
	private Agent trackingAgent;

	
	public MapPanel(GameState theGame)
	{
		this.theGame = theGame;
		//this.setBackground(Color.GREEN);
		randomSeed = new Random().nextInt(10000);
		this.setOpaque(false);
		this.displayRBBlocks = false;
		this.aMap = theGame.getMap();
		this.revalidate();
		this.setPreferredSize(new Dimension(aMap.getHeight() * 24, aMap
				.getWidth() * 24));
		
		this.mousePosition = new Point(0, 0);
		this.blockWidth = 1;
		this.blockHeight = 1;
		/*
		this.buildings = new ArrayList<Building>();
		this.buildingOccupied = new boolean [aMap.getWidth()][aMap.getHeight()];
		for(int i = 0; i < buildingOccupied.length; i++)
		{
			for(int j = 0; j < buildingOccupied[i].length; j++)
			{
				buildingOccupied[i][j] = false;
			}
		}
		*/
		this.agentsStillAlive = new ArrayList<Agent>();
		this.agentsStillAlive = theGame.getAgents().getAliveAgents();
		this.drawPointer = false;
		this.trackingAgent = null;
		

		try
		{
			BufferedImage resourceSpriteSheet = ImageIO.read(new File("image/gamepackage/resources.png"));
			for(int i = 0; i < 3; i++)
			{
				rock[i] = resourceSpriteSheet.getSubimage(i*24, 0, 24, 24);
				tree[i] = resourceSpriteSheet.getSubimage(i*24, 24, 24, 24);
			}
			BufferedImage spriteSheet = ImageIO.read(new File("image/gamepackage/land.png"));
			for(int i = 0; i < 4; i++)
			{
				jungle[i] = spriteSheet.getSubimage(i*24, 0, 24, 24);
				beach[i] = spriteSheet.getSubimage(i*24, 24, 24, 24);
				if(i < 3)
				{
					rocky[i] = spriteSheet.getSubimage(i*24, 48, 24, 24);
				}
				if(i == 0)
				{
					water = spriteSheet.getSubimage(i*24, 72, 24, 24);
					buildingHover = spriteSheet.getSubimage((i+1)*24, 72, 24, 24);
					containerImage = spriteSheet.getSubimage((i+2)*24, 72, 24, 24);
				}
			}
			camomileFont = Font.createFont(Font.PLAIN, new File(
					"fonts/camomile.ttf"));
			elektoraFont = Font.createFont(Font.PLAIN, new File(
					"fonts/elektora.ttf"));
			camomileFont = camomileFont.deriveFont(Font.PLAIN, 20);
			elektoraFont = elektoraFont.deriveFont(Font.PLAIN, 20);
			GraphicsEnvironment ge = GraphicsEnvironment
					.getLocalGraphicsEnvironment();
			ge.registerFont(camomileFont);
			ge.registerFont(elektoraFont);
			
			spriteSheet = ImageIO.read(new File("image/gamepackage/terrains.png"));
			pointer = ImageIO.read(new File("image/ui/pointer.png"));
			buildHoverG = ImageIO.read(new File("image/ui/build_hover.png")).getSubimage(0, 0, 24, 24);
			buildHoverR = ImageIO.read(new File("image/ui/build_hover.png")).getSubimage(24, 0, 24, 24);
			buildHoverD = ImageIO.read(new File("image/ui/build_hover.png")).getSubimage(48, 0, 24, 24);
			getFromSpriteSheetGroup(water2Rocky2Jungle, spriteSheet, 0);
			getFromSpriteSheetGroup(water2Sand, spriteSheet, 1);
			getFromSpriteSheetGroup(sand2Rocky, spriteSheet, 2);
			getFromSpriteSheetGroup(sand2Jungle, spriteSheet, 3);
			getFromSpriteSheetGroup(jungle2Rocky, spriteSheet, 4);
			agentImage = ImageIO.read(new File("image/gamepackage/agent.png"));
			crashedPlane = ImageIO.read(new File("image/gamepackage/crashed_plane.png"));
			crashedPlane2 = ImageIO.read(new File("image/gamepackage/crashed_plane2.png"));
			
			this.paintMap();
			
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		catch(FontFormatException e)
		{
			e.printStackTrace();
		}
		
		planeAnimationTimer = new Timer(500, new PlaneAnimation());
		
		this.addMouseMotionListener(new trackMouseMovement());
	}
	/*
	public boolean[][] getBuildingOccupied()
	{
		return buildingOccupied;
	}
	*/
	/**
	 * getFromSpriteSheetGroup: Loading a specific group of terrains.
	 * @param image: Object image array writing to.
	 * @param spriteSheet: Object terrains source image loarding from.
	 * @param selectedGroup: Select the group on the terrains source image.
	 */
	private void getFromSpriteSheetGroup(BufferedImage[] image, BufferedImage spriteSheet, int selectedGroup)
	{
		int group = selectedGroup * 4;
		for(int i = 0; i < 12; i++)
		{
			if(i < 4)
			{
				image[i] = spriteSheet.getSubimage(i*24, group*24, 24, 24);
			}
			else if(i < 8)
			{
				image[i] = spriteSheet.getSubimage((i-4)*24, (group+1)*24, 24, 24);
			}
			else if(i < 10)
			{
				image[i] = spriteSheet.getSubimage((i-8)*24, (group+2)*24, 24, 24);
			}
			else
			{
				image[i] = spriteSheet.getSubimage((i-10)*24, (group+3)*24, 24, 24);
			}
		}
		image[12] = spriteSheet.getSubimage(24*2, (group+2)*24, 24, 24);
		image[13] = spriteSheet.getSubimage(24*3, (group+2)*24, 24, 24);
		image[14] = spriteSheet.getSubimage(24*2, (group+3)*24, 24, 24);
		image[15] = spriteSheet.getSubimage(24*3, (group+3)*24, 24, 24);
	}

	/**
	 * reGenerateAMap: Re-generate a new game map.
	 * @param aMap: The game map.
	 */
	public void reGenerateAMap(Map aMap)
	{
		this.aMap = aMap;
		randomSeed = new Random().nextInt(10000);
		this.repaint();
	}
	/**
	 * reDecorations: Re-decoration a map with random decorations.
	 */
	public void reDecorations()
	{
		randomSeed = new Random().nextInt(10000);
		this.repaint();
	}
	
	/**
	 * paintMap: Paint a map, store the map into an image called "paintImage" 
	 * so that paintComponent can only draw the "paintImage" instead of  
	 * redrawing the entire map every time.
	 */
	public void paintMap()
	{
		paintImage = new BufferedImage(aMap.getHeight()*24, aMap.getWidth()*24, BufferedImage.TYPE_3BYTE_BGR);
		Graphics gc = paintImage.createGraphics();
		Graphics2D g3 = (Graphics2D) gc;
		drawMap(g3, aMap);
	}
	
	private class PlaneAnimation implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			BufferedImage tempCrashedPlane = crashedPlane;
			crashedPlane = crashedPlane2;
			crashedPlane2 = tempCrashedPlane;
		}
		
	}
	
	@Override
	public void paintComponent(Graphics g)
	{
		//super.paintComponent(g);
		planeAnimationTimer.start();
		//BufferedImage tempCrashedPlane = crashedPlane;
		//crashedPlane = crashedPlane2;
		//crashedPlane2 = tempCrashedPlane;
		
		Graphics2D g2 = (Graphics2D) g;
		

		g2.setColor(Color.WHITE);
		g2.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 12));
		g2.drawImage(paintImage, 0, 0, null);
		
		
		
		// Test build-able.
		if(SHOW_MAP_TEST)
		{
			for(int i = 0; i < aMap.getWidth(); i++)
			{
				for(int j = 0; j < aMap.getHeight(); j++)
				{
					if(aMap.getTile(i, j).getResource() == null
						&& aMap.getTile(i, j).getBuilding() == null
						&& (!theGame.getBuildingsOccupied()[j][i]))
					{
						g2.drawImage(buildHoverG, j*24, i*24, null);
					}
					else
					{
						g2.drawImage(buildHoverR, j*24, i*24, null);
					}
				}
			}
		}
		
		
		
		// Draw crahsed plane
		g2.drawImage(crashedPlane, aMap.getPlaneLocation().x*24, aMap.getPlaneLocation().y*24, null);
		
		
		
		
		
		// Display buildings on the map.
		g2.setFont(new Font(camomileFont.getFamily(), Font.BOLD, 10));
		for(Building aBuilding : theGame.getBuildingsUnderConstruction())
		{
			Point buildingLocation = aBuilding.getBuildingLocation();
			BufferedImage aBuildingHover = buildHoverD;
			if(aBuilding.buildCompeleted(aMap))
			{
				g2.setColor(Color.BLACK);
				aBuildingHover = buildingHover;
			}
			for(int Y = 0; Y < aBuilding.getBlockHeight(); Y++)
			{
				for(int X = 0; X < aBuilding.getBlockWidth(); X++)
				{
					g2.drawImage(aBuildingHover, (buildingLocation.y+Y)*24, (buildingLocation.x+X)*24, null);
				}
			}
			//System.out.println(aBuilding.getName());
			g2.drawString(aBuilding.getName(), (buildingLocation.y)*24 + 2, (buildingLocation.x)*24 + 12);
			g2.setColor(Color.WHITE);
			
		}
		g2.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 12));
		
		// Display containers on the map.
		for(Point aContainer : theGame.getContainerList())
		{
			g2.drawImage(containerImage, (aContainer.y)*24, (aContainer.x)*24, null);
		}
		
		
		// Display agents on the map.
		this.agentsStillAlive = theGame.getAgents().getAliveAgents();
		for(Agent a : agentsStillAlive)
		{
			Point agentLocation = a.getLocation();

			g2.drawImage(agentImage, (agentLocation.y)*24, (agentLocation.x)*24, null);
		}
		if(drawPointer && trackingAgent != null)
		{
			g2.drawImage(pointer, (trackingAgent.getLocation().y)*24, (trackingAgent.getLocation().x - 1)*24, null);
		}
		
		
		if(displayRBBlocks)
		{
			//System.out.println(mousePosition.x + " " + mousePosition.y);
			//SwingUtilities.convertPointFromScreen(mousePosition, e.getComponent());
			
			// Check build-able. Make sure the block size is changed for different buildings.
			Point coveredIndex = new Point((int)mousePosition.x/24, (int)mousePosition.y/24);
			
			for(int X = 0; X < blockWidth; X++)
			{
				for(int Y = 0; Y < blockHeight; Y++)
				{
					int drawX = coveredIndex.x+X;
					int drawY = coveredIndex.y+Y;
					if(drawX < aMap.getWidth() && drawY < aMap.getHeight())
					{
						if(aMap.getTile(coveredIndex.y + Y, coveredIndex.x + X).getResource() == null 
						&& aMap.getTile(coveredIndex.y + Y, coveredIndex.x + X).getBuilding() == null
							&& (!theGame.getBuildingsOccupied()[coveredIndex.x + X][coveredIndex.y + Y]))
						{
							g2.drawImage(buildHoverG, (coveredIndex.x+X)*24, (coveredIndex.y+Y)*24, null);
						}
						else
						{
							g2.drawImage(buildHoverR, (coveredIndex.x+X)*24, (coveredIndex.y+Y)*24, null);
						}
					}
				}
			}
			
		}
	}
	
	public void setPointer(boolean onNoff)
	{
		drawPointer = onNoff;
	}
	public void setTrackingAgent(Agent aAgent)
	{
		trackingAgent = aAgent;
	}
	
	/**
	 * trackMouseMovement:  Tracks the mouse position live.
	 */
	private class trackMouseMovement implements MouseMotionListener
	{

		@Override
		public void mouseDragged(MouseEvent e)
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseMoved(MouseEvent e)
		{
			mousePosition = e.getPoint();//MouseInfo.getPointerInfo().getLocation();
			//SwingUtilities.convertPointFromScreen(mousePosition, e.getComponent());
			
			
		}
		
	}
	
	
	/**
	 * saveMapIntoPNG: Save the current map into a PNG file named as "Game_Map.png".
	 * @throws IOException
	 */
	public void saveMapIntoPNG(String format) throws IOException
	{
		Date date = new Date();
		try
		{
			ImageIO.write(paintImage, format, new File("Map - " + date.toString() + "." + format.toLowerCase()));
		}
		catch(IOException e1)
		{
			e1.printStackTrace();
		}
    }

	/**
	 * drawMap: Draw the graphic map with the give map type.
	 * @param g2: The object Graphics2D object writing to.
	 * @param aMap: The object map loading from.
	 */
	private void drawMap(Graphics2D g2, Map aMap)
	{
		
		int x = 0, y = 0;
		int randSeed = 0;
		boolean randBool = true;
		// String str = maps;
		maps = new Terrains[aMap.getWidth()][aMap.getHeight()];
		
		this.aMap = aMap;
		
		for(int i = 0; i < aMap.getWidth(); i++)
		{
			for(int j = 0; j < aMap.getHeight(); j++)
			{
				maps[i][j] = aMap.getTile(i, j).getTerrain();
			}
		}
		for(int i = 0; i < aMap.getHeight(); i++)
		{
			randSeed = new Random(randSeed).nextInt(10000);
			
			for(int j = 0; j < aMap.getWidth(); j++)
			{
				randSeed = new Random(randomSeed*(aMap.getHeight()-i)*(aMap.getWidth()-j)).nextInt(1000);
				randBool = new Random(randomSeed*j*i).nextBoolean();
				
				x = j * 24;
				y = i * 24;
				
				// Beach
				if(aMap.getTile(i, j).getTerrain().equals(Terrains.Beach))
				{	
					if(randSeed < 800)
					{
						if(randBool)
						{
							g2.drawImage(beach[0], x, y, 24, 24, null);
						}
						else
						{
							g2.drawImage(beach[1], x, y, 24, 24, null);
						}
					}
					else if(randSeed >= 800 && randSeed <= 980)
					{
						g2.drawImage(beach[2], x, y, 24, 24, null);
					}
					else
					{
						g2.drawImage(beach[3], x, y, 24, 24, null);
						
					}
					SmartEdge(sand2Jungle, x, y, g2, maps, i, j, Terrains.Jungle, Terrains.Rocky, Terrains.Water);
					//SmartEdge(sand2Rocky, x, y, g2, maps, i, j, Terrains.Rocky, Terrains.Rocky, Terrains.Rocky);
				}

				// Water
				else if(aMap.getTile(i, j).getTerrain().equals(Terrains.Water))
				{
					
					g2.drawImage(water, x, y, 24, 24, null);
					
					SmartEdge(water2Rocky2Jungle, x, y, g2, maps, i, j, Terrains.Jungle, Terrains.Rocky, Terrains.Beach);
					//SmartEdge(water2Sand, x, y, g2, maps, i, j, Terrains.Beach, Terrains.Beach, Terrains.Beach);
				}

				// Jungle
				else if(aMap.getTile(i, j).getTerrain().equals(Terrains.Jungle))
				{
					
					if(randSeed < 800)
					{
						g2.drawImage(jungle[0], x, y, 24, 24, null);
						
					}
					else if(randSeed < 900)
					{
						g2.drawImage(jungle[1], x, y, 24, 24, null);
					}
					else if(randSeed <= 960)
					{
						g2.drawImage(jungle[2], x, y, 24, 24, null);
					}
					else
					{
						g2.drawImage(jungle[3], x, y, 24, 24, null);
					}
				}

				// Rocky
				else if(aMap.getTile(i, j).getTerrain().equals(Terrains.Rocky))
				{
					if(randSeed < 800)
					{
						if(randBool)
						{
							g2.drawImage(rocky[0], x, y, 24, 24, null);
						}
						else
						{
							g2.drawImage(rocky[1], x, y, 24, 24, null);
						}
					}
					else
					{
						g2.drawImage(rocky[2], x, y, 24, 24, null);
					}
					
					SmartEdge(jungle2Rocky, x, y, g2, maps, i, j, Terrains.Jungle, Terrains.Water, Terrains.Beach);
				}
				if(aMap.getTile(i, j).getResource() != null)
				{
					
					if(aMap.getTile(i, j).getResource().equals(Resources.Wood))
					{
						int randomNum = new Random().nextInt(2);
						g2.drawImage(tree[randomNum], x, y, 24, 24, null);
					}
					if(aMap.getTile(i, j).getResource().equals(Resources.Food))
					{
						//int randomNum = new Random().nextInt(2);
						g2.drawImage(tree[2], x, y, 24, 24, null);
					}
					if(aMap.getTile(i, j).getResource().equals(Resources.Stone))
					{
						int randomNum = new Random().nextInt(3);

						g2.drawImage(rock[randomNum], x, y, 24, 24, null);
					}
				}
				
				// TODO: Container!!!
				/*
				if(getItem(aMap.getTile(i, j)) != null)
				{
					g2.drawImage(container, x, y, 24, 24, null);
				}*/
			}	
			
		}
		
		// Draw crashed plane
		//g2.drawImage(crashedPlane, aMap.getPlaneLocation().x*24, aMap.getPlaneLocation().y*24, null);
	}
	
	/**
	 * SmartEdge: THE SMART EDGE! Draw on a specific location on 
	 * the object Grapgics2d object, a correct map tile image will 
	 * be generated after compare a, b, c three kind of input 
	 * terrain types (a, b, c can be the same terrain type). 
	 * 
	 * @param image: Takes the spritesheet image object stored all kinds of possible drawing situation.
	 * @param x
	 * @param y
	 * @param g2: The object Graphics2D object writing to.
	 * @param array: The object terrain array.
	 * @param i: Current row index of the map.
	 * @param j: Current column index of the map.
	 * @param a: First comparable terrain object. 
	 * @param b: Second comparable terrain object.
	 * @param c: Third comparable terrain object.
	 */
	private void SmartEdge(BufferedImage[] image, int x, int y, Graphics2D g2, Terrains[][] array, int i, int j, Terrains a, Terrains b, Terrains c)
	{
		Terrains ter = array[i][j];
		if(i == 0 || j == 0 || i == array.length-1 || j == array[0].length-1)
		{
			
		}
		else if(array[i][j].equals(ter))
		{
			
			/** 15 */
			/*   | X |   */
			/* X | ~ | X */
			/*   | X |   */
			if((array[i-1][j].equals(a) || array[i-1][j].equals(b) || array[i-1][j].equals(c))
					
				&& (array[i][j-1].equals(a) || array[i][j-1].equals(b)  || array[i][j-1].equals(c))
				&& (array[i][j+1].equals(a) || array[i][j+1].equals(b)  || array[i][j+1].equals(c))
				
				&& (array[i+1][j].equals(a) || array[i+1][j].equals(b)  || array[i+1][j].equals(c))
				)
			{
				g2.drawImage(image[15], x, y, 24, 24, null);
			}
			
			/** 12 */
			/*   | X |   */
			/* X | ~ |   */
			/*   | X |   */
			else if((array[i-1][j].equals(a) || array[i-1][j].equals(b) || array[i-1][j].equals(c))
					
					&& (array[i][j-1].equals(a) || array[i][j-1].equals(b)  || array[i][j-1].equals(c))
					
					&& (array[i+1][j].equals(a) || array[i+1][j].equals(b)  || array[i+1][j].equals(c))
					)
			{
				g2.drawImage(image[12], x, y, 24, 24, null);
			}
			
			
			/** 13 */
			/*   | X |   */
			/* X | ~ | X */
			/*   |   |   */
			else if((array[i-1][j].equals(a) || array[i-1][j].equals(b) || array[i-1][j].equals(c))
					
					&& (array[i][j-1].equals(a) || array[i][j-1].equals(b)  || array[i][j-1].equals(c))
					&& (array[i][j+1].equals(a) || array[i][j+1].equals(b)  || array[i][j+1].equals(c))
					)
			{
				g2.drawImage(image[13], x, y, 24, 24, null);
			}
			
			
			/** 14 */
			/*   |   |   */
			/* X | ~ | X */
			/*   | X |   */
			else if((array[i][j-1].equals(a) || array[i][j-1].equals(b)  || array[i][j-1].equals(c))
					&& (array[i][j+1].equals(a) || array[i][j+1].equals(b)  || array[i][j+1].equals(c))
				
					&& (array[i+1][j].equals(a) || array[i+1][j].equals(b)  || array[i+1][j].equals(c))
					)
			{
				g2.drawImage(image[14], x, y, 24, 24, null);
			}
			
			
			/** 16 */
			/*   | X |   */
			/*   | ~ | X */
			/*   | X |   */
			else if((array[i-1][j].equals(a) || array[i-1][j].equals(b) || array[i-1][j].equals(c))
					
					&& (array[i][j+1].equals(a) || array[i][j+1].equals(b)  || array[i][j+1].equals(c))
					
					&& (array[i+1][j].equals(a) || array[i+1][j].equals(b)  || array[i+1][j].equals(c))
					)
			{
				g2.drawImage(image[12], x+24, y, -24, 24, null);
			}
			
			
			/** 17 */
			/*   | X |   */
			/*   | ~ |   */
			/*   | X |   */
			else if((array[i-1][j].equals(a) || array[i-1][j].equals(b) || array[i-1][j].equals(c))
				
					&& (array[i+1][j].equals(a) || array[i+1][j].equals(b)  || array[i+1][j].equals(c))
					)
			{
				g2.drawImage(image[1], x, y, 24, 24, null);

	            g2.drawImage(image[5], x, y, 24, 24, null);
			}
			
			/** 18 */
			/*   |   |   */
			/* X | ~ | X */
			/*   |   |   */
			else if((array[i][j-1].equals(a) || array[i][j-1].equals(b) || array[i][j-1].equals(c))
				
					&& (array[i][j+1].equals(a) || array[i][j+1].equals(b)  || array[i][j+1].equals(c))
					)
			{
				g2.drawImage(image[3], x, y, 24, 24, null);

	            g2.drawImage(image[7], x, y, 24, 24, null);
			}
			
			
			/** 0 */
			/*   | X |   */
			/* X | ~ |   */
			/*   |   |   */
			else if((array[i-1][j].equals(a) || array[i-1][j].equals(b) || array[i-1][j].equals(c))

					&& (array[i][j-1].equals(a) || array[i][j-1].equals(b)  || array[i][j-1].equals(c))
					)
			{
				g2.drawImage(image[0], x, y, 24, 24, null);
				
				if((array[i+1][j+1].equals(a) || array[i+1][j+1].equals(b) || array[i+1][j+1].equals(c)))
				{
					g2.drawImage(image[8], x, y, 24, 24, null);
				}
			}
			
			
			/** 2 */
			/*   | X |   */
			/*   | ~ | X */
			/*   |   |   */
			else if((array[i-1][j].equals(a) || array[i-1][j].equals(b) || array[i-1][j].equals(c))

					&& (array[i][j+1].equals(a) || array[i][j+1].equals(b)  || array[i][j+1].equals(c))
					)
			{
				g2.drawImage(image[2], x, y, 24, 24, null);
				
				if((array[i+1][j-1].equals(a) || array[i+1][j-1].equals(b) || array[i+1][j-1].equals(c)))
				{
					g2.drawImage(image[9], x, y, 24, 24, null);
				}
			}
			
			
			/** 19 */
			/* X | ~ | X */
			/* ~ | ~ | ~ */
			/* X | ~ | X */
			else if((array[i-1][j-1].equals(a) || array[i-1][j-1].equals(b) || array[i-1][j-1].equals(c))
					&&(array[i-1][j].equals(ter))
					&&(array[i-1][j+1].equals(a) || array[i-1][j+1].equals(b) || array[i-1][j+1].equals(c))
					
					&&(array[i][j-1].equals(ter))
					&&(array[i][j+1].equals(ter))
					
					&& (array[i+1][j-1].equals(a) || array[i+1][j-1].equals(b)  || array[i+1][j-1].equals(c))
					&&(array[i+1][j].equals(ter))
					&& (array[i+1][j+1].equals(a) || array[i+1][j+1].equals(b)  || array[i+1][j+1].equals(c))
					)
			{
				g2.drawImage(image[8], x, y, 24, 24, null);
	            g2.drawImage(image[9], x, y, 24, 24, null);
	            g2.drawImage(image[10], x, y, 24, 24, null);
	            g2.drawImage(image[11], x, y, 24, 24, null);
			}
			
			
			/** 1 */
			/*   | X |   */
			/* ~ | ~ | ~ */
			/*   |   |   */
			else if((array[i-1][j].equals(a) || array[i-1][j].equals(b) || array[i-1][j].equals(c))
				
					&&(array[i][j-1].equals(ter))
					&&(array[i][j+1].equals(ter))
				
					//&&(array[i+1][j].equals(ter))
					)
			{
				g2.drawImage(image[1], x, y, 24, 24, null);
				
				if((array[i+1][j+1].equals(a) || array[i+1][j+1].equals(b) || array[i+1][j+1].equals(c)))
				{
					g2.drawImage(image[8], x, y, 24, 24, null);
				}
				if((array[i+1][j-1].equals(a) || array[i+1][j-1].equals(b) || array[i+1][j-1].equals(c)))
				{
					g2.drawImage(image[9], x, y, 24, 24, null);
				}
			}
			
			
			/** 3 */
			/*   | ~ |   */
			/*   | ~ | X */
			/*   | ~ |   */
			else if((array[i-1][j].equals(ter))
					
					//&&(array[i][j-1].equals(ter))
					&&(array[i][j+1].equals(a) || array[i][j+1].equals(b) || array[i][j+1].equals(c))
					
					&&(array[i+1][j].equals(ter))
					)
			{
				g2.drawImage(image[3], x, y, 24, 24, null);
				
				if((array[i-1][j-1].equals(a) || array[i-1][j-1].equals(b) || array[i-1][j-1].equals(c)))
				{
					g2.drawImage(image[11], x, y, 24, 24, null);
				}
				if((array[i+1][j-1].equals(a) || array[i+1][j-1].equals(b) || array[i+1][j-1].equals(c)))
				{
					g2.drawImage(image[9], x, y, 24, 24, null);
				}
			}
			
			/** 4 */
			/*   |   |   */
			/* X | ~ |   */
			/*   | X |   */
			else if((array[i][j-1].equals(a) || array[i][j-1].equals(b) || array[i][j-1].equals(c))

					&& (array[i+1][j].equals(a) || array[i+1][j].equals(b) || array[i+1][j].equals(c))			// -O-
					)
			{
				g2.drawImage(image[4], x, y, 24, 24, null);
				
				if((array[i-1][j+1].equals(a) || array[i-1][j+1].equals(b) || array[i-1][j+1].equals(c)))
				{
					g2.drawImage(image[10], x, y, 24, 24, null);
				}
			}
			
			
			/** 5 */
			/*   |   |   */
			/* ~ | ~ | ~ */
			/*   | X |   */
			else if(//(array[i-1][j].equals(ter))&&
					
					(array[i][j-1].equals(ter))
					&&(array[i][j+1].equals(ter))
				
					&&(array[i+1][j].equals(a) || array[i+1][j].equals(b) || array[i+1][j].equals(c))
					)
			{
				g2.drawImage(image[5], x, y, 24, 24, null);
				
				if((array[i-1][j-1].equals(a) || array[i-1][j-1].equals(b) || array[i-1][j-1].equals(c)))
				{
					g2.drawImage(image[11], x, y, 24, 24, null);
				}
				if((array[i-1][j+1].equals(a) || array[i-1][j+1].equals(b) || array[i-1][j+1].equals(c)))
				{
					g2.drawImage(image[10], x, y, 24, 24, null);
				}
			}
			

			/** 6 */
			/*   |   |   */
			/*   | ~ | X */
			/*   | X |   */
			else if((array[i][j+1].equals(a) || array[i][j+1].equals(b) || array[i][j+1].equals(c))

					&& (array[i+1][j].equals(a) || array[i+1][j].equals(b) || array[i+1][j].equals(c))			// -O-
					)
			{
				g2.drawImage(image[6], x, y, 24, 24, null);
				
				if((array[i-1][j-1].equals(a) || array[i-1][j-1].equals(b) || array[i-1][j-1].equals(c)))
				{
					g2.drawImage(image[11], x, y, 24, 24, null);
				}
			}
			
			/** 7 */
			/*   | ~ |   */
			/* X | ~ |   */
			/*   | ~ |   */
			else if((array[i-1][j].equals(ter))
					
					&&(array[i][j-1].equals(a) || array[i][j-1].equals(b) || array[i][j-1].equals(c))
					//&&(array[i][j+1].equals(ter))
					
					&&(array[i+1][j].equals(ter))
					)
			{
				g2.drawImage(image[7], x, y, 24, 24, null);
				
				if((array[i-1][j+1].equals(a) || array[i-1][j+1].equals(b) || array[i-1][j+1].equals(c)))
				{
					g2.drawImage(image[10], x, y, 24, 24, null);
				}
				if((array[i+1][j+1].equals(a) || array[i+1][j+1].equals(b) || array[i+1][j+1].equals(c)))
				{
					g2.drawImage(image[8], x, y, 24, 24, null);
				}
			}
			
			/** 8 */
			/*   |   |   */
			/*   | ~ | ~ */
			/*   | ~ | X */
			else if((array[i][j+1].equals(ter))
					
					&&(array[i+1][j].equals(ter))
					&&(array[i+1][j+1].equals(a) || array[i+1][j+1].equals(b) || array[i+1][j+1].equals(c))
					)
			{
				g2.drawImage(image[8], x, y, 24, 24, null);
				if((array[i-1][j-1].equals(a) || array[i-1][j-1].equals(b) || array[i-1][j-1].equals(c)))
				{
					g2.drawImage(image[11], x, y, 24, 24, null);
				}
				if((array[i-1][j+1].equals(a) || array[i-1][j+1].equals(b) || array[i-1][j+1].equals(c)))
				{
					g2.drawImage(image[10], x, y, 24, 24, null);
				}
				if((array[i+1][j-1].equals(a) || array[i+1][j-1].equals(b) || array[i+1][j-1].equals(c)))
				{
					g2.drawImage(image[9], x, y, 24, 24, null);
				}
			}
			
			
			/** 9 */
			/*   |   |   */
			/* ~ | ~ |   */
			/* X | ~ |   */
			else if((array[i][j-1].equals(ter))
					
					&&(array[i+1][j-1].equals(a) || array[i+1][j-1].equals(b) || array[i+1][j-1].equals(c))
					&&(array[i+1][j].equals(ter))
					)
			{
				g2.drawImage(image[9], x, y, 24, 24, null);
				
				if((array[i-1][j-1].equals(a) || array[i-1][j-1].equals(b) || array[i-1][j-1].equals(c)))
				{
					g2.drawImage(image[11], x, y, 24, 24, null);
				}
				if((array[i-1][j+1].equals(a) || array[i-1][j+1].equals(b) || array[i-1][j+1].equals(c)))
				{
					g2.drawImage(image[10], x, y, 24, 24, null);
				}
				if((array[i+1][j+1].equals(a) || array[i+1][j+1].equals(b) || array[i+1][j+1].equals(c)))
				{
					g2.drawImage(image[8], x, y, 24, 24, null);
				}
			}
			
			
			/** 10 */
			/*   | ~ | X */
			/*   | ~ | ~ */
			/*   |   |   */
			else if((array[i-1][j].equals(ter))
					&&(array[i-1][j+1].equals(a) || array[i-1][j+1].equals(b) || array[i-1][j+1].equals(c))
					
					&&(array[i][j+1].equals(ter))
					)
			{
				g2.drawImage(image[10], x, y, 24, 24, null);
				
				if((array[i-1][j-1].equals(a) || array[i-1][j-1].equals(b) || array[i-1][j-1].equals(c)))
				{
					g2.drawImage(image[11], x, y, 24, 24, null);
				}
				if((array[i+1][j-1].equals(a) || array[i+1][j-1].equals(b) || array[i+1][j-1].equals(c)))
				{
					g2.drawImage(image[9], x, y, 24, 24, null);
				}
				if((array[i+1][j+1].equals(a) || array[i+1][j+1].equals(b) || array[i+1][j+1].equals(c)))
				{
					g2.drawImage(image[8], x, y, 24, 24, null);
				}
			}
			
			
			/** 11 */
			/* X | ~ |   */
			/* ~ | ~ |   */
			/*   |   |   */
			else if((array[i-1][j-1].equals(a) || array[i-1][j-1].equals(b) || array[i-1][j-1].equals(c))
					&&(array[i-1][j].equals(ter))
					
					&&(array[i][j-1].equals(ter))
					)
			{
				g2.drawImage(image[11], x, y, 24, 24, null);
				
				if((array[i-1][j+1].equals(a) || array[i-1][j+1].equals(b) || array[i-1][j+1].equals(c)))
				{
					g2.drawImage(image[10], x, y, 24, 24, null);
				}
				if((array[i+1][j-1].equals(a) || array[i+1][j-1].equals(b) || array[i+1][j-1].equals(c)))
				{
					g2.drawImage(image[9], x, y, 24, 24, null);
				}
				if((array[i+1][j+1].equals(a) || array[i+1][j+1].equals(b) || array[i+1][j+1].equals(c)))
				{
					g2.drawImage(image[8], x, y, 24, 24, null);
				}
			}
			
		}
	}
	
	/**
	 * buildTrackerSwitch : Turn on the mosue tracker with the block checker with draw a gree hover on the build-able map tile, otherwise red.
	 */
	public void turnBuildTrackerSwitchOn(boolean onNoff)
	{
		displayRBBlocks = onNoff;
		System.out.println("Map block tracker turned on? " + displayRBBlocks);
	}
	
	/**
	 * setBuildingBlockCheckerSize: Set the size before build.
	 * @param width
	 * @param height
	 */
	public void setBuildingBlockCheckerSize(int width, int height)
	{
		this.blockWidth = width;
		this.blockHeight = height;
	}
	/*
	public void addBuilding(Building aBuilding)
	{
		buildings.add(aBuilding);
	}
	*/
	public void setSHOW_MAP_TEST(boolean onNoff)
	{
		SHOW_MAP_TEST = onNoff;
	}
}
