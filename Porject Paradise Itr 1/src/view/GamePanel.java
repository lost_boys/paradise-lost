/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package view;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import main.MainViewController;
import model.*;

@SuppressWarnings("serial")
public class GamePanel extends JPanel
{
	private static final int MOVE_SPEED = 1;
	private static final int TIMER_DELAY = 0;
	private static final int CORNER_RANGE = 100;
	private static final int BORDER_RANGE = 4;
	private static final int SCROLLBAR_SIZE = 0;
	private boolean HIDE_HUD_WHEN_SCROLL = false;

	private Point defaultViewPortPosition;
	private GameState theGame;
	private Map aMap;
	private MapPanel MapPane;
	private JScrollPane scrollPane;
	private int width, height;
	public Timer moveViewPortRightTimer, moveViewPortLeftTimer,
			moveViewPortUpTimer, moveViewPortDownTimer,
			moveViewPortUpRightTimer, moveViewPortUpLeftTimer,
			moveViewPortDownRightTimer, moveViewPortDownLeftTimer;

	private MainViewController aFrame;
	private AgentCollection agents;
	private Building buildingToBuild;
	private boolean buildItemThisTime;

	public GamePanel(GameState theGame, MainViewController aFrame)
	{
		this.aFrame = aFrame;

		this.width = aFrame.getContentWidth();
		this.height = aFrame.getContentHeight();
		this.theGame = theGame;
		this.aMap = theGame.getMap();
		this.agents = theGame.getAgents();
		this.buildItemThisTime = false;
		this.setOpaque(false);
		this.setLayout(null);

		// Change default viewport position from here!
		defaultViewPortPosition = new Point(aMap.getPlaneLocation().x * 24
				- width / 2, aMap.getPlaneLocation().y * 24 - height / 2);

		// Setup scrollPane with MapPane
		MapPane = new MapPanel(theGame);
		System.out.println(aMap + "\n"); // test print the map
		MapPane.addMouseListener(new mouseTrackListener());

		scrollPane = new JScrollPane(MapPane);
		scrollPane.setBorder(BorderFactory.createEmptyBorder());
		scrollPane.getVerticalScrollBar().setPreferredSize(
				new Dimension(SCROLLBAR_SIZE, 0));
		scrollPane.getHorizontalScrollBar().setPreferredSize(
				new Dimension(0, SCROLLBAR_SIZE));
		scrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scrollPane.setBounds(0, 0, width, height);
		scrollPane.getViewport().setViewPosition(defaultViewPortPosition);

		scrollPane.addMouseWheelListener(new scollingListener());

		// Move view listeners keep the viewport moveable after mouse movement
		// actions.
		moveViewPortRightTimer = new Timer(TIMER_DELAY, new moveViewPortRight());
		moveViewPortLeftTimer = new Timer(TIMER_DELAY, new moveViewPortLeft());
		moveViewPortUpTimer = new Timer(TIMER_DELAY, new moveViewPortUp());
		moveViewPortDownTimer = new Timer(TIMER_DELAY, new moveViewPortDown());
		moveViewPortUpRightTimer = new Timer(TIMER_DELAY,
				new moveViewPortUpRight());
		moveViewPortUpLeftTimer = new Timer(TIMER_DELAY,
				new moveViewPortUpLeft());
		moveViewPortDownRightTimer = new Timer(TIMER_DELAY,
				new moveViewPortDownRight());
		moveViewPortDownLeftTimer = new Timer(TIMER_DELAY,
				new moveViewPortDownLeft());

		this.add(scrollPane);
	}

	public JScrollPane getScrollPane()
	{
		return scrollPane;
	}

	public MapPanel getMapPane()
	{
		return MapPane;
	}

	public void refreshMapPanel()
	{
		MapPane = new MapPanel(theGame);
	}

	/**
	 * mouseTrackListener: Gathering mouse info from "MAP" viewport.
	 */
	private class mouseTrackListener implements MouseListener
	{
		@Override
		public void mouseClicked(MouseEvent e)
		{
			// Test print mouse on map position.
			Point mousePosition = MouseInfo.getPointerInfo().getLocation();
			SwingUtilities.convertPointFromScreen(mousePosition,
					e.getComponent());
			System.out.println("\n+----------------------------------\n"
					+ "| * Mouse Clicked!"
					+ "\n|----------------------------------\n"
					+ "| On map: X: " + mousePosition.x + ", Y: "
					+ mousePosition.y);
			String str = aMap.getMapTeilInfo(((int) mousePosition.x) / 24,
					((int) mousePosition.y) / 24);

			// Test print mouse on screen position and at window position.
			Point overViewPosition = MouseInfo.getPointerInfo().getLocation();
			Point windowPostition = getLocationOnScreen();
			System.out.println("| On screen: X: " + overViewPosition.x
					+ ", Y: " + overViewPosition.y);
			System.out.println("| On window:at: X: "
					+ (overViewPosition.x - windowPostition.x) + ", Y: "
					+ (overViewPosition.y - windowPostition.y) + "\n|");

			// Test print maptile info.
			System.out.println("| MapTile info:  [" + ((int) mousePosition.x)
					/ 24 + "] [" + ((int) mousePosition.y) / 24 + "]\n" + str
					+ "+----------------------------------\n\n");

			/** Build function. */
			// Check builtbale
			boolean builtable = true;
			Point coveredIndex = new Point((int) mousePosition.x / 24,
					(int) mousePosition.y / 24);
			if(aFrame.getHUD().isBuildPressed())
			{
				if(!buildItemThisTime)
				{
					for(int X = 0; X < buildingToBuild.getBlockHeight(); X++)
					{
						for(int Y = 0; Y < buildingToBuild.getBlockWidth(); Y++)
						{
							if(aMap.getTile(coveredIndex.y + Y, coveredIndex.x + X)
								.getResource() != null
								|| aMap.getTile(coveredIndex.y + Y,
										coveredIndex.x + X).getBuilding() != null
								|| theGame.getBuildingsOccupied()[coveredIndex.x
										+ X][coveredIndex.y + Y])
							{
								builtable = false;
							}

						}
					}

					if(SwingUtilities.isRightMouseButton(e))
					{
						aFrame.getHUD().turnBuildOff();
					}
					else if(builtable)
					{
						agents.command(new Build(buildingToBuild,
							((int) mousePosition.y) / 24,
							((int) mousePosition.x) / 24));
						buildingToBuild.setBuildingLocation(
							((int) mousePosition.y) / 24,
							((int) mousePosition.x) / 24);
						theGame.addBuilding(buildingToBuild);

						for(int i = 0; i < buildingToBuild.getBlockWidth(); i++)
						{
							for(int j = 0; j < buildingToBuild.getBlockHeight(); j++)
							{
								theGame.getBuildingsOccupied()[((int) mousePosition.x)
									/ 24 + j][((int) mousePosition.y) / 24 + i] = true;
							}
						}
					}
				}
				else	// build item this time!
				{
					System.out.println("You placed a container!!!!!");
					if(aMap.getTile(coveredIndex.y, coveredIndex.x)
							.getResource() != null
							|| aMap.getTile(coveredIndex.y, coveredIndex.x).getBuilding() != null
							|| theGame.getBuildingsOccupied()[coveredIndex.x][coveredIndex.y])
					{
							builtable = false;
					}
					if(SwingUtilities.isRightMouseButton(e))
					{
						aFrame.getHUD().turnBuildOff();
					}
					else if(builtable)
					{
						System.out.println("The container is place!");
						theGame.getAgents().command(new PlaceItemCommand(((int) mousePosition.y) / 24, ((int) mousePosition.x) / 24, Item.Container));
						
						theGame.getContainerList().add(new Point(((int) mousePosition.y) / 24, ((int) mousePosition.x) / 24));
						
						aFrame.getConsole().addCommandString(
								"Placing a container command sent out!");
						theGame.pushMessage("A container is place!");
						theGame.getBuildingsOccupied()[((int) mousePosition.x) / 24][((int) mousePosition.y) / 24] = true;
							
					}
				}

			}
			else
			{
				// Display agent info if user clicked on an agent.
				if(aMap.getTile(((int) mousePosition.y) / 24,
						((int) mousePosition.x) / 24).getAgent() != null)
				{
					Agent theAgent = aMap.getTile(((int) mousePosition.y) / 24,
							((int) mousePosition.x) / 24).getAgent();
					aFrame.getHUD().setAgentInfoToHUD(theAgent);
					MapPane.setPointer(true);
					MapPane.setTrackingAgent(theAgent);
				}
				else
				{
					MapPane.setPointer(false);
					aFrame.getHUD().closeHUDWindow();
				}
			}

			aFrame.getHUD().turnBuildOff();

		}

		@Override
		public void mousePressed(MouseEvent e)
		{
		}

		@Override
		public void mouseReleased(MouseEvent e)
		{
		}

		@Override
		public void mouseEntered(MouseEvent e)
		{
			if(HIDE_HUD_WHEN_SCROLL)
			{
				aFrame.displayHUD();
			}
			moveViewPortRightTimer.stop();
			moveViewPortLeftTimer.stop();
			moveViewPortUpTimer.stop();
			moveViewPortDownTimer.stop();
			moveViewPortUpRightTimer.stop();
			moveViewPortUpLeftTimer.stop();
			moveViewPortDownRightTimer.stop();
			moveViewPortDownLeftTimer.stop();
		}

		@Override
		public void mouseExited(MouseEvent e)
		{

			Point overViewPosition = MouseInfo.getPointerInfo().getLocation();
			Point windowPostition = getLocationOnScreen();
			System.out.println("\n+----------------------------------\n"
					+ "| ^ Mouse Exited!"
					+ "\n|----------------------------------\n| On Screen: X: "
					+ overViewPosition.x + ", Y: " + overViewPosition.y);
			System.out.println("| At window:at: X: "
					+ (overViewPosition.x - windowPostition.x) + ", Y: "
					+ (overViewPosition.y - windowPostition.y)
					+ "\n+----------------------------------\n\n");
			int X = overViewPosition.x - windowPostition.x;
			int Y = overViewPosition.y - windowPostition.y;

			// Move Up Right
			if((X > width - CORNER_RANGE - SCROLLBAR_SIZE && Y < BORDER_RANGE)
					|| (X > width - BORDER_RANGE - SCROLLBAR_SIZE && Y < CORNER_RANGE))
			{
				if(HIDE_HUD_WHEN_SCROLL)
				{
					aFrame.hideHUD();
				}
				moveViewPortUpRightTimer.start();
			}
			// Move Up Left
			else if((X < CORNER_RANGE && Y < BORDER_RANGE)
					|| (X < BORDER_RANGE && Y < CORNER_RANGE))
			{
				if(HIDE_HUD_WHEN_SCROLL)
				{
					aFrame.hideHUD();
				}
				moveViewPortUpLeftTimer.start();
			}
			// Move Down Right
			else if((X > width - CORNER_RANGE - SCROLLBAR_SIZE && Y > height
					- BORDER_RANGE - SCROLLBAR_SIZE)
					|| (X > width - BORDER_RANGE - SCROLLBAR_SIZE && Y > height
							- CORNER_RANGE - SCROLLBAR_SIZE))
			{
				if(HIDE_HUD_WHEN_SCROLL)
				{
					aFrame.hideHUD();
				}
				moveViewPortDownRightTimer.start();
			}
			// Move Down Left
			else if((X < CORNER_RANGE && Y > height - BORDER_RANGE
					- SCROLLBAR_SIZE)
					|| (X < BORDER_RANGE && Y > height - CORNER_RANGE
							- SCROLLBAR_SIZE))
			{
				if(HIDE_HUD_WHEN_SCROLL)
				{
					aFrame.hideHUD();
				}
				moveViewPortDownLeftTimer.start();
			}
			// Move Right
			else if(X > width - BORDER_RANGE)
			{
				if(HIDE_HUD_WHEN_SCROLL)
				{
					aFrame.hideHUD();
				}
				moveViewPortRightTimer.start();
			}
			// Move Down
			else if(Y > height - BORDER_RANGE)
			{
				if(HIDE_HUD_WHEN_SCROLL)
				{
					aFrame.hideHUD();
				}
				moveViewPortDownTimer.start();
			}
			// Move Left
			else if(X < BORDER_RANGE)
			{
				if(HIDE_HUD_WHEN_SCROLL)
				{
					aFrame.hideHUD();
				}
				moveViewPortLeftTimer.start();
			}
			// Move Up
			else if(Y < BORDER_RANGE)
			{
				if(HIDE_HUD_WHEN_SCROLL)
				{
					aFrame.hideHUD();
				}
				moveViewPortUpTimer.start();
			}
		}

	}

	/**
	 * moveViewPort Listeners: Move view port towards different direction by
	 * detecting the mouse exit movements.
	 */
	private class moveViewPortRight implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Point viewPortPosition = scrollPane.getViewport().getViewPosition();
			if(viewPortPosition.x < aMap.getWidth() * 24)
			{
				viewPortPosition.x += MOVE_SPEED;
				scrollPane.getViewport().setViewPosition(viewPortPosition);
			}
			aFrame.refresh();
		}
	}

	private class moveViewPortLeft implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Point viewPortPosition = scrollPane.getViewport().getViewPosition();
			if(viewPortPosition.x > 0)
			{
				viewPortPosition.x -= MOVE_SPEED;
				scrollPane.getViewport().setViewPosition(viewPortPosition);
			}
			aFrame.refresh();
		}
	}

	private class moveViewPortUp implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Point viewPortPosition = scrollPane.getViewport().getViewPosition();
			if(viewPortPosition.y > 0)
			{
				viewPortPosition.y -= MOVE_SPEED;
				scrollPane.getViewport().setViewPosition(viewPortPosition);
			}
			aFrame.refresh();
		}
	}

	private class moveViewPortDown implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Point viewPortPosition = scrollPane.getViewport().getViewPosition();
			// if(viewPortPosition.y < height)
			{
				viewPortPosition.y += MOVE_SPEED;
				scrollPane.getViewport().setViewPosition(viewPortPosition);
				// System.out.println(viewPortPosition + "\n" +
				// scrollPane.getViewport().getHeight());
			}
			aFrame.refresh();
		}
	}

	private class moveViewPortUpRight implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Point viewPortPosition = scrollPane.getViewport().getViewPosition();
			if(viewPortPosition.x < aMap.getWidth() * 24)
			{
				viewPortPosition.x += MOVE_SPEED;
				scrollPane.getViewport().setViewPosition(viewPortPosition);
			}
			if(viewPortPosition.y > 0)
			{
				viewPortPosition.y -= MOVE_SPEED;
				scrollPane.getViewport().setViewPosition(viewPortPosition);
			}
			aFrame.refresh();
		}
	}

	private class moveViewPortUpLeft implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Point viewPortPosition = scrollPane.getViewport().getViewPosition();
			if(viewPortPosition.x > 0)
			{
				viewPortPosition.x -= MOVE_SPEED;
				scrollPane.getViewport().setViewPosition(viewPortPosition);
			}
			if(viewPortPosition.y > 0)
			{
				viewPortPosition.y -= MOVE_SPEED;
				scrollPane.getViewport().setViewPosition(viewPortPosition);
			}
			aFrame.refresh();
		}
	}

	private class moveViewPortDownRight implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Point viewPortPosition = scrollPane.getViewport().getViewPosition();
			if(viewPortPosition.x < aMap.getWidth() * 24)
			{
				viewPortPosition.x += MOVE_SPEED;
				scrollPane.getViewport().setViewPosition(viewPortPosition);
			}
			// if(viewPortPosition.y < height)
			{
				viewPortPosition.y += MOVE_SPEED;
				scrollPane.getViewport().setViewPosition(viewPortPosition);
			}
			aFrame.refresh();
		}
	}

	private class moveViewPortDownLeft implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e)
		{
			Point viewPortPosition = scrollPane.getViewport().getViewPosition();
			if(viewPortPosition.x > 0)
			{
				viewPortPosition.x -= MOVE_SPEED;
				scrollPane.getViewport().setViewPosition(viewPortPosition);
			}
			// if(viewPortPosition.y < height)
			{
				viewPortPosition.y += MOVE_SPEED;
				scrollPane.getViewport().setViewPosition(viewPortPosition);
			}
			aFrame.refresh();
		}
	}

	/**
	 * scollingListener: Refesh when scolling, fix the HUDPanel glitch.
	 */
	public class scollingListener implements MouseWheelListener
	{

		@Override
		public void mouseWheelMoved(MouseWheelEvent e)
		{
			aFrame.refresh();
		}

	}

	/**
	 * forze: Froze the background when a alert panel shows up.
	 */
	public void froze()
	{
		MapPane.removeMouseListener(MapPane.getMouseListeners()[0]);
		// scrollPane.removeMouseWheelListener(scrollPane.getMouseWheelListeners()[0]);

	}

	/**
	 * melt: Re-active the listeners after alert panel is closed.
	 */
	public void melt()
	{
		MapPane.addMouseListener(new mouseTrackListener());
		// scrollPane.addMouseWheelListener(new scollingListener());
	}

	public void setBuildingToBuild(Building building)
	{
		buildingToBuild = building;
	}

	public void setHIDE_HUD_WHEN_SCROLL(boolean onNoff)
	{
		HIDE_HUD_WHEN_SCROLL = onNoff;
	}
	
	public void setBuildItem(boolean onNoff)
	{
		buildItemThisTime = onNoff;
	}
}
