/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package view;

import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;
import javax.swing.*;

@SuppressWarnings("serial")
public class AlertPanel extends JPanel
{
	private Font camomileFont, elektoraFont;
	private BufferedImage innerImage, warningImage, winImage, loseImage;

	public AlertPanel(String img, String question, String cancel, String confirm,
			ActionListener cancelListener, ActionListener confirmListener,
			int windowHeight)
	{
		try
		{
			innerImage = ImageIO.read(new File("image/ui/inner.png"));
			warningImage = ImageIO.read(new File("image/ui/warning.png"));
			winImage = ImageIO.read(new File("image/ui/win.png"));
			loseImage = ImageIO.read(new File("image/ui/lose.png"));

			camomileFont = Font.createFont(Font.PLAIN, new File(
					"fonts/camomile.ttf"));
			elektoraFont = Font.createFont(Font.PLAIN, new File(
					"fonts/elektora.ttf"));
			camomileFont = camomileFont.deriveFont(Font.PLAIN, 20);
			elektoraFont = elektoraFont.deriveFont(Font.PLAIN, 20);
			GraphicsEnvironment ge = GraphicsEnvironment
					.getLocalGraphicsEnvironment();
			ge.registerFont(camomileFont);
			ge.registerFont(elektoraFont);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		this.setOpaque(false);
		this.setLayout(null);

		BufferedImage image = warningImage;
		if(img.equals("win"))
		{
			image = winImage;
		}
		else if(img.equals("lose"))
		{
			image = loseImage;
		}
		JLabel warning = new JLabel(new ImageIcon(image));
		this.setBounds(0, (windowHeight - 460) / 2, 1024, 460);
		JLabel quit = new JLabel(question);
		JButton confirmQuit = new JButton(confirm);
		JButton cancelQuit = new JButton(cancel);
		quit.setHorizontalAlignment(SwingConstants.CENTER);
		quit.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 14));
		confirmQuit.setHorizontalAlignment(SwingConstants.CENTER);
		confirmQuit.setHorizontalAlignment(SwingConstants.CENTER);
		confirmQuit.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 24));
		cancelQuit.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 24));
		cancelQuit.addActionListener(cancelListener);
		confirmQuit.addActionListener(confirmListener);
		this.add(warning);
		this.add(quit);
		this.add(confirmQuit);
		this.add(cancelQuit);

		warning.setBounds(0, 100, 1024, 160);
		quit.setBounds(0, 260, 1024, 20);

		cancelQuit.setBounds(300, 305, 180, 40);
		confirmQuit.setBounds(1024 - 300 - 180, 305, 180, 40);

		JLabel alertBg = new JLabel(new ImageIcon(innerImage));
		this.add(alertBg);
		alertBg.setBounds(0, 0, 1024, 460);
	}
	
	public AlertPanel(String img, String question, String confirm,
			ActionListener confirmListener,
			int windowHeight)
	{
		try
		{
			innerImage = ImageIO.read(new File("image/ui/inner.png"));
			warningImage = ImageIO.read(new File("image/ui/warning.png"));
			winImage = ImageIO.read(new File("image/ui/win.png"));
			loseImage = ImageIO.read(new File("image/ui/lose.png"));

			camomileFont = Font.createFont(Font.PLAIN, new File(
					"fonts/camomile.ttf"));
			elektoraFont = Font.createFont(Font.PLAIN, new File(
					"fonts/elektora.ttf"));
			camomileFont = camomileFont.deriveFont(Font.PLAIN, 20);
			elektoraFont = elektoraFont.deriveFont(Font.PLAIN, 20);
			GraphicsEnvironment ge = GraphicsEnvironment
					.getLocalGraphicsEnvironment();
			ge.registerFont(camomileFont);
			ge.registerFont(elektoraFont);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		this.setOpaque(false);
		this.setLayout(null);

		BufferedImage image = warningImage;
		if(img.equals("win"))
		{
			image = winImage;
		}
		else if(img.equals("lose"))
		{
			image = loseImage;
		}
		JLabel warning = new JLabel(new ImageIcon(image));
		this.setBounds(0, (windowHeight - 460) / 2, 1024, 460);
		JLabel quit = new JLabel(question);
		JButton confirmQuit = new JButton(confirm);
		quit.setHorizontalAlignment(SwingConstants.CENTER);
		quit.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 14));
		confirmQuit.setHorizontalAlignment(SwingConstants.CENTER);
		confirmQuit.setHorizontalAlignment(SwingConstants.CENTER);
		confirmQuit.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 24));
		confirmQuit.addActionListener(confirmListener);
		this.add(warning);
		this.add(quit);
		this.add(confirmQuit);

		warning.setBounds(0, 100, 1024, 160);
		quit.setBounds(0, 260, 1024, 20);

		confirmQuit.setBounds(422, 305, 180, 40);

		JLabel alertBg = new JLabel(new ImageIcon(innerImage));
		this.add(alertBg);
		alertBg.setBounds(0, 0, 1024, 460);
	}
}
