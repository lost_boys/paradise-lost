/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.plaf.basic.BasicComboBoxRenderer;

import main.MainViewController;
import model.*;

@SuppressWarnings(
{ "rawtypes", "serial", "unchecked" })
public class HUDPanel extends JPanel
{
	private final int DRAWER_WIDTH = 110;
	private final int DRAWER_HEIGHT = 4 * (5 + 30) + 5;
	private MainViewController aFrame;
	private HUDWindowPanel HUDWindow;
	private GameState theGame;
	private JButton quit, save, mute, more;
	private Button buildTent, buildKitchen, buildWorkshop, buildDock;
	private JPanel globalResourcePanel;
	private Font camomileFont, elektoraFont;
	private BufferedImage resourceBarImage, buildingBarImage, buttonImage,
			populationIcon, foodIcon, waterIcon, woodIcon, rockIcon, ironIcon;
	private int width, height, drawerPositionY;
	private AlertPanel quiteAlertPanel, overwirteAlertPanel, winAlertPanel, loseAlertPanel;
	private GamePanel gamePanel;
	private Timer drawerTimer;
	private JComboBox commandList;

	private JComboBox agentList;
	private ArrayList<Agent> al;

	private int food, water, wood, rock, iron, population;

	private JPanel drawerPanel;
	private boolean containerPressed;

	public HUDPanel(GameState theGame, MainViewController aFrame)
	{
		this.aFrame = aFrame;
		this.theGame = theGame;
		this.width = aFrame.getContentWidth();
		this.height = aFrame.getContentHeight();
		this.setOpaque(false);
		this.setLayout(null);
		containerPressed = false;
		this.gamePanel = aFrame.getGamePanel();

		// totalPopulation = theGame.getAgents().totalAgents();

		try
		{
			resourceBarImage = ImageIO.read(new File(
					"image/ui/resource_bar.png"));
			buildingBarImage = ImageIO.read(new File(
					"image/ui/buildings_bar.png"));
			BufferedImage resourceIconSpriteSheet = ImageIO.read(new File(
					"image/gamepackage/resource_icons.png"));
			ironIcon = resourceIconSpriteSheet.getSubimage(0 * 16, 0, 16, 16);
			woodIcon = resourceIconSpriteSheet.getSubimage(1 * 16, 0, 16, 16);
			waterIcon = resourceIconSpriteSheet.getSubimage(2 * 16, 0, 16, 16);
			populationIcon = resourceIconSpriteSheet.getSubimage(3 * 16, 0, 16,
					16);
			rockIcon = resourceIconSpriteSheet.getSubimage(4 * 16, 0, 16, 16);
			foodIcon = resourceIconSpriteSheet.getSubimage(5 * 16, 0, 16, 16);

			buttonImage = ImageIO.read(new File("image/ui/button.png"));
			camomileFont = Font.createFont(Font.PLAIN, new File(
					"fonts/camomile.ttf"));
			elektoraFont = Font.createFont(Font.PLAIN, new File(
					"fonts/elektora.ttf"));
			camomileFont = camomileFont.deriveFont(Font.PLAIN, 20);
			elektoraFont = elektoraFont.deriveFont(Font.PLAIN, 20);
			GraphicsEnvironment ge = GraphicsEnvironment
					.getLocalGraphicsEnvironment();
			ge.registerFont(camomileFont);
			ge.registerFont(elektoraFont);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		/** TOP!!! */
		HUDWindow = new HUDWindowPanel(theGame, aFrame);
		HUDWindow.setVisible(false);
		this.add(HUDWindow);

		/** THIS - 1st Layer: Quite Alert Panel */
		// Setup the alert panel
		quiteAlertPanel = new AlertPanel("alert",
				"Are you sure you want quite? All unsaved memory will be lost.", "Cancel", "Yes",
				new CloseAlertPanelListener(), new ConfirmQuitListener(),
				height);
		overwirteAlertPanel = new AlertPanel("alert",
				"Do you want to overwrite the previous memory?", "Cancel",
				"Yes", new CloseAlertPanelListener(),
				new ConfirmSaveListener(), height);
		
		quiteAlertPanel = new AlertPanel("alert",
				"Are you sure you want quite without save?", "Cancel", "Yes",
				new CloseAlertPanelListener(), new ConfirmQuitListener(),
				height);
		overwirteAlertPanel = new AlertPanel("alert",
				"Do you want to overwrite the previous memory?", "Cancel",
				"Yes", new CloseAlertPanelListener(),
				new ConfirmSaveListener(), height);
		
		winAlertPanel = new AlertPanel("win",
				"You won the game! Now you have a dock, you can build your boat and escape from this island.", "Escape",
				new QuitListener(), height);
		loseAlertPanel = new AlertPanel("lose",
				"You lose the game! Your agents all died!!!", "OK..."
				, new QuitListener(), height);
		

		this.add(quiteAlertPanel);
		this.add(overwirteAlertPanel);
		this.add(winAlertPanel);
		this.add(loseAlertPanel);
		overwirteAlertPanel.setVisible(false);
		quiteAlertPanel.setVisible(false);
		winAlertPanel.setVisible(false);
		loseAlertPanel.setVisible(false);

		/** THIS - 3rd Layer: Drawer panel */
		drawerPanel = new JPanel()
		{
			public void paintComponent(Graphics g)
			{
				super.paintComponent(g);

				Graphics2D g2 = (Graphics2D) g;
				g2.drawImage(buttonImage, 5 - 3, 5 - 2, null);
				g2.drawImage(buttonImage, 5 - 3, 40 - 2, null);
				g2.drawImage(buttonImage, 5 - 3, 75 - 2, null);
				g2.drawImage(buttonImage, 5 - 3, 110 - 2, null);
				repaint();
			}
		};
		drawerPanel.setLayout(null);
		drawerPanel.setOpaque(false);
		drawerPositionY = -DRAWER_HEIGHT + 40;
		drawerPanel.setBounds(0, drawerPositionY, DRAWER_WIDTH, DRAWER_HEIGHT);
		drawerPanel.setSize(DRAWER_WIDTH, DRAWER_HEIGHT);
		this.add(drawerPanel);

		/** drawerPanel - 1st Layer: Menu button */
		quit = new JButton("Quit");
		quit.setHorizontalAlignment(SwingConstants.CENTER);
		quit.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 14));
		quit.setForeground(Color.white);
		quit.addActionListener(new menuListener());
		quit.setBorder(BorderFactory.createEmptyBorder());
		quit.setContentAreaFilled(false);
		quit.setFocusable(false);
		drawerPanel.add(quit);
		quit.setBounds(5, 5, 80, 30);

		/** drawerPanel - 1st Layer: Save button */
		save = new JButton("Save");
		save.setHorizontalAlignment(SwingConstants.CENTER);
		save.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 14));
		save.setForeground(Color.white);
		save.setBorder(BorderFactory.createEmptyBorder());
		save.setContentAreaFilled(false);
		save.addActionListener(new saveListener());
		save.setFocusable(false);
		drawerPanel.add(save);
		save.setBounds(5, 40, 80, 30);

		/** drawerPanel - 1st Layer: Load button */
		mute = new JButton("Mute");
		mute.setHorizontalAlignment(SwingConstants.CENTER);
		mute.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 14));
		mute.setForeground(Color.white);
		mute.setBorder(BorderFactory.createEmptyBorder());
		mute.setContentAreaFilled(false);
		mute.addActionListener(new muteListener());
		mute.setFocusable(false);
		drawerPanel.add(mute);
		mute.setBounds(5, 75, 80, 30);

		/** drawerPanel - 1st Layer: More button */
		more = new JButton("More");
		more.setHorizontalAlignment(SwingConstants.CENTER);
		more.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 14));
		more.setForeground(Color.white);
		more.setBorder(BorderFactory.createEmptyBorder());
		more.setContentAreaFilled(false);
		more.addActionListener(new openDrawerListener());
		more.setFocusable(false);
		drawerPanel.add(more);
		more.setBounds(5, 110, 80, 30);

		/** THIS - 4th Layer: Global resource panel */
		globalResourcePanel = new JPanel();
		globalResourcePanel.setBounds(width - 250, 0, 250, 40);
		globalResourcePanel.setOpaque(false);
		globalResourcePanel.setLayout(null);
		this.add(globalResourcePanel);

		/** Build buttons */
		int buildBuMargin = 560;
		int buildBuDistance = 100;
		int buildBuSize = 100;

		/** THIS - 4th Layer: Build tent button */
		buildTent = new Button("Living Tent");
		buildTent.setHorizontalAlignment(SwingConstants.CENTER);
		buildTent.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 14));
		buildTent.setForeground(Color.white);
		buildTent.setContentAreaFilled(false);
		buildTent.setBorder(BorderFactory.createEmptyBorder());
		buildTent.addActionListener(new buildListener("tent", buildTent));
		this.add(buildTent);
		buildTent.setBounds(width - buildBuMargin - 5, height - 30,
				buildBuSize, 20);

		/** THIS - 4th Layer: Build kitchen button */
		buildBuMargin -= buildBuDistance;
		buildKitchen = new Button("Kitchen");
		buildKitchen.setHorizontalAlignment(SwingConstants.CENTER);
		buildKitchen
				.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 14));
		buildKitchen.setForeground(Color.white);
		buildKitchen.setContentAreaFilled(false);
		buildKitchen.setBorder(BorderFactory.createEmptyBorder());
		buildKitchen.addActionListener(new buildListener("kitchen",
				buildKitchen));
		this.add(buildKitchen);
		buildKitchen.setBounds(width - buildBuMargin - 5, height - 30,
				buildBuSize, 20);

		/** THIS - 4th Layer: Build workshop button */
		buildBuMargin -= buildBuDistance;
		buildWorkshop = new Button("Workshop");
		buildWorkshop.setHorizontalAlignment(SwingConstants.CENTER);
		buildWorkshop
				.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 14));
		buildWorkshop.setForeground(Color.white);
		buildWorkshop.setContentAreaFilled(false);
		buildWorkshop.setBorder(BorderFactory.createEmptyBorder());
		buildWorkshop.addActionListener(new buildListener("workshop",
				buildWorkshop));
		this.add(buildWorkshop);
		buildWorkshop.setBounds(width - buildBuMargin - 5, height - 30,
				buildBuSize, 20);

		/** THIS - 4th Layer: Build dock button */
		buildBuMargin -= buildBuDistance;
		buildDock = new Button("Dock");
		buildDock.setHorizontalAlignment(SwingConstants.CENTER);
		buildDock.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 14));
		buildDock.setForeground(Color.white);
		buildDock.setContentAreaFilled(false);
		buildDock.setBorder(BorderFactory.createEmptyBorder());
		buildDock.addActionListener(new buildListener("dock", buildDock));
		this.add(buildDock);
		buildDock.setBounds(width - buildBuMargin - 5, height - 30,
				buildBuSize, 20);

		/** THIS - 5th Layer: Command dropdown list */
		buildBuMargin -= buildBuDistance;
		String[] craftCommands =
		{ "Commands", "Place a Container", "Chop Wood", "Collect Stone", "Find Iron", "Get Water", "Harvest Food"};
		commandList = new JComboBox(craftCommands);
		// commandList.setFont(new Font(camomileFont.getFamily(), Font.PLAIN,
		// 14));
		commandList.setRenderer(new BasicComboBoxRenderer());
		commandList.setSelectedIndex(0);
		commandList.addActionListener(new dropDownListListener());
		commandList.setFocusable(false);
		this.add(commandList);
		commandList.setBounds(width - buildBuMargin - 5, height - 30,
				buildBuSize + 40, 20);

		/** THIS - 5th Layer: Command dropdown list */
		// buildBuMargin -= buildBuDistance;
		al = theGame.getAgents().getAliveAgents();
		Object agentsOnMap[] = new Object[al.size() + 1];
		agentsOnMap[0] = "Select Agent";
		for(int i = 0; i < al.size(); i++)
		{
			agentsOnMap[i + 1] = al.get(i);
		}

		agentList = new JComboBox(agentsOnMap);
		// commandList.setFont(new Font(camomileFont.getFamily(), Font.PLAIN,
		// 14));
		agentList.setRenderer(new BasicComboBoxRenderer());
		agentList.setSelectedIndex(0);
		agentList.addActionListener(new agentListListener());
		agentList.setFocusable(false);
		this.add(agentList);
		agentList.setBounds(width - buildBuMargin - 250, 7,
				buildBuSize + 40, 30);
	}

	public class agentListListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e)
		{
			JComboBox cb = (JComboBox) e.getSource();
			if(al.contains(cb.getSelectedItem()))
			{
				int x = (((Agent) cb.getSelectedItem()).getLocation().y * 24 - width / 2), y = (((Agent) cb
						.getSelectedItem()).getLocation().x * 24 - height / 2);
				if(x <= 0)
				{
					x = 0;
				}
				if(y <= 0)
				{
					y = 0;
				}
				if(x >= theGame.getMap().getWidth() * 24)
				{
					x = theGame.getMap().getWidth() * 24 - width;
				}
				if(y >= theGame.getMap().getHeight() * 24)
				{
					y = theGame.getMap().getHeight() * 24 - height;
				}
				Point changeViewPortByAgents = new Point(x, y);
				aFrame.getGamePanel().getScrollPane().getViewport()
						.setViewPosition(changeViewPortByAgents);
				System.out.println(changeViewPortByAgents);

				aFrame.getGamePanel().getMapPane().setPointer(true);
				aFrame.getGamePanel().getMapPane()
						.setTrackingAgent((Agent) cb.getSelectedItem());
				setAgentInfoToHUD((Agent) cb.getSelectedItem());

			}
			agentList.setSelectedIndex(0);
		}
	}

	public class dropDownListListener implements ActionListener
	{

		public void actionPerformed(ActionEvent e)
		{
			JComboBox cb = (JComboBox) e.getSource();
			String commandName = (String) cb.getSelectedItem();
			System.out.println(commandName);
			if(commandName.contains("Wood"))
			{
				theGame.getAgents().command(new HarvestCommand(Resources.Wood));
				System.out.println("Collecting wood command sent out!");
				aFrame.getConsole().addCommandString(
						"Harvest wood command sent out!");
				theGame.pushMessage("Chopping wood.");
			}
			else if(commandName.contains("Stone"))
			{
				theGame.getAgents()
						.command(new HarvestCommand(Resources.Stone));
				System.out.println("Collecting stones command sent out!");
				aFrame.getConsole().addCommandString(
						"Harvest stone command sent out!");
				theGame.pushMessage("Collecting stones.");
			}
			else if(commandName.contains("Iron"))
			{
				theGame.getAgents().command(new HarvestCommand(Resources.Iron));
				System.out.println("Collecting iron resource command sent out!");
				aFrame.getConsole().addCommandString(
						"Harvest iron command sent out!");
				theGame.pushMessage("Collecting iron resource.");
			}
			else if(commandName.contains("Water"))
			{
				theGame.getAgents()
						.command(new HarvestCommand(Resources.Water));
				System.out.println("Collecting water resource command sent out!");
				aFrame.getConsole().addCommandString(
						"Harvest water command sent out!");
				theGame.pushMessage("Collecting water resource.");
			}
			else if(commandName.contains("Food"))
			{
				theGame.getAgents().command(new HarvestCommand(Resources.Food));
				System.out.println("Harvest food command sent out!");
				aFrame.getConsole().addCommandString(
						"Harvest food command sent out!");
				theGame.pushMessage("Harvest food.");
			}
			else if(commandName.contains("Container"))
			{
				aFrame.getGamePanel().setBuildItem(true);
				int x = 10, y = 10;
				System.out.println("Placing a container at " + x + ", " + y + ".");
				containerPressed = true;
				aFrame.getGamePanel().getMapPane().turnBuildTrackerSwitchOn(true);
				//aFrame.getGamePanel().setBuildingToBuild(building);
				aFrame.getGamePanel()
						.getMapPane()
						.setBuildingBlockCheckerSize(1,1);
			}
			commandList.setSelectedIndex(0);
		}
	}

	public void setAgentInfoToHUD(Agent aAgent)
	{
		HUDWindow.setAgent(aAgent);
		HUDWindow.setVisible(true);
	}

	public void closeHUDWindow()
	{
		HUDWindow.setVisible(false);
		aFrame.getGamePanel().getMapPane().setPointer(false);
	}

	public void paintComponent(Graphics g)
	{
		// super.paintComponent(g);
		water = (int) theGame.getResourcepool().get(Resources.Water);
		wood = (int) theGame.getResourcepool().get(Resources.Wood);
		rock = (int) theGame.getResourcepool().get(Resources.Stone);
		iron = (int) theGame.getResourcepool().get(Resources.Iron);
		food = (int) theGame.getResourcepool().get(Resources.Food);

		population = theGame.getAgents().totalAgents();

		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.WHITE);
		g2.drawImage(resourceBarImage, width - resourceBarImage.getWidth(), 0,
				null);
		g2.drawImage(buildingBarImage, width - buildingBarImage.getWidth(),
				height - 0 - buildingBarImage.getHeight(), null);
		// Draw icons on the resourceBar
		g2.drawImage(ironIcon, width - 50, 5, null);
		g2.drawImage(woodIcon, width - 100, 5, null);
		g2.drawImage(rockIcon, width - 150, 5, null);
		g2.drawImage(waterIcon, width - 200, 5, null);
		g2.drawImage(foodIcon, width - 250, 5, null);
		g2.drawImage(populationIcon, width - 250 + 16, 5 + 16, null);

		g2.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 12));
		g2.drawString(iron + "", width - 50 + 16 + 5, 16);
		g2.drawString(wood + "", width - 100 + 16 + 5, 16);
		g2.drawString(rock + "", width - 150 + 16 + 5, 16);
		g2.drawString(water + "", width - 200 + 16 + 5, 16);
		g2.drawString(food + "", width - 250 + 16 + 5, 16);

		g2.drawString(
				"Population: " + population /* + "/" + totalPopulation */,
				width - 250 + 16 + 16, 16 + 16);

		g2.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 12));
		g2.drawString("RESOURCES", width - resourceBarImage.getWidth() + 240,
				16 + 16);

		int xMargin = 10, yMargin = 25;

		List<String> pushNotification = theGame.getAllNotifications();
		if(pushNotification.size() > 0)
		{
			// System.out.println(pushNotification);
			String textNot = pushNotification.get(pushNotification.size() - 1);
			if(pushNotification.size() > 1)
			{
				String textNotTop = pushNotification.get(pushNotification
						.size() - 2);
				// second notification (top)
				g2.setColor(new Color(0, 0, 0, 0.2f));
				g2.drawString(textNotTop, xMargin - 1, height - yMargin - 1);
				g2.drawString(textNotTop, xMargin + 1, height - yMargin + 1);
				g2.drawString(textNotTop, xMargin + 1, height - yMargin - 1);
				g2.drawString(textNotTop, xMargin - 1, height - yMargin + 1);
				g2.drawString(textNotTop, xMargin - 1, height - yMargin);
				g2.drawString(textNotTop, xMargin + 1, height - yMargin);
				g2.drawString(textNotTop, xMargin, height - yMargin - 1);
				g2.drawString(textNotTop, xMargin, height - yMargin + 1);

				g2.setColor(new Color(1, 1, 1, 0.5f));
				g2.drawString(textNotTop, xMargin, height - yMargin);
			}

			// first notification (bottom)
			g2.setColor(Color.BLACK);
			g2.drawString(textNot, xMargin - 1, height - yMargin - 1 + 15);
			g2.drawString(textNot, xMargin + 1, height - yMargin + 1 + 15);
			g2.drawString(textNot, xMargin + 1, height - yMargin - 1 + 15);
			g2.drawString(textNot, xMargin - 1, height - yMargin + 1 + 15);
			g2.drawString(textNot, xMargin - 1, height - yMargin + 15);
			g2.drawString(textNot, xMargin + 1, height - yMargin + 15);
			g2.drawString(textNot, xMargin, height - yMargin - 1 + 15);
			g2.drawString(textNot, xMargin, height - yMargin + 1 + 15);

			g2.setColor(Color.WHITE);
			g2.drawString(textNot, xMargin, height - yMargin + 15);
		}

		repaint();
	}

	private class openDrawerListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			drawerTimer = new Timer(0, new moveDrawerListener());
			drawerTimer.start();

			updateUI();
		}

	}

	private class moveDrawerListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			if(drawerPositionY < 0)
			{
				drawerPositionY += 1;
				drawerPanel.setBounds(0, drawerPositionY, DRAWER_WIDTH,
						DRAWER_HEIGHT);
			}
			else
			{
				drawerTimer.stop();
				more.setText("Less");
				more.removeActionListener(more.getActionListeners()[0]);
				more.addActionListener(new closeDrawerListener());
			}

		}

	}

	private class closeDrawerListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			drawerTimer = new Timer(0, new moveBackDrawerListener());
			drawerTimer.start();

			updateUI();
		}

	}

	private class moveBackDrawerListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			if(drawerPositionY > -DRAWER_HEIGHT + 40)
			{
				drawerPositionY -= 1;
				drawerPanel.setBounds(0, drawerPositionY, DRAWER_WIDTH,
						DRAWER_HEIGHT);
			}
			else
			{
				drawerTimer.stop();
				more.setText("More");
				more.removeActionListener(more.getActionListeners()[0]);
				more.addActionListener(new openDrawerListener());
			}

		}

	}

	/**
	 * CloseAlertPanelListener: Close the alert and abort quit.
	 */
	private class CloseAlertPanelListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			overwirteAlertPanel.setVisible(false);
			quiteAlertPanel.setVisible(false);

			activateButtons();
			gamePanel.melt();
			aFrame.restartTimer();
		}
	}

	/**
	 * ConfirmQuitListener: Close the alert and abort quit.
	 */
	private class ConfirmQuitListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			// Quit the current game.
			aFrame.showMenu();
		}
	}

	private class menuListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			disableButtons();
			gamePanel.froze();
			quiteAlertPanel.setVisible(true);
			aFrame.getTheGameTimer().stop();
		}

	}

	/**
	 * ConfirmSaveListener: Close the alert and abort quit.
	 */
	private class ConfirmSaveListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{

			aFrame.saveTheGame();
			overwirteAlertPanel.setVisible(false);
			activateButtons();
			gamePanel.melt();
			aFrame.restartTimer();
		}
	}

	private class saveListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			disableButtons();
			gamePanel.froze();
			overwirteAlertPanel.setVisible(true);
			aFrame.getTheGameTimer().stop();
		}
	}

	private class muteListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			// TODO: MUTE music!
			if(aFrame.playMusic())
			{
				aFrame.setMusicPlay(false);
				mute.setText("Un-mute");
				System.out.println("You mute the music!");
			}
			else
			{
				aFrame.setMusicPlay(true);
				mute.setText("Mute");
				System.out.println("You un-mute the music!");
			}
		}
	}

	public void disableButtons()
	{
		quit.setEnabled(false);
		save.setEnabled(false);
		mute.setEnabled(false);
		more.setEnabled(false);
		buildTent.setEnabled(false);
		buildKitchen.setEnabled(false);
		buildWorkshop.setEnabled(false);
		buildDock.setEnabled(false);
		aFrame.getGamePanel().getMapPane().setPointer(false);
		HUDWindow.setVisible(false);
		agentList.setVisible(false);
		commandList.setVisible(false);
	}

	public void activateButtons()
	{
		quit.setEnabled(true);
		save.setEnabled(true);
		mute.setEnabled(true);
		more.setEnabled(true);
		buildTent.setEnabled(true);
		buildKitchen.setEnabled(true);
		buildWorkshop.setEnabled(true);
		buildDock.setEnabled(true);
		agentList.setVisible(true);
		commandList.setVisible(true);
	}

	/**
	 * buildListener : Turn on the mosue tracker with the block checker with
	 * draw a gree hover on the build-able map tile, otherwise red.
	 */
	private class buildListener implements ActionListener
	{

		private Building building;
		private Button aButton;
		private String aStr;

		public buildListener(String aStr, Button aButton)
		{
			this.aStr = aStr;
			this.aButton = aButton;
		}

		@Override
		public void actionPerformed(ActionEvent e)
		{
			// Make switching buttons functional
			buildTent.setForeground(Color.white);
			buildKitchen.setForeground(Color.white);
			buildWorkshop.setForeground(Color.white);
			buildDock.setForeground(Color.white);
			buildTent.pressed = false;
			buildKitchen.pressed = false;
			buildWorkshop.pressed = false;
			buildDock.pressed = false;
			containerPressed = false;

			aFrame.getGamePanel().setBuildItem(false);
			if(aStr.equals("tent"))
			{
				building = new Building.Tent();
			}
			else if(aStr.equals("kitchen"))
			{
				building = new Building.Kitchen();
			}
			else if(aStr.equals("workshop"))
			{
				building = new Building.Workshop();
			}
			else if(aStr.equals("dock"))
			{
				building = new Building.Dock();
			}

			aFrame.getGamePanel().getMapPane().turnBuildTrackerSwitchOn(true);
			aFrame.getGamePanel().setBuildingToBuild(building);
			aFrame.getGamePanel()
					.getMapPane()
					.setBuildingBlockCheckerSize(building.getBlockHeight(),
							building.getBlockWidth());

			if(aButton.pressed)
			{
				aButton.setForeground(Color.white);
				aButton.pressed = false;
			}
			else
			{
				// HUDWindow.setVisible(false);
				aButton.setForeground(Color.gray);
				aButton.pressed = true;
			}

		}
	}

	public boolean isBuildPressed()
	{
		return buildTent.pressed || buildKitchen.pressed
				|| buildWorkshop.pressed || buildDock.pressed || containerPressed;
	}

	public void turnBuildOff()
	{
		buildTent.setForeground(Color.white);
		buildKitchen.setForeground(Color.white);
		buildWorkshop.setForeground(Color.white);
		buildDock.setForeground(Color.white);

		buildTent.pressed = false;
		buildKitchen.pressed = false;
		buildWorkshop.pressed = false;
		buildDock.pressed = false;
		containerPressed = false;

		aFrame.getGamePanel().getMapPane().turnBuildTrackerSwitchOn(false);

	}
	
	/** Close or countinue after win or lose panel shown up */
	private class QuitListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{		
			// Quit the current game.
			aFrame.showMenu();
			/*
			disableButtons();
			gamePanel.froze();
			aAlertPanel.setVisible(true);
			*/
		}
		
	}
	private class CloseWinListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			activateButtons();
			gamePanel.melt();
			winAlertPanel.setVisible(false);
			aFrame.restartTimer();
		}
	}
	private class CloseLoseListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			activateButtons();
			gamePanel.melt();
			loseAlertPanel.setVisible(false);
			aFrame.restartTimer();
		}
		
	}
	
	
	public AlertPanel getLoseAlertPanel()
	{
		return loseAlertPanel;
	}

	public AlertPanel getWinAlertPanel()
	{
		return winAlertPanel;
	}
}
