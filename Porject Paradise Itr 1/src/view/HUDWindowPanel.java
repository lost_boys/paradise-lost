/**
 * @author Bill Wilder
 * @author Collin Gifford
 * @author Sanket Shah
 * @author Yunhao Zhang (Hercy Chang)
 */

package view;

import java.awt.*;

import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

import main.MainViewController;
import model.Agent;
import model.GameState;

@SuppressWarnings("serial")
public class HUDWindowPanel extends JPanel
{
	private int WINDOW_WIDTH = 240;
	private int WINDOW_HEIGHT = 280;
	private int width;
	private int height;
	private Font camomileFont, elektoraFont;
	private GameState theGame;
	private MainViewController aFrame;
	private Agent aAgent;
	private JButton close;
	private BufferedImage windowBGImage;

	public HUDWindowPanel(GameState theGame, MainViewController aFrame)
	{
		try
		{
			windowBGImage = ImageIO.read(new File("image/ui/HUDWindow.png"));
			camomileFont = Font.createFont(Font.PLAIN, new File(
					"fonts/camomile.ttf"));
			elektoraFont = Font.createFont(Font.PLAIN, new File(
					"fonts/elektora.ttf"));
			camomileFont = camomileFont.deriveFont(Font.PLAIN, 20);
			elektoraFont = elektoraFont.deriveFont(Font.PLAIN, 20);
			GraphicsEnvironment ge = GraphicsEnvironment
					.getLocalGraphicsEnvironment();
			ge.registerFont(camomileFont);
			ge.registerFont(elektoraFont);
		}

		catch(Exception e)
		{
			e.printStackTrace();
		}
		this.aFrame = aFrame;
		this.width = aFrame.getContentWidth();
		this.height = aFrame.getContentHeight();

		this.theGame = theGame;

		this.setOpaque(false);
		this.setLayout(null);

		this.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		this.setBounds(width - WINDOW_WIDTH - 40, 40, WINDOW_WIDTH + 40,
				WINDOW_HEIGHT + 40);

		this.setBackground(new Color(0, 0, 0, 125));

		JPanel innerPanel = new JPanel()
		{
			@Override
			public void paintComponent(Graphics g)
			{
				Graphics2D g2 = (Graphics2D) g;
				g2.setColor(Color.WHITE);
				g2.setFont(new Font(camomileFont.getFamily(), Font.BOLD, 16));

				// Draw line
				g2.setColor(Color.GRAY);
				g2.drawLine(5, 20, WINDOW_WIDTH - 5, 20);
				g2.setColor(Color.BLACK);
				g2.drawLine(5, 21, WINDOW_WIDTH - 5, 21);
				g2.setColor(Color.WHITE);

				if(aAgent != null)
				{
					// Agent name
					g2.setColor(Color.BLACK);
					g2.drawString(aAgent.getName(), 5, 16);
					g2.setColor(Color.WHITE);
					g2.drawString(aAgent.getName(), 5, 15);
					int margin = 35;

					// Get agent status.
					g2.setFont(new Font(camomileFont.getFamily(), Font.PLAIN,
							12));
					drawString(g2, aAgent.getStatusList().get(0), 5, margin,
							WINDOW_WIDTH - 10);
					margin += 20;

					// Draw line
					g2.setColor(Color.GRAY);
					g2.drawLine(5, margin, WINDOW_WIDTH - 5, margin);
					g2.setColor(Color.BLACK);
					g2.drawLine(5, margin + 1, WINDOW_WIDTH - 5, margin + 1);
					g2.setColor(Color.WHITE);
					margin += 15;

					// Get agent life status.
					g2.setFont(new Font(camomileFont.getFamily(), Font.PLAIN,
							12));
					drawString(g2, aAgent.getStatusList().get(1), 5, margin,
							WINDOW_WIDTH - 10);
					margin += 15;
					drawString(g2, aAgent.getStatusList().get(2), 5, margin,
							WINDOW_WIDTH - 10);
					margin += 15;
					drawString(g2, aAgent.getStatusList().get(3), 5, margin,
							WINDOW_WIDTH - 10);
					margin += 5;

					// Draw line
					g2.setColor(Color.GRAY);
					g2.drawLine(5, margin, WINDOW_WIDTH - 5, margin);
					g2.setColor(Color.BLACK);
					g2.drawLine(5, margin + 1, WINDOW_WIDTH - 5, margin + 1);
					g2.setColor(Color.WHITE);
					margin += 15;

					// Get resource carraied.
					g2.setFont(new Font(camomileFont.getFamily(), Font.BOLD, 12));
					drawString(g2, "Resource carraied:", 5, margin,
							WINDOW_WIDTH - 10);
					margin += 15;
					g2.setFont(new Font(camomileFont.getFamily(), Font.PLAIN,
							12));
					drawString(g2, aAgent.getStatusList().get(4), 5, margin,
							WINDOW_WIDTH - 10);
					margin += 15;
					drawString(g2, aAgent.getStatusList().get(5), 5, margin,
							WINDOW_WIDTH - 10);
					margin += 15;
					drawString(g2, aAgent.getStatusList().get(6), 5, margin,
							WINDOW_WIDTH - 10);
					margin += 15;
					drawString(g2, aAgent.getStatusList().get(7), 5, margin,
							WINDOW_WIDTH - 10);
					margin += 15;
					drawString(g2, aAgent.getStatusList().get(8), 5, margin,
							WINDOW_WIDTH - 10);
					margin += 5;

					// Draw line
					g2.setColor(Color.GRAY);
					g2.drawLine(5, margin, WINDOW_WIDTH - 5, margin);
					g2.setColor(Color.BLACK);
					g2.drawLine(5, margin + 1, WINDOW_WIDTH - 5, margin + 1);
					g2.setColor(Color.WHITE);
					margin += 15;

					// Get items carraied.
					g2.setFont(new Font(camomileFont.getFamily(), Font.BOLD, 12));
					drawString(g2, "Item carraied:", 5, margin,
							WINDOW_WIDTH - 10);
					g2.setFont(new Font(camomileFont.getFamily(), Font.PLAIN,
							12));
					if(aAgent.getStatusList().size() == 9)
					{
						margin += 15;
						drawString(g2, "N/a", 5, margin, WINDOW_WIDTH - 10);
					}
					else
					{
						for(int i = 9; i < aAgent.getStatusList().size(); i++)
						{
							margin += 15;
							drawString(g2, aAgent.getStatusList().get(i), 5,
									margin, WINDOW_WIDTH - 10);
						}
					}
					margin += 5;
				}
				else
				{
					g2.setColor(Color.BLACK);
					g2.drawString("N/a", 5, 15);
					g2.setColor(Color.WHITE);
					g2.drawString("N/a", 5, 16);
					g2.setFont(new Font(camomileFont.getFamily(), Font.PLAIN,
							12));
				}

			}
		};
		innerPanel.setLayout(null);
		// innerPanel.setBackground(Color.WHITE);
		innerPanel.setOpaque(false);
		innerPanel.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		innerPanel.setBounds(20, 20, WINDOW_WIDTH, WINDOW_HEIGHT);
		this.add(innerPanel);

		/** 1st Layer: Close button */
		close = new JButton("\u00D7");
		close.setHorizontalAlignment(SwingConstants.CENTER);
		close.setFont(new Font(camomileFont.getFamily(), Font.PLAIN, 14));
		close.setForeground(Color.white);
		close.addActionListener(new closeListener());
		close.setBorder(BorderFactory.createEmptyBorder());
		close.setContentAreaFilled(false);
		close.setFocusable(false);
		innerPanel.add(close);
		close.setBounds(WINDOW_WIDTH - 15, 5, 10, 10);
	}

	public void drawString(Graphics g, String s, int x, int margin, int width)
	{
		// FontMetrics gives us information about the width,
		// height, etc. of the current Graphics object's Font.
		FontMetrics fm = g.getFontMetrics();

		int lineHeight = fm.getHeight();

		int curX = x;
		int curY = margin;

		String[] words = s.split(" ");

		for(String word : words)
		{
			// Find out thw width of the word.
			int wordWidth = fm.stringWidth(word + " ");

			// If text exceeds the width, then move to next line.
			if(curX + wordWidth >= x + width)
			{
				curY += lineHeight;
				curX = x;
			}

			g.drawString(word, curX, curY);

			// Move over to the right for next word.
			curX += wordWidth;
		}

	}

	@Override
	public void paintComponent(Graphics g)
	{
		Graphics2D g2 = (Graphics2D) g;
		g2.drawImage(windowBGImage, 0, 0, null);

	}

	public void setAgent(Agent aAgent)
	{
		this.aAgent = aAgent;
	}

	private class closeListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e)
		{
			setVisible(false);
		}

	}
}
